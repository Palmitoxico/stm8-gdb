# Microsoft Developer Studio Project File - Name="libiberty" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

#	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=libiberty - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libiberty.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libiberty.mak" CFG="libiberty - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libiberty - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "libiberty - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "libiberty"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libiberty - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../gdb"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /GX /Od /I "." /I "..\include" /D "NDEBUG" /D "NEED_basename" /D "NEED_sys_siglist" /D "NEED_strsignal" /D "NO_SYS_FILE_H" /D "WIN32" /D "_WINDOWS" /D "HOST_WIN32" /FD /c
# SUBTRACT CPP /Fr /YX
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "libiberty - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../gdb"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /GX /Z7 /Od /I "." /I "..\include" /D "_DEBUG" /D "HOST_WIN32" /D "NEED_basename" /D "NEED_sys_siglist" /D "NEED_strsignal" /D "NO_SYS_FILE_H" /D "WIN32" /D "_WINDOWS" /Fr /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "libiberty - Win32 Release"
# Name "libiberty - Win32 Debug"
# Begin Group "Sources"

# PROP Default_Filter ".c"
# Begin Source File

SOURCE=.\argv.c
# End Source File
# Begin Source File

SOURCE=.\asprintf.c
# End Source File
# Begin Source File

SOURCE=.\atexit.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\basename.c
# End Source File
# Begin Source File

SOURCE=".\choose-temp.c"
# End Source File
# Begin Source File

SOURCE=.\concat.c
# End Source File
# Begin Source File

SOURCE=".\cplus-dem.c"
# End Source File
# Begin Source File

SOURCE=.\fdmatch.c

!IF  "$(CFG)" == "libiberty - Win32 Release"

# ADD CPP /Ze

!ELSEIF  "$(CFG)" == "libiberty - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\floatformat.c
# End Source File
# Begin Source File

SOURCE=.\fnmatch.c
# End Source File
# Begin Source File

SOURCE=.\getopt.c
# End Source File
# Begin Source File

SOURCE=.\getopt1.c
# End Source File
# Begin Source File

SOURCE=.\getruntime.c
# End Source File
# Begin Source File

SOURCE=.\hex.c
# End Source File
# Begin Source File

SOURCE=.\memmove.c
# End Source File
# Begin Source File

SOURCE=.\objalloc.c
# End Source File
# Begin Source File

SOURCE=.\obstack.c
# End Source File
# Begin Source File

SOURCE=.\pexecute.c
# End Source File
# Begin Source File

SOURCE=.\spaces.c
# End Source File
# Begin Source File

SOURCE=.\strcasecmp.c
# End Source File
# Begin Source File

SOURCE=.\strerror.c
# End Source File
# Begin Source File

SOURCE=.\strncasecmp.c
# End Source File
# Begin Source File

SOURCE=.\strsignal.c
# End Source File
# Begin Source File

SOURCE=.\strtoul.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\vasprintf.c
# End Source File
# Begin Source File

SOURCE=.\xatexit.c
# End Source File
# Begin Source File

SOURCE=.\xexit.c
# End Source File
# Begin Source File

SOURCE=.\xmalloc.c
# End Source File
# Begin Source File

SOURCE=.\xstrdup.c
# End Source File
# Begin Source File

SOURCE=.\xstrerror.c
# End Source File
# End Group
# Begin Group "Headers"

# PROP Default_Filter ".h"
# Begin Source File

SOURCE=".\alloca-conf.h"
# End Source File
# Begin Source File

SOURCE=..\include\ansidecl.h
# End Source File
# Begin Source File

SOURCE=.\config.h
# End Source File
# Begin Source File

SOURCE=..\include\demangle.h
# End Source File
# Begin Source File

SOURCE=..\include\floatformat.h
# End Source File
# Begin Source File

SOURCE=..\include\fnmatch.h
# End Source File
# Begin Source File

SOURCE=..\include\getopt.h
# End Source File
# Begin Source File

SOURCE=..\include\libiberty.h
# End Source File
# Begin Source File

SOURCE=..\include\objalloc.h
# End Source File
# Begin Source File

SOURCE=..\include\obstack.h
# End Source File
# End Group
# End Target
# End Project
