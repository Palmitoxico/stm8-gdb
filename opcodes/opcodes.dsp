# Microsoft Developer Studio Project File - Name="opcodes" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=opcodes - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "opcodes.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "opcodes.mak" CFG="opcodes - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "opcodes - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "opcodes - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "opcodes"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "opcodes - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../gdb"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /GX /Od /I "." /I "../include" /I "../bfd" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "HOST_WIN32" /D "HAVE_CONFIG_H" /D "_GNU_SOURCE" /D "TARGET_ST7" /D "ARCH_st7" /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "opcodes - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../gdb"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /GX /Z7 /Od /I "." /I "../include" /I "../bfd" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "HOST_WIN32" /D "HAVE_CONFIG_H" /D "_GNU_SOURCE" /D "TARGET_ST7" /D "ARCH_st7" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "opcodes - Win32 Release"
# Name "opcodes - Win32 Debug"
# Begin Group "Sources"

# PROP Default_Filter ".c"
# Begin Source File

SOURCE=".\dis-buf.c"
# End Source File
# Begin Source File

SOURCE=".\st7-asm.c"
# End Source File
# Begin Source File

SOURCE=".\st7-disasm.c"
# End Source File
# Begin Source File

SOURCE=".\st7-instr.c"

!IF  "$(CFG)" == "opcodes - Win32 Release"

!ELSEIF  "$(CFG)" == "opcodes - Win32 Debug"

# ADD CPP /w /W0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=".\stm7-instr.c"
# End Source File
# Begin Source File

SOURCE=".\stm7pv1-instr.c"
# End Source File
# Begin Source File

SOURCE=".\stm7pv2-instr.c"
# End Source File
# End Group
# Begin Group "Headers"

# PROP Default_Filter ".h"
# Begin Source File

SOURCE=..\include\ansidecl.h
# End Source File
# Begin Source File

SOURCE=..\bfd\bfd.h
# End Source File
# Begin Source File

SOURCE=.\config.h
# End Source File
# Begin Source File

SOURCE="..\include\dis-asm.h"
# End Source File
# Begin Source File

SOURCE=.\ins7.h
# End Source File
# Begin Source File

SOURCE=.\sysdep.h
# End Source File
# End Group
# End Target
# End Project
