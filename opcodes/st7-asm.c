/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* Module that implements the on line assembler of gdb7 */
#include "bfd.h"

#include "ins7.h"

static unsigned long instr_addr;
static char label_string[STRING_LENGTH];
static unsigned long operand_addr;

static char *errmsgs7[] =
{ "* Mnemonic error *",
  "* 1st operand begin error *",
  "* 1st operand number/symbol error *",
  "* 1st operand end error *",
  "* 2nd operand begin error *",
  "* 2nd operand number/symbol error *",
  "* 2nd operand end error *",
  "* 3rd operand number/symbol error *"
};

static int is_size_extension(char *instr)
{
  return (*instr == '.'
		  && (toupper(*(instr+1)) == 'B'
		      || toupper(*(instr+1)) == 'W'
			  || toupper(*(instr+1)) == 'E'));
}

static int is_index_register(char *instr)
{
  return (strncasecmp(instr, "X)", 2) == 0
		  || strncasecmp(instr, "Y)", 2) == 0
		  || strncasecmp(instr, "SP)", 3) == 0);
}

/************************************/
/*   PARSE INSTRUCTION              */
/************************************/

/* instr: instruction to be assembled */
/* instr_chain_list: same as instr but keywords are separated by '\0' */
/* field_pointer: pointer to instruction fields */
static prepare_instruction(char *instr, char *instr_chain_list, char **field_pointer)
{ 
  char *cur_instr;  /* used to traverse instr */
  char instr2[STRING_LENGTH]; /* modified copy of instr */
  char instr3[STRING_LENGTH]; /* modified copy of instr2 */

  /* instr2 is the same as instr but .B may be added within []. */
  for(cur_instr = instr2; *instr != ' ' && *instr != '\0'; ) *cur_instr++ = *instr++;
  for(*cur_instr++ = ' '; *instr != '\0'; instr++)
  {
	if (*instr == ']')
    {
	  if (!is_size_extension(instr-2))
		{
		  *cur_instr++ = '.';
		  if (mcu_core == STM7P_CORE) {
			*cur_instr++ = 'W';
		  } else {
			*cur_instr++ = 'B';
		  }
		}
	}
    if (*instr != ' ')
	  *cur_instr++ = *instr;
  }
  *cur_instr = '\0';

  /* instr3 is the same as instr2 but operands are separated by a blank. */
  for (cur_instr = instr3, instr = instr2; *instr != ' '; ) *cur_instr++ = *instr++;
  for (*cur_instr++ = *instr++; *instr != '\0'; instr++)
  { 
    if (*instr == ',' && !is_index_register(instr+1))
	  *cur_instr++ = ' ';
    else
      *cur_instr++ = *instr;
  }
  while (cur_instr < (instr3 + STRING_LENGTH - 1)) *cur_instr++ = ' ';
  *cur_instr = '\0';

  /* Operation code */
  for (cur_instr = instr_chain_list, instr = instr3, *field_pointer++ = cur_instr;
       *instr != ' '; )
	*cur_instr++ = *instr++;

  /* First operand */
  for (*cur_instr++ = '\0', *field_pointer++ = cur_instr, instr++;
       *instr == '(' || *instr == '[' || *instr == '#'; )
  {
	*cur_instr++ = *instr++;
    if(*instr != '(' && *instr != '[')
    {
	  *cur_instr++ = '\0';
      *field_pointer++ = cur_instr;
      if (is_index_register(instr))
		break;
      for( ; *instr != ' ' && *instr != '.' && *instr != ',' && *instr != ')' && *instr != ']'; )
		*cur_instr++ = *instr++;
      *cur_instr++ = '\0';
      *field_pointer++ = cur_instr;
      break;
	}
  }
  while (*instr != ' ')
	*cur_instr++ = *instr++;

  /* Second operand */
  for (*cur_instr++ = '\0', *field_pointer++ = cur_instr, instr++;
       *instr == '(' || *instr == '[' || *instr == '#'; )
  {
	*cur_instr++ = *instr++;
    if (*instr != '(' && *instr != '[')
    {
	  *cur_instr++ = '\0';
      *field_pointer++ = cur_instr;
      if (is_index_register(instr))
		break;
      for ( ; *instr != ' ' && *instr != '.' && *instr != ',' && *instr != ')' && *instr != ']'; )
		*cur_instr++ = *instr++;
      *cur_instr++ = '\0';
      *field_pointer++ = cur_instr;
      break;
	}
  }
  while (*instr != ' ') *cur_instr++ = *instr++;
  for (instr++; *instr != ' '; ) *cur_instr++ = *instr++;
  *cur_instr = '\0';
}


/*****************************************/
/*   ASSEMBLE NUMERICAL VALUES           */
/*****************************************/

/* number: number to be assembled */
/* type: type of value to be assembled */
static long assemble_value(char *number, uchar type)
{
  int asterisk = 0;
  long value = 0; /* assembled value */

  if (*number == '*')
  {
	asterisk = 1;
	value = instr_addr;
	number++;
  }
  if (!asterisk || *number)
  {
    if ((*number >= '0' && *number <= '9') || *number == '+' || *number == '-')
      value += strtol (number, 0, 0);
    /* try to find a GDB symbol with such a name */
    else if (!get_symbol_address(number, &value))
      return (-1); /* we have not found a gdb symbol with this name */
  }

  switch (type)
  {
    case A08:
    case I08:
        operand_addr++;
        if (value >= VAL00 && value <= VAL08)
		  value |= 0x01000000;
        else
		  value = -1;
        break;
    case A16:
    case I16:
        operand_addr += 2;
        if (value >= VAL00 && value <= VAL08)
		  value = -1;
        else {
		  if (value > VAL08 && value <= VAL16)
			value |= 0x02000000;
          else
			value = -1;
        }
        break;
#ifdef TARGET_STM7
    case A16NA:
        operand_addr += 2;
		if (value >= VAL00 && value <= VAL16)
		  value |= 0x02000000;
        else
		  value = -1;
        break;
    case A24:
        operand_addr += 3;
        if (value >= VAL00 && value <= VAL24)
		  value |= 0x03000000;
        else
		  value = -1;
        break;
#endif
    case D08:
        operand_addr++;
        value -= operand_addr;
        if (value >= VALN7 && value <= VAL07)
		  value = (value & VAL08) | 0x01000000;
        else
		  value = -1;
        break;
    case N03:
        if (value < VAL00 || value > VAL03)
		  value = -1;
        break;
    case N08:
        operand_addr++;
        if (value >= VAL00 && value <= VAL08)
		  value |= 0x01000000;
        else
		  value = -1;
        break;
#ifdef TARGET_STM7P
    case N16:
        operand_addr += 2;
		if (value >= VAL00 && value <= VAL16)
		  value |= 0x02000000;
        else
		  value = -1;
        break;
#endif
    default:
        value = -1;
        break;
  }
  return value;
}

static int is_type(uchar type)
{
	return (type == A08 || type == A16 || type == A16NA || type == A24
		    || type == D08
			|| type == I08 || type == I16
			|| type == N03 || type == N08
#ifdef TARGET_STM7P
			|| type == N16
#endif
			);
}

/*************************/
/*   ST7 ASSEMBLER       */
/*************************/

/* instr: instruction to be assembled */
/* buffer_start: string containing assembled instruction bytes */
/* errmsg_ptr: pointer to returned error message */

static long st7_assemble(char *instr, uchar *buffer_start, char ** errmsg_ptr)
{
  char **field_pointer;			/* pointer to instruction fields */
  int operand1_flag;			/* 1st operand present */
  int operand2_flag;			/* 2nd operand present */
  int operand3_flag;			/* 3rd operand present */
  
  long operand1_value;			/* 1st operand value */
  long operand2_value;			/* 2nd operand value */
  long operand3_value;			/* 3rd operand value */

  struct inst7 *instr_descr;	/* pointer to instruction description */
  uchar err;					/* assembly error number */
  uchar *buffer;				/* pointer to assembled instruction */
  ushort code;					/* operation code of instruction to be assembled */
  char instr_chain_list[STRING_LENGTH+2];
								/* list of instruction fields */
  char *fields[8];				/* array of instruction fields */
  
#if 0
  for(instr_descr = mcucore_instr;
	  (code = instr_descr->codop) != ASSFIN;
	  instr_descr++)
	  {
		  if (is_type(instr_descr->type1)
			  && is_type(instr_descr->type2))
			  printf("%x %s\n", instr_descr->codop, mcucore_tokens[instr_descr->mnemonic]);
	  }
#endif
	prepare_instruction(instr, instr_chain_list, fields);
    *errmsg_ptr = 0;
    for (instr_descr = mcucore_instr,err = 0,buffer = buffer_start;
	     (code = instr_descr->codop) != ASSFIN;
		 instr_descr++)
    {
	  field_pointer = fields;
	  operand1_flag = 0;
      operand2_flag = 0;
      operand3_flag = 0;
      operand1_value = 0;
      operand2_value = 0;
      operand3_value = 0;
      operand_addr = instr_addr+1;
      if (code>>8 != 0)
		operand_addr++;
      if (strcasecmp(*field_pointer,mcucore_tokens[instr_descr->mnemonic]) == 0)
	  {
		if (instr_descr->prefix1 == END)
		  break;
        else {
		  field_pointer++;
          if (instr_descr->prefix1 != EMPTY) {
			if (strcasecmp(*field_pointer,mcucore_tokens[instr_descr->prefix1]) == 0)
		      field_pointer++;
            else {
		      if (err < 1)
			    err = 1;
              continue;
			} 
		  }
          if (instr_descr->type1 != EMPTY) {
			operand1_value = assemble_value(*field_pointer,instr_descr->type1);
            if (operand1_value != -1) {
              operand1_flag = 1;
			  field_pointer++;
            } else {
			  if (err < 2)
				err = 2;
              continue;
			}
		  }
          if (instr_descr->postfix1 != EMPTY) {
			if (strcasecmp(*field_pointer,mcucore_tokens[instr_descr->postfix1]) == 0)
			  field_pointer++;
            else {
			  if (err < 3)
			    err = 3;
              continue;
			}
		  }
          if (instr_descr->prefix2 == END)
			break;
          else {
			if (instr_descr->prefix2 != EMPTY) {
			  if (strcasecmp(*field_pointer,mcucore_tokens[instr_descr->prefix2]) == 0)
				field_pointer++;
              else {
				if (err < 4)
				  err = 4;
                continue;
			  }
			}
            if (instr_descr->type2 != EMPTY) {
			  operand2_value = assemble_value(*field_pointer,instr_descr->type2);
              if (operand2_value != -1) {
				operand2_flag = 1;
				if (operand2_value>>24 == 0)
				  code += 2*operand2_value;
                field_pointer++;
              }
              else {
				if (err < 5)
				  err = 5;
                continue;
			  }
			}
            if (instr_descr->postfix2 != EMPTY) {
			  if (strcasecmp(*field_pointer,mcucore_tokens[instr_descr->postfix2]) == 0)
				field_pointer++;
              else {
				if (err < 6)
				  err = 6;
                continue;
			  }
			}
            if (instr_descr->type3 == END)
			  break;
            else {
			  operand3_value = assemble_value(*field_pointer,instr_descr->type3);
              if (operand3_value != -1) {
				operand3_flag = 1;
				break;
              } else
				err = 7;
			}
		  }
		}
	  }
	}
    if (code == DESFIN) {
	  *buffer++ = operand1_value;
    } else if (code == ASSFIN)
	  *errmsg_ptr = errmsgs7[err];
    else {
	  if (code>>8 != 0)
		*buffer++ = code>>8;
      *buffer++ = code;
#ifdef TARGET_STM7
	  /* Take into account MOV instruction which has two operands
	     which are inverted. */
	  /* More generally take into account two-operand instructions
	     with inverted operands. */
	  if (operand1_flag
		  && operand2_flag
		  && !operand3_flag
		  && instr_descr->position1 >= 0
		  && instr_descr->position2 >= 0
		  && instr_descr->position2 < instr_descr->position1)
		{
		  long temp_operand = operand1_value;
		  operand1_value = operand2_value;
		  operand2_value = temp_operand;
		}
	  /* Note: there are 2 variants of MOV from memory to memory.
		 MOV BYTE,BYTE
		 MOV WORD,WORD
		 Fortunately the operation codes are in this order
		 and we don't have to modify assemble_value. */
#endif
      if (operand1_value>>24 == 3)
		*buffer++ = operand1_value>>16;
      if (operand1_value>>24 >= 2)
		*buffer++ = operand1_value>>8;
      if (operand1_value>>24 >= 1)
		*buffer++ = operand1_value;

      if (operand2_value>>24 == 3)
		*buffer++ = operand2_value>>16;
	  if (operand2_value>>24 >= 2)
		*buffer++ = operand2_value>>8;
	  if (operand2_value>>24 >= 1)
		*buffer++ = operand2_value;
	  
	  if (operand3_value>>24 == 1)
		*buffer++ = operand3_value;
	}

  return (long)(buffer-buffer_start);
}

/****************************************
* st7asm
*
*   return -1 if an error occurs
****************************************/


int st7asm(bfd_vma *memaddr, char *asmline, char **errmsg_ptr)
{
  long len;
  unsigned char buffer[MAXILEN];

  instr_addr = *memaddr;

  /* Assemble the new instruction */
  len = st7_assemble(asmline, buffer, errmsg_ptr);

  *memaddr = instr_addr + len;
  if (*errmsg_ptr)
	return -1;
  else
    target_write_memory (instr_addr, buffer, len);
  return 0;
}
