/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* BFD support for the ST7 architecture. */

#include "bfd.h"
#include "sysdep.h"
#include "libbfd.h"

static bfd_arch_info_type arch_info_struct = 
{
  16,				/* 16 bits in a word */
  16,				/* 16 bits in an address */
  8,				/* 8 bits in a byte */
  bfd_arch_st7,		/* enum bfd_architecture */
  0,				/* only 1 machine */
  "st7",			/* arch_name */
  "st7",			/* printable_name */
  3,				/* section align power */
  true,				/* true if this is the default machine
				   for the architecture */
  bfd_default_compatible, 
  bfd_default_scan ,

  /* How to disassemble an instruction, producing a printable
     representation on a specified stdio stream.  This isn't
     defined for most processors at present, because of the size
     of the additional tables it would drag in, and because gdb
     wants to use a different interface.  */
  0,
};



const bfd_arch_info_type bfd_st7_arch =
  {
    16,	/* bits in a word */
    16,	/* bits in an address */
    8,	/* bits in a byte */
    bfd_arch_st7,
    0,
    "st7",
    "st7",
    3,
    true, /* the default */
    bfd_default_compatible, 
    bfd_default_scan,
    0,
  };
