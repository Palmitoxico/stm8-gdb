# Microsoft Developer Studio Project File - Name="bfd" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

#	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=bfd - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "bfd.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "bfd.mak" CFG="bfd - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "bfd - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "bfd - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "bfd"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "bfd - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../gdb"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /GX /Od /I "." /I "../include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "HOST_WIN32" /D "HAVE_CONFIG_H" /D "_GNU_SOURCE" /D "TARGET_ST7" /D "HOST_HIWARE" /D "HOST_CRASH_B" /D "ALMOST_STDC" /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "bfd - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../gdb"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /GX /Z7 /Od /I "." /I "../include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "HOST_WIN32" /D "HAVE_CONFIG_H" /D "_GNU_SOURCE" /D "TARGET_ST7" /D "HOST_HIWARE" /D "HOST_CRASH_B" /D "ALMOST_STDC" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "bfd - Win32 Release"
# Name "bfd - Win32 Debug"
# Begin Group "Sources"

# PROP Default_Filter ".c"
# Begin Source File

SOURCE=.\archive.c
# End Source File
# Begin Source File

SOURCE=.\archures.c

!IF  "$(CFG)" == "bfd - Win32 Release"

# ADD CPP /D SELECT_ARCHITECTURES=&bfd_st7_arch /D SELECT_VECS="&hicross_vec,&cbmap_vec,&bfd_elf32_big_generic_vec" /D "HAVE_hicross_vec" /D "HAVE_cbmap_vec" /D "HAVE_bfd_elf32_big_generic_vec"

!ELSEIF  "$(CFG)" == "bfd - Win32 Debug"

# ADD CPP /Z7 /D SELECT_ARCHITECTURES=&bfd_st7_arch /D SELECT_VECS="&hicross_vec,&cbmap_vec,&bfd_elf32_st7_vec" /D "HAVE_hicross_vec" /D "HAVE_cbmap_vec" /D "HAVE_bfd_elf32_st7_vec"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\bfd.c
# End Source File
# Begin Source File

SOURCE=.\binary.c

!IF  "$(CFG)" == "bfd - Win32 Release"

!ELSEIF  "$(CFG)" == "bfd - Win32 Debug"

# ADD CPP /Ze

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\cache.c
# End Source File
# Begin Source File

SOURCE=.\cbmap.c
# End Source File
# Begin Source File

SOURCE=.\coffgen.c
# End Source File
# Begin Source File

SOURCE=.\corefile.c
# End Source File
# Begin Source File

SOURCE=".\cpu-st7.c"
# End Source File
# Begin Source File

SOURCE=.\dwarf2.c
# End Source File
# Begin Source File

SOURCE=.\elf.c
# End Source File
# Begin Source File

SOURCE=".\elf32-st7.c"
# End Source File
# Begin Source File

SOURCE=.\elf32.c
# End Source File
# Begin Source File

SOURCE=.\elflink.c
# End Source File
# Begin Source File

SOURCE=.\format.c
# End Source File
# Begin Source File

SOURCE=.\hash.c
# End Source File
# Begin Source File

SOURCE=.\hicross.c
# End Source File
# Begin Source File

SOURCE=.\ihex.c
# End Source File
# Begin Source File

SOURCE=.\init.c
# End Source File
# Begin Source File

SOURCE=.\libbfd.c
# End Source File
# Begin Source File

SOURCE=.\linker.c
# End Source File
# Begin Source File

SOURCE=.\opncls.c
# End Source File
# Begin Source File

SOURCE=.\reloc.c
# End Source File
# Begin Source File

SOURCE=.\section.c
# End Source File
# Begin Source File

SOURCE=.\srec.c
# End Source File
# Begin Source File

SOURCE=".\stab-syms.c"
# End Source File
# Begin Source File

SOURCE=.\stabs.c
# End Source File
# Begin Source File

SOURCE=.\syms.c
# End Source File
# Begin Source File

SOURCE=.\targets.c
# ADD CPP /D SELECT_ARCHITECTURES=&bfd_st7_arch /D SELECT_VECS="&hicross_vec,&cbmap_vec,&bfd_elf32_st7_vec" /D "HAVE_hicross_vec" /D "HAVE_cbmap_vec" /D "HAVE_bfd_elf32_st7_vec"
# End Source File
# Begin Source File

SOURCE=.\tekhex.c
# End Source File
# End Group
# Begin Group "Headers"

# PROP Default_Filter ".h"
# Begin Source File

SOURCE=..\include\ansidecl.h
# End Source File
# Begin Source File

SOURCE=..\include\aout\aout64.h
# End Source File
# Begin Source File

SOURCE=..\include\aout\ar.h
# End Source File
# Begin Source File

SOURCE=.\bfd.h
# End Source File
# Begin Source File

SOURCE=..\include\bfdlink.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\common.h
# End Source File
# Begin Source File

SOURCE=.\config.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\ecoff.h
# End Source File
# Begin Source File

SOURCE=".\elf-bfd.h"
# End Source File
# Begin Source File

SOURCE=".\elf32-target.h"
# End Source File
# Begin Source File

SOURCE=.\elfcode.h
# End Source File
# Begin Source File

SOURCE=.\elfcore.h
# End Source File
# Begin Source File

SOURCE=.\elflink.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\external.h
# End Source File
# Begin Source File

SOURCE=..\include\fnmatch.h
# End Source File
# Begin Source File

SOURCE="..\include\fopen-bin.h"
# End Source File
# Begin Source File

SOURCE=.\genlink.h
# End Source File
# Begin Source File

SOURCE=..\include\hicross.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\internal.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\internal.h
# End Source File
# Begin Source File

SOURCE=.\libaout.h
# End Source File
# Begin Source File

SOURCE=.\libbfd.h
# End Source File
# Begin Source File

SOURCE=.\libcbmap.h
# End Source File
# Begin Source File

SOURCE=.\libcoff.h
# End Source File
# Begin Source File

SOURCE=.\libecoff.h
# End Source File
# Begin Source File

SOURCE=.\libhicross.h
# End Source File
# Begin Source File

SOURCE=..\include\libiberty.h
# End Source File
# Begin Source File

SOURCE=..\include\objalloc.h
# End Source File
# Begin Source File

SOURCE=..\include\aout\ranlib.h
# End Source File
# Begin Source File

SOURCE=..\include\aout\stab.def
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\include\aout\stab_gnu.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\sym.h
# End Source File
# Begin Source File

SOURCE=.\sysdep.h
# End Source File
# Begin Source File

SOURCE=.\targmatch.h
# End Source File
# End Group
# End Target
# End Project
