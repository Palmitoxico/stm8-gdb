/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* BFD back-end for Crash-Barrier map files.
   Written by Michel Cazal.

This file is part of BFD, the Binary File Descriptor library.
*/

#include "bfd.h"
#include "sysdep.h"
#include "libbfd.h"
#include "libcbmap.h"


static  cbmap_symbol_type **prev_symbols_ptr;
static  unsigned int symbol_count = 0;
static  unsigned int external_symbol_max_index = 0;

#ifdef TARGET_ST18950
extern int use_windows;
#endif
#ifdef CRASH_BARRIER
int crash_barrier=0;
#endif

/************************ local processing functions ****************/

static asection *
DEFUN(retrieve_section,(abfd, 
          addr  
          ),
      bfd *abfd AND
      unsigned long addr
      )
{
  asection * sect = abfd->sections;

  while (sect) {
    if ((addr >= sect->vma) &&
       (addr < sect->vma + sect->_raw_size))
      return sect;
    sect = sect->next;
  }
  /* no section found, containing the symbol definition */
  return (asection *)&bfd_abs_section;
}

static cbmap_symbol_type *
DEFUN(get_symbol,(abfd, 
          cbmap,  
          last_symbol,
          symbol_count,
          pptr,
          max_index
          ),
      bfd *abfd AND
      cbmap_data_type *cbmap AND
      cbmap_symbol_type *last_symbol AND
      unsigned int *symbol_count AND
      cbmap_symbol_type *** pptr AND
      unsigned int *max_index
      )
{
    cbmap_symbol_type *  new_symbol =
      (cbmap_symbol_type *)bfd_alloc(abfd,
                                       sizeof(cbmap_symbol_type));

    ( *symbol_count)++;
    ** pptr= new_symbol;
    *pptr = &new_symbol->next;
    return new_symbol;
}


static void
DEFUN(cbmap_slurp_external_symbols,(abfd, bias, pt),
      bfd *abfd AND
      char *bias AND
      char **pt)
{
  cbmap_data_type *cbmap = CBMAP_DATA(abfd);
  cbmap_symbol_type  *symbol = (cbmap_symbol_type *)NULL;
  char *next;

  /* goto beginning of External Label List */
  while (strncmp (*pt, "Symbol Name", 11)) {
     next = strchr(*pt, '\n');
     if (!next) return;
     *pt = next + 1; /* skip to beginning of next line */
  }
  next = strchr(*pt, '\n');
  if (!next) return;
  *pt = next + 1; /* skip to beginning of next line */
  next = strchr(*pt, '\n');
  if (!next) return;
  *pt = next + 1; /* skip to beginning of next line */


  while (strncmp (*pt, "\n", 1) &&
         strncmp (*pt, "\r\n", 2)) {
      next = strchr(*pt, ' ');
      if (!next) break;
      *(next) = '\0'; /* isolate symbol name */
      symbol = get_symbol(abfd, cbmap, symbol, &symbol_count,
              &prev_symbols_ptr, 
              &external_symbol_max_index);
      
      symbol->symbol.the_bfd = abfd;
      symbol->symbol.flags = BSF_GLOBAL | BSF_EXPORT;
      symbol->symbol.name = bfd_alloc(abfd,strlen(*pt)+1);
      strcpy ((char *)symbol->symbol.name, *pt);
      *pt = next + 1; /* skip name */

      symbol->symbol.value = strtoul (*pt, pt, 16);

      while (**pt == ' ') (*pt)++; /* skip blanks until scope name */
      if (!strncmp (*pt, "BYTE", 4)) symbol->scope_length = 1;
      else if (!strncmp (*pt, "WORD", 4)) symbol->scope_length = 2;
      else symbol->scope_length = 4; 
      *pt = *pt + 4; /* skip scope name */

      while (**pt == ' ') (*pt)++; /* skip blanks until def name */
      next = strchr(*pt, '(');
      if (!next) break;
      symbol->linenumdef = strtoul (next + 1, NULL, 10);
      symbol->symbol.section = retrieve_section (abfd,
                                                 symbol->symbol.value);
      /* test if the symbol is declared in an INCLUDE */
      if (strncmp (*pt, symbol->symbol.section->name, next - *pt))
        symbol->linenumdef = 0; /* Remove wrong linenumdef info */
      symbol->symbol.udata.p = (PTR)NULL;

      next = strchr(*pt, '\n');
      if (!next) break;
      *pt = next + 1; /* skip to beginning of next line */

      if (!strncmp (*pt, "\n", 1))
         *pt = *pt + 1; /* We allow just one blank line separating symbols */
      else
	if (!strncmp (*pt, "\r\n", 2))
         *pt = *pt + 2; /* We allow just one blank line separating symbols */
  }

  abfd->symcount = symbol_count;

  *prev_symbols_ptr = (cbmap_symbol_type *)NULL;

}


long
DEFUN(cbmap_get_symtab_upper_bound,(abfd),
      bfd *abfd)
{
  return (abfd->symcount != 0) ? 
    (abfd->symcount+1) * (sizeof (cbmap_symbol_type *)) : 0;
return 0;
}

/* 
Move from our internal lists to the canon table, and insert in
symbol index order
*/

extern bfd_target cbmap_vec;
long
DEFUN(cbmap_get_symtab,(abfd, location),
      bfd *abfd AND
      struct symbol_cache_entry **location)
{
  cbmap_symbol_type *symp;
  unsigned int i = 0;

  if (abfd->symcount) 
  {
    cbmap_data_type *cbmap = CBMAP_DATA(abfd);

    for (symp = CBMAP_DATA(abfd)->external_symbols;
     symp != (cbmap_symbol_type *)NULL;
     symp = symp->next) {
      /* Place into table at correct index locations */
      location[i++] = &symp->symbol;

    }

   location[abfd->symcount] = (asymbol *)NULL;
  }

  return abfd->symcount;

}



/* This is the Crash Barrier map backend structure.  The backend field of the
 * target vector points to this.
 */

static const struct cbmap_backend_data cbmap_backend_data =
{
  /* Supported architecture.  */
  bfd_arch_st7
};


static asymbol *
DEFUN(cbmap_make_empty_symbol, (abfd),
      bfd*abfd)
{
  asymbol *new=  (asymbol *)bfd_zalloc (abfd, sizeof (asymbol));
  new->the_bfd = abfd;
  return new;
}


/* Default or unused standard target routines (see bfd.h) */
#define FOO PROTO
#define cbmap_core_file_failing_command (char *(*)())(bfd_nullvoidptr)
#define cbmap_core_file_failing_signal (int (*)())bfd_0
#define cbmap_core_file_matches_executable_p ( FOO(boolean, (*),(bfd *, bfd *)))bfd_false
#define cbmap_slurp_armap bfd_true
#define cbmap_slurp_extended_name_table bfd_true
#define cbmap_truncate_arname (void (*)())bfd_nullvoidptr
#define cbmap_write_armap  (FOO( boolean, (*),(bfd *, unsigned int, struct orl *, unsigned int, int))) bfd_nullvoidptr
#define cbmap_close_and_cleanup _bfd_generic_close_and_cleanup
#define cbmap_set_section_contents (FOO( boolean, (*),(bfd *, sec_ptr, PTR, file_ptr, bfd_size_type))) bfd_false
#define cbmap_get_section_contents (FOO( boolean, (*),(bfd *, sec_ptr, PTR, file_ptr, bfd_size_type))) bfd_false
#define cbmap_new_section_hook _bfd_dummy_new_section_hook
#define cbmap_get_reloc_upper_bound (FOO( unsigned int, (*),(bfd *, sec_ptr))) bfd_0u 
#define cbmap_canonicalize_reloc (FOO( unsigned int, (*),(bfd *, sec_ptr, arelent **,asymbol **))) bfd_0u
#define cbmap_print_symbol _bfd_nosymbols_print_symbol
#define cbmap_get_lineno _bfd_nosymbols_get_lineno
#define cbmap_bfd_is_local_label_name _bfd_nosymbols_bfd_is_local_label_name
#define cbmap_get_symbol_info _bfd_nosymbols_get_symbol_info
#define cbmap_set_arch_mach bfd_default_set_arch_mach
#define cbmap_openr_next_archived_file ( FOO(bfd *, (*),(bfd *, bfd *)))(bfd_nullvoidptr) 
#define cbmap_find_nearest_line _bfd_nosymbols_find_nearest_line
#define cbmap_generic_stat_arch_elt ( FOO(int, (*),(bfd *, struct stat *)))bfd_0
#define cbmap_sizeof_headers ( FOO(int, (*),(bfd *, boolean)))bfd_0
#define cbmap_bfd_debug_info_start bfd_void
#define cbmap_bfd_debug_info_end bfd_void
#define cbmap_bfd_debug_info_accumulate (FOO( void, (*),(bfd *, struct sec *))) bfd_void
#define cbmap_bfd_get_relocated_section_contents  bfd_generic_get_relocated_section_contents
#define cbmap_bfd_relax_section bfd_generic_relax_section
#define cbmap_bfd_reloc_type_lookup \
  ((CONST struct reloc_howto_struct *(*) PARAMS ((bfd *, bfd_reloc_code_real_type))) bfd_nullvoidptr)
#define cbmap_bfd_make_debug_symbol \
  ((asymbol *(*) PARAMS ((bfd *, void *, unsigned long))) bfd_nullvoidptr)
#define cbmap_bfd_link_hash_table_create _bfd_generic_link_hash_table_create
#define cbmap_bfd_link_add_symbols _bfd_generic_link_add_symbols
#define cbmap_bfd_final_link _bfd_generic_final_link
#define cbmap_bfd_is_local_label _bfd_nosymbols_bfd_is_local_label
#define cbmap_read_minisymbols _bfd_nosymbols_read_minisymbols
#define cbmap_minisymbol_to_symbol _bfd_nosymbols_minisymbol_to_symbol
#define cbmap_bfd_link_split_section _bfd_nolink_bfd_link_split_section 

/* Local function that computes the 'section start' symbol name
   from the section name. We cannot keep the original section
   name because some unauthorized character may appear like ':'...
   
   The choosen computation is : keep the base name and
   replace '.', '[', ']' by '_'
   
   ex: C:\TEST\CB\DEMO2.ASM[1] becomes _DEMO2_ASM_1_
*/
static void str_section_name (dest, src)
    char *dest;
    char *src;
{
  char *pt;

  pt = strrchr (src, '\\');
  if (pt == NULL)
    strcpy (dest, src);
  else
	{
      strcpy (dest, pt);
      *dest = '_';
	}
  /* eliminate unauthorized characters */
  while (pt = strchr (dest, '[')) *pt = '_';
  while (pt = strchr (dest, ']')) *pt = '_';
  while (pt = strchr (dest, '.')) *pt = '_';
}


/* Check format routine */

static asection *
DEFUN(get_section_entry,(abfd, index, name),
 bfd *abfd AND
      unsigned int index AND
      char * name)
{
  asection *section;
  char *tmp = bfd_alloc(abfd,strlen(name)+1);

  strcpy (tmp, name);
  section = bfd_make_section(abfd, tmp);
  if (section != NULL)
	{
      section->flags = SEC_NO_FLAGS | SEC_ALLOC;
      section->index = index;
	}
  return section;
}

static int
DEFUN(cbmap_slurp_sections,(abfd, bias, pt),
      bfd *abfd AND
      char *bias AND
      char **pt)
{
  asection *section = (asection *)NULL;
  char *name;
  unsigned int section_index = 0 ;
  cbmap_data_type *cbmap = CBMAP_DATA(abfd);
  cbmap_symbol_type  *symbol = (cbmap_symbol_type *)NULL;

  while (strncmp (*pt, "\n", 1) &&
         strncmp (*pt, "\r\n", 2)) { /* while not empty line */
      char *next;
      char *end;
      unsigned long lnum;
	  /* Parse lines from Segment List in the map file
		C:\stm7p applis\stm8\simple_stm8_prog1\main.asm[1]    4-   14         0 -        8                 'ram0'  [void]
		*/
      end = strchr(*pt, '\n');
      if (!end) break;
      *end = '\0'; /* isolate end of line */

      next = strchr(*pt, ']');
      if (!next) break;
      *(next+1) = '\0'; /* isolate section name */
      section = get_section_entry(abfd, section_index, *pt);
      if (section == NULL)
		return -1;
	  section->filepos = *pt - bias; /* set pos from beginning of file */
    
      *pt = next + 2; /* skip name */
      section->target_index = strtol (*pt, pt, 10); /* misused for line start */
      next = strchr(*pt, '-');
      if (!next) break;
      *pt = next + 1; /* skip '-' */
      lnum = strtoul (*pt, pt, 10);
      if (lnum > section->target_index)
        section->lineno_count = lnum - section->target_index; 
      else
        /* Protection against invalid map file info for old asm versions */
        section->lineno_count = 0;

      section->vma = strtoul (*pt, pt, 16);
      section->lma = section->vma;

      next = strchr(*pt, '-');
      if (!next) break;
      *pt = next + 1; /* skip the '-' */

      section->_raw_size = strtoul (*pt, pt, 16) - section->vma + 1;
#ifdef TARGET_ST18950
      section->vma <<= 1;
      section->_raw_size <<= 1;
#endif


#if 0 /* There is no need to build a symbol for each segment.
		Moreover it confuses the symbol browser.
		Strange symbols with segment names appear when displaying symbols
		using info functions or info variables.
		*/
      /* Now try to get the name of the section to build a symbol. */
      while (**pt == ' ') *pt = *pt + 1;

      symbol = get_symbol(abfd, cbmap, symbol, &symbol_count,
                          &prev_symbols_ptr, 
                          &external_symbol_max_index);
      symbol->symbol.the_bfd = abfd;
      symbol->symbol.flags = BSF_GLOBAL | BSF_EXPORT| BSF_WEAK | BSF_SECTION_SYM;
      symbol->symbol.value = section->vma;
      symbol->scope_length = 2;
      symbol->linenumdef = section->target_index;
      symbol->symbol.section = section;
      symbol->symbol.udata.p = (PTR)NULL;

#if 0 /* Note that the Crash-Barrier keyword is in fact "segment".
	     The segment name is not a unique identifier because a segment
		 can consist of several parts! */
      if (**pt == '\'') {
        /* a name is found, use it for the symbol */

        next = strchr(*pt, ' ');
        if (!next) break;
        *(next) = '\0'; /* isolate symbol name */

        symbol->symbol.name = bfd_alloc(abfd,strlen(*pt)+1);
        strcpy ((char *)symbol->symbol.name, *pt);
        *pt = next + 1;
      }
      else
#endif
	  {
		/* We use the first string of the line describing a segment
		   in the map file:
		   this string is obtained by suffixing the source file path name
		   with a counter which is incremented each time
		   the "segment" keyword is found in the assembly language source file.
		   This string is therefore unique. */
        char *secname = (char *) malloc (strlen (section->name) + 1);

		/* Remove the directory name and convert special characters into "_". */
        str_section_name (secname, section->name);
        symbol->symbol.name = bfd_alloc(abfd,strlen(secname)+1);
        strcpy ((char *)symbol->symbol.name, secname);
        free (secname);
      }
#endif

      /* Update CODE/DATA/ROM flag according to the "[...]" appearence */
      *pt = strchr(*pt, '[');
      if (!*pt)
      {
        /* for compatibility with old assembler version */
        section->flags |= SEC_CODE;
      }
      else
      { 
        next = *pt + 1;
        if (*next == 'v') /* [void] */ {
		  /* Non initialized variables */
          section->flags |= SEC_DATA;
        } else if (*next == 't') /* both [text] and [textdata] */ {
          section->flags |= SEC_CODE | SEC_ROM;
		  if (*(next+4) == 'd') /* [textdata] */ {
			  section->flags |= SEC_DATA;
		  }
        } else if (*next == 'd') /* [data] */ {
          section->flags |= SEC_DATA | SEC_ROM;
		}

        /* MISUSE of section->userdata :
        this is indicating that the current segment is
        declared in an include file */
        next = strchr (*pt, '<');
        if (next)
          section->userdata = (void *)1;
      }


      *pt = end + 1; /* skip to next line */
      section_index++;
    }
	return 0;
}


static boolean
DEFUN(cbmap_mkobject,(abfd),
      bfd *abfd)
{ 
abfd->tdata.cbmap_data =
   (cbmap_data_type *)bfd_zalloc(abfd,sizeof(cbmap_data_type));
  

  return true;
}

#define CBMAP_HEADER_SIZE_DOS 32
static const char cbmap_header_dos[] = "\r\nSegment List\r\n------------\r\n\r\n";
#define CBMAP_HEADER_SIZE_UNIX 28
static const char cbmap_header_unix[] = "\nSegment List\n------------\n\n";

const bfd_target *
DEFUN(cbmap_object_p,(abfd),
      bfd *abfd)
{
  cbmap_data_type *cbmap;
  cbmap_data_type *save = CBMAP_DATA(abfd);
  unsigned char buffer[300];
  unsigned long file_size;
  unsigned char *first_byte;
  unsigned char *pt_byte;
  int CBMAP_HEADER_SIZE;

  abfd->tdata.cbmap_data = 0;
  cbmap_mkobject(abfd);
  
  cbmap = CBMAP_DATA(abfd);

  /* Header parsing */
  bfd_seek(abfd, (file_ptr) 0, SEEK_SET);
  if (1 !=
      bfd_read((PTR)buffer, 1, 1, abfd))
    goto fail;

  if (buffer[0] == '\r')
    CBMAP_HEADER_SIZE = CBMAP_HEADER_SIZE_DOS;
  else
    CBMAP_HEADER_SIZE = CBMAP_HEADER_SIZE_UNIX;

  if ((CBMAP_HEADER_SIZE-1) !=
      bfd_read((PTR)(&buffer[1]), 1, CBMAP_HEADER_SIZE-1, abfd))
    goto fail;
  buffer[CBMAP_HEADER_SIZE] = 0;
  if (strcmp (buffer, cbmap_header_dos) &&
      strcmp (buffer, cbmap_header_unix)) goto fail;

  /* Now read in local memory the remaining bytes in the map file */
  file_size = bfd_get_size (abfd) - CBMAP_HEADER_SIZE;
#if 1
  first_byte = (unsigned char *) malloc(file_size);
  if (first_byte == NULL)
    goto fail;
#else
  first_byte = (unsigned char *) alloca(file_size);
#endif

  bfd_read((PTR)(first_byte), 1, file_size, abfd);
  pt_byte = first_byte;

  /* prepare symbol reading */
  prev_symbols_ptr = &cbmap->external_symbols;
  symbol_count = 0;
  external_symbol_max_index = 0;

  if (cbmap_slurp_sections(abfd, first_byte - CBMAP_HEADER_SIZE, &pt_byte) == -1)
	{
	  free (first_byte);
	  goto fail;
	}

  cbmap_slurp_external_symbols(abfd, first_byte - CBMAP_HEADER_SIZE, &pt_byte);

  abfd->flags = HAS_SYMS;


  free (first_byte);

#ifdef CRASH_BARRIER
  crash_barrier = 1;
#ifdef ENABLE_GDBTK
  if (use_windows)
     gdbtk_crash_barrier();
#endif
#endif

  return abfd->xvec;

fail:
  bfd_release(abfd, cbmap);
  abfd->tdata.cbmap_data = save;
  return (bfd_target *)NULL;
}

/* cbmap_vec initialization */

bfd_target cbmap_vec =
{
  "cbmap",           /* name */
  bfd_target_cbmap_flavour,
  BFD_ENDIAN_BIG,              /* target byte order */
  BFD_ENDIAN_BIG,              /* target headers byte order */
  (HAS_RELOC | EXEC_P |        /* object flags */
   HAS_LINENO | HAS_DEBUG |
   HAS_SYMS | HAS_LOCALS | WP_TEXT | D_PAGED),
  ( SEC_CODE|SEC_DATA|SEC_ROM|SEC_HAS_CONTENTS
   |SEC_ALLOC | SEC_LOAD | SEC_RELOC), /* section flags */
   0,                /* no leading underscore */
  ' ',               /* ar_pad_char */
  16,                /* ar_max_namelen */
#if 0 /* Does not exist in gdb4.16 */
    1,               /* minimum alignment */
#endif
bfd_getb64, bfd_getb_signed_64, bfd_putb64,
    bfd_getb32, bfd_getb_signed_32, bfd_putb32,
    bfd_getb16, bfd_getb_signed_16, bfd_putb16, /* data */
bfd_getb64, bfd_getb_signed_64, bfd_putb64,
    bfd_getb32, bfd_getb_signed_32, bfd_putb32,
    bfd_getb16, bfd_getb_signed_16, bfd_putb16, /* hdrs */

  { _bfd_dummy_target,
     cbmap_object_p,        /* bfd_check_format */
     _bfd_dummy_target,
    _bfd_dummy_target,
     },
  {
    bfd_false,
    bfd_false, 
    bfd_false,
    bfd_false
    },
  {
    bfd_false,
    bfd_false,
    bfd_false,
    bfd_false,
  },
   BFD_JUMP_TABLE_GENERIC (_bfd_generic),
   BFD_JUMP_TABLE_COPY (_bfd_generic),
   BFD_JUMP_TABLE_CORE (_bfd_nocore),
   BFD_JUMP_TABLE_ARCHIVE (_bfd_noarchive),
   BFD_JUMP_TABLE_SYMBOLS (cbmap),
   BFD_JUMP_TABLE_RELOCS (_bfd_norelocs),
   BFD_JUMP_TABLE_WRITE (_bfd_generic),
   BFD_JUMP_TABLE_LINK (cbmap),
   BFD_JUMP_TABLE_DYNAMIC (_bfd_nodynamic),
  (PTR) &cbmap_backend_data
};

