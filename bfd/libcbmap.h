/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* Crash Barrier map file formats:  definitions internal to BFD.
This file is part of BFD, the Binary File Descriptor library. */



/* This is the backend information kept for Crash Barrier map files.  This
   structure is constant for a particular backend. */

#define CBMAP_BACKEND(abfd) \
  ((struct cbmap_backend_data *) (abfd)->xvec->backend_data)

struct cbmap_backend_data
{
  /* Supported architecture.  */
  enum bfd_architecture arch;
};


 /* Each canonical asymbol really looks like this: */

typedef struct cbmap_symbol 
{
  asymbol symbol;
  struct cbmap_symbol *next;
  /* The def linenumber information for this symbol */
  unsigned int linenumdef;
  /* The length for this symbol */
  unsigned int scope_length;
} cbmap_symbol_type;


typedef struct cbmap_data_struct
{
  /* List of GLOBAL symbols */
  cbmap_symbol_type *external_symbols;
} cbmap_data_type;

/* We take the address of the first element of a asymbol to ensure that the
 * macro is only ever applied to an asymbol.  */
#define cbmapsymbol(asymbol) ((cbmap_symbol_type *)(&((asymbol)->the_bfd)))

extern alent *cbmap_get_lineno PARAMS ((bfd *, asymbol *));


#define CBMAP_DATA(abfd) ((abfd)->tdata.cbmap_data)
