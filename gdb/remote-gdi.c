/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* Generic remote debugging interface for OMI-GDI.
   Made by Michel CAZAL Dec 3th, 1998
  */


#ifdef WIN32
#include <windows.h>
#define EVAL(NAME) NAME
#define PARAMETERS(parameterlist)
#define RETURN_TYPE FARPROC
#else
#define EVAL(NAME) (* ## NAME ## )
#define PARAMETERS(parameterlist) parameterlist
#define RETURN_TYPE DiReturnT OmiGdiExport	
#include <dlfcn.h>
#endif


#include "gdi.h"
#include "defs.h"
#include "inferior.h"
#include "value.h"
#include "gdb_string.h"
#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <signal.h>
#include <setjmp.h>
#include <errno.h>
#include "terminal.h"
#include "target.h"
#include "gdbcore.h"
#include "remote-utils.h"
#include "command.h"
#include <string.h>
#include "gdbcmd.h"
#include "st7-utils.h"
#include "symfile.h"
#include "objfiles.h"
#include "../opcodes/ins7.h"
#include "top.h"

#ifdef TARGET_STM7
static unsigned short addr_size = 3;
#endif

#ifdef HOST_STVISUAL /* user interrupt thread handling */
#if 0
extern HANDLE  hGdiApiMutex;
#endif
#endif /* HOST_STVISUAL */

#ifdef EXIT_CALLBACK
#include "critical-section.h"
#endif

extern struct cmd_list_element	*cmdlist;

#define SKIP_BLANK(p)	{while isspace(*(p)) (p)++;}
#define SKIP_LETTERS(p)	{while (*(p) && ! isspace (*(p))) (p)++;}

#define SKIP_TO_END_OF_ARG(from,end)   				\
 {								\
    for ((end) = (from);					\
	isalnum (*(end)) || *(end) == '_' || *(end) == ':';	\
	(end)++);						\
  }

static DiFeaturesT features;
static char gdi_current_micro[256]; /* remember current configuration set */

int gdi_spy = 0;
static char *gdi_spy_file_name;
FILE *gdi_spy_stream = NULL;

/* memory cache buffer for improving x command */
#define CACHE_SIZE_MAX 1024
static char cache_enabled = 0;
static char cache_valid = 0;
static int cache_size = 1;
static char cache_buffer[CACHE_SIZE_MAX];
static CORE_ADDR cache_startaddr = 0;

#define ST_INFERIOR_PID 42

/* Keyword recognized by STVD. */
static char *error_prefix = STVD_ERROR_PREFIX;

/* Default: try to guess call type if no debug information on call type is found. */
/* Useful for STM7 and STM8 which have two types of call: near call (call) and far call (callf). */
int guess_call_type = 1;

int check_disassembly = 0;

void disable_memory_cache(void)
{
	cache_enabled = 0;
	cache_valid = 0;
}

static int compute_optimal_cache_size(int size)
{
	int cache_size;
	if (size > CACHE_SIZE_MAX) {
		int ratio = size / CACHE_SIZE_MAX;
		int rest = size % CACHE_SIZE_MAX;
		if (rest == 0) {
			cache_size = CACHE_SIZE_MAX;
		} else {
			cache_size = size / (ratio + 1);
			if (size % (ratio + 1) != 0) {
				cache_size++;
			}
		}
	} else {
		cache_size = size;
	}
	if (cache_size == 1) {
		cache_enabled = 0;
	}
	return cache_size;
}

void enable_memory_cache(int size)
{
	cache_enabled = 1;
	cache_valid = 0;
	cache_size = compute_optimal_cache_size(size);
}

void cleanup_marker (char * foo)
{
}

static int no_other_separator(int c)
{	
  return 0;
}

static void standard_error PARAMS ((DiReturnT returnStatus, DiStringT functionName))
{
  switch (returnStatus)
    {
    case DI_ERR_STATE:
      error("state error in %s", functionName);
      break;
    case DI_ERR_PARAM:
      error("invalid %s parameters", functionName);
      break;
    case DI_ERR_COMMUNICATION:
      error("communication error in %s", functionName);
      break;
    case DI_ERR_NOTSUPPORTED:
      error("%s not supported", functionName);
      break;
    case DI_ERR_NONFATAL:
      error("non fatal error in %s", functionName);
      break;
    case DI_ERR_CANCEL:
      error("%s cancelled", functionName);
      break;
    case DI_ERR_FATAL:
      error("fatal error in %s", functionName);
      break;
    case DI_ERR_WARNING:
      error("warning in %s", functionName);
      break;
    default:
      error("unknown error in %s", functionName);
      break;
    }
}

static DiReturnT di_get_error_message PARAMS ((DiStringT *errorMsg))
{
	DiReturnT returnStatus;

	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, ">DiErrorGetMessage\n");
		fflush(gdi_spy_stream);
	}
	returnStatus = DiErrorGetMessage(errorMsg);
	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, "<DiErrorGetMessage %d pszErrorMsg=%s\n",
				returnStatus, *errorMsg);
		fflush(gdi_spy_stream);
	}
	return returnStatus;
}

static void error_handler PARAMS ((DiReturnT returnStatus, DiStringT functionName))
{
	DiStringT errorMsg;
	
	if (returnStatus != DI_OK)
    {
		DiReturnT errorGetMessageReturnStatus;
		
		errorGetMessageReturnStatus = di_get_error_message (&errorMsg);
		
		if (errorGetMessageReturnStatus != DI_OK)
			standard_error(errorGetMessageReturnStatus, "DiErrorGetMessage");
		else
		{
			if (errorMsg == NULL || strcmp(errorMsg, "") == 0)
				standard_error(returnStatus, functionName);
			else if (returnStatus == DI_ERR_WARNING)
				warning("%s", errorMsg);
			else
				error("%s", errorMsg);
		}
    }
}

static void ignore PARAMS ((void))
{
}

static void *handle;
static void bind_all_gdi_symbols PARAMS ((void));

#ifdef WIN32

#include "win32-gdidll-funcnames.h"

static void open_gdi_dll PARAMS ((char *dll_name))
{
  handle = LoadLibrary(dll_name);
  if (handle == NULL)
    {
	  LPVOID lpMsgBuf;

	  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL,
					GetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPSTR) &lpMsgBuf,
					0,
					NULL);
	  error("can't open %s : %s", dll_name, lpMsgBuf);
    }
  bind_all_gdi_symbols();
}

static void *bind_symbol PARAMS ((void *handle, char *symbol_name))
{
  void *function;

  function = GetProcAddress(handle, symbol_name);
  if (function == NULL)
    {
      error("can't find %s\n", symbol_name);
    }
  return function;
}

static int close_gdi_dll PARAMS ((void))
{
  int returnStatus;

  returnStatus = FreeLibrary(handle);
  if (returnStatus == 0)
    {
      error("can't close %x", handle);
      return -1;
    }
  else
    return 0;
}
#else

#include "unix-gdidll-funcnames.h"

static int open_gdi_dll PARAMS ((char *dll_name))
{
  handle = dlopen(dll_name, 1);
  if (handle == NULL)
    {
      printf_filtered("%s\n", dlerror());
      error("can't open %s", dll_name);
      return -1;
    }
  bind_all_gdi_symbols();
}

static void *bind_symbol PARAMS ((void *handle, char *symbol_name))
{
  void *function;

  function = dlsym(handle, symbol_name);
  if (function == NULL)
    {
      printf_filtered("%s\n", dlerror());
      error("can't find %s\n", symbol_name);
    }
  return function;
}

static int close_gdi_dll PARAMS ((void))
{
  int returnStatus;

  returnStatus = dlclose(handle);
  if (returnStatus != 0)
    {
      printf_filtered("%s\n", dlerror());
      error("can't close %x", handle);
      return -1;
    }
  else
    return 0;
}
#endif

static void bind_all_gdi_symbols PARAMS ((void))
{
  DiGdiOpen = bind_symbol(handle, DiGdiOpenExportedName);
  DiGdiClose = bind_symbol(handle, DiGdiCloseExportedName);
  DiGdiGetFeatures = bind_symbol(handle, DiGdiGetFeaturesExportedName);
  DiGdiSetConfig = bind_symbol(handle, DiGdiSetConfigExportedName);
  DiGdiInitIO = bind_symbol(handle, DiGdiInitIOExportedName);
  DiGdiInitRegisterMap = bind_symbol(handle, DiGdiInitRegisterMapExportedName);
  DiGdiInitMemorySpaceMap = bind_symbol(handle, DiGdiInitMemorySpaceMapExportedName);
  DiGdiAddCallback = bind_symbol(handle, DiGdiAddCallbackExportedName);
  DiGdiCancel = bind_symbol(handle, DiGdiCancelExportedName);
  DiGdiSynchronize = bind_symbol(handle, DiGdiSynchronizeExportedName);
  DiDirectAddMenuItem = bind_symbol(handle, DiDirectAddMenuItemExportedName);
  DiDirectCommand = bind_symbol(handle, DiDirectCommandExportedName);
  DiDirectReadNoWait = bind_symbol(handle, DiDirectReadNoWaitExportedName);
  DiErrorGetMessage = bind_symbol(handle, DiErrorGetMessageExportedName);
  DiMemorySetMap = bind_symbol(handle, DiMemorySetMapExportedName);
  DiMemoryGetMap = bind_symbol(handle, DiMemoryGetMapExportedName);
  DiMemorySetCpuMap = bind_symbol(handle, DiMemorySetCpuMapExportedName);
  DiMemoryGetCpuMap = bind_symbol(handle, DiMemoryGetCpuMapExportedName);
  DiMemoryDownload = bind_symbol(handle, DiMemoryDownloadExportedName);
  DiMemoryWrite = bind_symbol(handle, DiMemoryWriteExportedName);
  DiMemoryRead = bind_symbol(handle, DiMemoryReadExportedName);
  DiRegisterWrite = bind_symbol(handle, DiRegisterWriteExportedName);
  DiRegisterRead = bind_symbol(handle, DiRegisterReadExportedName);
  DiRegisterClassCreate = bind_symbol(handle, DiRegisterClassCreateExportedName);
  DiRegisterClassDelete = bind_symbol(handle, DiRegisterClassDeleteExportedName);
  DiRegisterClassWrite = bind_symbol(handle, DiRegisterClassWriteExportedName);
  DiRegisterClassRead = bind_symbol(handle, DiRegisterClassReadExportedName);
  DiBreakpointSet = bind_symbol(handle, DiBreakpointSetExportedName);
  DiBreakpointClear = bind_symbol(handle, DiBreakpointClearExportedName);
  DiBreakpointClearAll = bind_symbol(handle, DiBreakpointClearAllExportedName);
  DiExecResetChild = bind_symbol(handle, DiExecResetChildExportedName);
  DiExecSingleStep = bind_symbol(handle, DiExecSingleStepExportedName);
  DiExecContinueUntil = bind_symbol(handle, DiExecContinueUntilExportedName);
  DiExecContinue = bind_symbol(handle, DiExecContinueExportedName);
  DiExecContinueBackground = bind_symbol(handle, DiExecContinueBackgroundExportedName);
  DiExecGetStatus = bind_symbol(handle, DiExecGetStatusExportedName);
  DiExecStop = bind_symbol(handle, DiExecStopExportedName);
  DiCommGetAcceptableSettings = bind_symbol(handle, DiCommGetAcceptableSettingsExportedName);
  DiTraceSwitchOn = bind_symbol(handle, DiTraceSwitchOnExportedName);
  DiTraceGetInstructions = bind_symbol(handle, DiTraceGetInstructionsExportedName);
  DiTracePrintRawInfo = bind_symbol(handle, DiTracePrintRawInfoExportedName);
  DiTraceGetNrOfNewFrames = bind_symbol(handle, DiTraceGetNrOfNewFramesExportedName);
  DiCoverageSwitchOn = bind_symbol(handle, DiCoverageSwitchOnExportedName);
  DiCoverageGetInfo = bind_symbol(handle, DiCoverageGetInfoExportedName);
  DiProfilingSwitchOn = bind_symbol(handle, DiProfilingSwitchOnExportedName);
  DiProfileGetInfo = bind_symbol(handle, DiProfileGetInfoExportedName);
  DiStateOpen = bind_symbol(handle, DiStateOpenExportedName);
  DiStateSave = bind_symbol(handle, DiStateSaveExportedName);
  DiStateRestore = bind_symbol(handle, DiStateRestoreExportedName);
  DiStateClose = bind_symbol(handle, DiStateCloseExportedName);
  DiMeeEnumExecEnv = bind_symbol(handle, DiMeeEnumExecEnvExportedName);
  DiMeeConnect = bind_symbol(handle, DiMeeConnectExportedName);
  DiMeeGetFeatures = bind_symbol(handle, DiMeeGetFeaturesExportedName);
  DiMeeInitIO = bind_symbol(handle, DiMeeInitIOExportedName);
  DiMeeSelect = bind_symbol(handle, DiMeeSelectExportedName);
  DiMeeDisconnect = bind_symbol(handle, DiMeeDisconnectExportedName);
  DiCpuSelect = bind_symbol(handle, DiCpuSelectExportedName);
  DiCpuCurrent = bind_symbol(handle, DiCpuCurrentExportedName);
  DiGdiVersion = bind_symbol(handle, DiGdiVersionExportedName);
  DiProcess = bind_symbol(handle, DiProcessExportedName);
}

unsigned int debug_instrument_opened = 0;

void print_gdi_features PARAMS ((DiReturnT returnStatus))
{
  int i;

  fprintf(gdi_spy_stream, "<DiGdiGetFeatures %d\n", returnStatus);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->szIdentification=%s\n", features.szIdentification);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->szVersion=%s\n", features.szVersion);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnVersion=0x%x\n", features.dnVersion);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->pszConfig=[");
  for (i = 0; i < features.dnConfigArrayItems; i++)
    {
      if (i != 0) fprintf(gdi_spy_stream, " ");
      fprintf(gdi_spy_stream, "\"%s\"", features.pszConfig[i]);
    }
  fprintf(gdi_spy_stream, "]\n");
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnConfigArrayItems=%d\n", features.dnConfigArrayItems);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dccIOChannel=0x%x\n", features.dccIOChannel);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fMemorySetMapAvailable=%d\n", features.fMemorySetMapAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fMemorySetCpuMapAvailable=%d\n", features.fMemorySetCpuMapAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->pszMemoryType=[");
  for (i = 0; i < features.dnMemTypeArrayItems; i++)
    {
      if (i != 0) fprintf(gdi_spy_stream, " ");
      fprintf(gdi_spy_stream, "\"%s\"", features.pszMemoryType[i]);
    }
  fprintf(gdi_spy_stream, "]\n");
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnMemTypeArrayItems=%d\n", features.dnMemTypeArrayItems);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fEnableReadaheadCache=%d\n", features.fEnableReadaheadCache);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fTimerInCycles=%d\n", features.fTimerInCycles);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnTimerResolutionMantissa=%d\n", features.dnTimerResolutionMantissa);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->ddfDownloadFormat=0x%x\n", features.ddfDownloadFormat);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->ddfAuxDownloadFormat=0x%x\n", features.ddfAuxDownloadFormat);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fAuxiliaryDownloadPathAvailable=%d\n", features.fAuxiliaryDownloadPathAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dcCallback=0x%x\n", features.dcCallback);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fRegisterClassSupport=%d\n", features.fRegisterClassSupport);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fSingleStepSupport=%d\n", features.fSingleStepSupport);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fContinueUntilSupport=%d\n", features.fContinueUntilSupport);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fContinueBackgroundSupport=%d\n", features.fContinueBackgroundSupport);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnNrCodeBpAvailable=%d\n", features.dnNrCodeBpAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnNrDataBpAvailable=%d\n", features.dnNrDataBpAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fExecFromCodeBp=%d\n", features.fExecFromCodeBp);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fExecFromDataBp=%d\n", features.fExecFromDataBp);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fUnifiedBpLogic=%d\n", features.fUnifiedBpLogic);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fExecCycleCounterAvailable=%d\n", features.fExecCycleCounterAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fExecTimeAvailable=%d\n", features.fExecTimeAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fInstrTraceAvailable=%d\n", features.fInstrTraceAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fRawTraceAvailable=%d\n", features.fRawTraceAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fCoverageAvailable=%d\n", features.fCoverageAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fProfilingAvailable=%d\n", features.fProfilingAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fStateSaveRestoreAvailable=%d\n", features.fStateSaveRestoreAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnStateStoreMaxIndex=%d\n", features.dnStateStoreMaxIndex);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->pdbgBackground=[");
  for (i = 0; i < features.dnBackgroundArrayItems; i++)
    {
      if (i != 0) fprintf(gdi_spy_stream, " ");
      fprintf(gdi_spy_stream, "\"%s\"", features.pdbgBackground[i]);
    }
  fprintf(gdi_spy_stream, "]\n");
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnBackgroundArrayItems=%d\n", features.dnBackgroundArrayItems);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fDirectDiAccessAvailable=%d\n", features.fDirectDiAccessAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fApplicationIOAvailable=%d\n", features.fApplicationIOAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fKernelAware=%d\n", features.fKernelAware);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fMeeAvailable=%d\n", features.fMeeAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnNrCpusAvailable=%d\n", features.dnNrCpusAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->deWordEndianness=%d\n", features.deWordEndianness);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->dnNrHardWareCodeBpAvailable=%d\n", features.dnNrHardWareCodeBpAvailable);
  fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->fCodeHardWareBpSkids=%d\n", features.fCodeHardWareBpSkids);
  if (features.pReserved != NULL)
	fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->pReserved=\"%s\"\n", features.pReserved);
  else	
	fprintf(gdi_spy_stream, "<DiGdiGetFeatures pdfFeatures->pReserved=NULL\n");
}

/*      Address conversion utilities between gdb and gdi       */
#define DI_MS_MEMORY_SPACE 0x0000

void CORE2DiAddrT (CORE_ADDR ca, DiAddrT *dia)
{
  int i;
  dia->dmsMemSpace = DI_MS_MEMORY_SPACE;
  /* ### TO BE FIXED: Assume DI target is BIG_ENDIAN */
  for (i = 0; i < cbDiValueT - sizeof (CORE_ADDR); i++)
    dia->dlaLinAddress.v.val[i] = 0;
  for (i = cbDiValueT - 1; i >= cbDiValueT - sizeof (CORE_ADDR); i--)
  {
	dia->dlaLinAddress.v.val[i] = ca & 0xff;
	ca = ca / 256;
  }
}

void DiAddrT2CORE (DiAddrT *dia, CORE_ADDR *ca)
{
  int i;
  dia->dmsMemSpace = DI_MS_MEMORY_SPACE;
  /* ### TO BE FIXED: Assume DI target is BIG_ENDIAN */
  *ca = 0;
  for (i = cbDiValueT - sizeof (CORE_ADDR); i < cbDiValueT; i++)
     *ca = (*ca * 256) + dia->dlaLinAddress.v.val[i];
}

/* gdi callbacks */


/* DI_CB_EVAL_EXPR callback */
void gdicb_eval_expr (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	
	DiStringT szExpr;
	pDiExprResultT pderResult;
	DiReturnT *pdrStat;
	
	struct expression *expr;
	register value_ptr val;
	struct type *type;
	
	va_start (args, dcCallbackType);
	szExpr = va_arg (args, DiStringT);
	pderResult = va_arg (args, pDiExprResultT);
	pdrStat = va_arg (args, DiReturnT *);
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">>DI_CB_EVAL_EXPR Callback=0x%x dcCallbackType=0x%x szExpr=%s\n",
				gdicb_eval_expr, dcCallbackType, szExpr);
		fflush(gdi_spy_stream);
    }
	
	
	expr = parse_expression (szExpr);
	val = evaluate_expression (expr);
	type = VALUE_TYPE (val);
	
	*pdrStat = DI_OK;
	if (TYPE_CODE (type) == TYPE_CODE_PTR)
	{
		CORE_ADDR ca;
		pderResult->dtType = DI_TYPE_ADDR;
		ca = unpack_long (type, VALUE_CONTENTS(val));
		CORE2DiAddrT (ca, &(pderResult->u.daAddr));
	}
	else if (TYPE_CODE (type) == TYPE_CODE_INT)
	{
		int i;
		unsigned long value;
		pderResult->dtType = DI_TYPE_VALUE;
		/* ### TO BE FIXED: Assume DI target is BIG_ENDIAN */
		value = unpack_long (type, VALUE_CONTENTS(val));
		for (i = 0; i < cbDiValueT - sizeof (long); i++)
			pderResult->u.dvValue.val[i] = 0;
		for (i = cbDiValueT - 1; i >= cbDiValueT - sizeof (long); i--)
		{
			pderResult->u.dvValue.val[i] = value % 256;
			value = value / 256;
		}
	}
	else
		*pdrStat = DI_ERR_FATAL;
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				"<<DI_CB_EVAL_EXPR Callback drStat=%d derResult.dtType=%d derResult.u.dvValue=%x\n",
				*pdrStat, pderResult->dtType, pderResult->u.dvValue);
		fflush(gdi_spy_stream);
    }
	va_end (args);
}

extern void write_printf_pipe(char *message);

#define VSPRINTF_SIZE_MAX 1024

/* DI_CB_DEBUG callback */
/*
	Don't use vasprintf otherwise the robustness test doesn't pass.
	At some point, there is a problem in a free:
	- in gdicb_debug if gdb7 is used alone,
	- in command_line_input (get_execution_status) if gdb7 is launched by STVD7.
*/
static void
gdicb_debug (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	DiStringT vsprintf_format;
	va_list vsprintf_varargs;
	char printf_string[VSPRINTF_SIZE_MAX+1];
	int n;

	if (dcCallbackType != DI_CB_DEBUG)
		error("Bad gdicb_debug argument: dcCallbackType=%d.", dcCallbackType);
	
	va_start(args, dcCallbackType);
	vsprintf_format = va_arg(args, DiStringT);
	vsprintf_varargs = va_arg(args, va_list);
	n = _vsnprintf(printf_string, VSPRINTF_SIZE_MAX, vsprintf_format, vsprintf_varargs);
	printf_string[VSPRINTF_SIZE_MAX] = '\0';
	if (gdi_spy) {
		fprintf(gdi_spy_stream,
				">>DI_CB_DEBUG Callback=0x%x dcCallbackType=0x%x \"%s\"\n",
				gdicb_debug, dcCallbackType, printf_string);
		fflush(gdi_spy_stream);
    }
	write_printf_pipe(printf_string);
	va_end(args);
}

#ifdef EXIT_CALLBACK
static int exit_request = 0;
static HANDLE hThreadHandle = 0;

DWORD start_exit_command (LPVOID lpParameter)
{
	if (gdi_spy) {
		fprintf(gdi_spy_stream, "gdicb_exitrequest:start_exit_command:EnterGlobalCriticalSection\n");
		fflush(gdi_spy_stream);
    }
    EnterGlobalCriticalSection();
	if (gdi_spy) {
		fprintf(gdi_spy_stream, "gdicb_exitrequest:start_exit_command:quit_force\n");
		fflush(gdi_spy_stream);
    }
    quit_force((char *)0, 0);
	exit_request = 0;
	if (gdi_spy) {
		fprintf(gdi_spy_stream, "gdicb_exitrequest:start_exit_command:LeaveGlobalCriticalSection\n");
		fflush(gdi_spy_stream);
    }
    LeaveGlobalCriticalSection();
	return TRUE;
}

#define CREATE_EXIT_CALLBACK_THREAD

/* DI_CB_EXITREQUEST callback */
/* Normally this callback should just set a global variable indicating that an exit request has been
   made by the GDI DLL.
   It would be cleaner to perform the quit_command in the command_loop function within the main gdb thread
   but it would require more modifications in gdb core files to make sure that gdb can be stopped
   even if it is waiting for a character to be typed in the input console.
   It is simpler to perform the quit_command within the DI_CB_EXITREQUEST callback
   and use a critical section to avoid to stop gdb while executing a user command.
   It will work because this callback will be executed in a separate thread.
   The DSA configurator launches a new thread within gdb7 using CreateRemoteThread
   but it is cleaner to also create a thread within gdicb_exitrequest
   in case it is used in a different context. */
static void
gdicb_exitrequest (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	DiUInt16T fSystemError;
	DiStringT vsprintf_format;
	va_list vsprintf_varargs;
	char printf_string[VSPRINTF_SIZE_MAX+1];
	int n;
	DWORD dwThreadId;

	if (dcCallbackType != DI_CB_EXITREQUEST)
		error("Bad gdicb_exitrequest argument: dcCallbackType=%d.", dcCallbackType);

	va_start(args, dcCallbackType);
	fSystemError = va_arg (args, DiUInt16T);
	if (fSystemError) {
		write_printf_pipe("System error!\n");
	} else {
		write_printf_pipe("Internal error!\n");
	}
	vsprintf_format = va_arg(args, DiStringT);
	vsprintf_varargs = va_arg(args, va_list);
	n = _vsnprintf(printf_string, VSPRINTF_SIZE_MAX, vsprintf_format, vsprintf_varargs);
	printf_string[VSPRINTF_SIZE_MAX] = '\0';
	if (gdi_spy) {
		fprintf(gdi_spy_stream,
				">>DI_CB_EXITREQUEST Callback=0x%x dcCallbackType=0x%x fSystemError=%d \"%s\"\n",
				gdicb_exitrequest, dcCallbackType, fSystemError, printf_string);
		fflush(gdi_spy_stream);
    }
	write_printf_pipe(printf_string);
	va_end(args);  

	if (exit_request) {
		if (gdi_spy) {
			fprintf(gdi_spy_stream, "Additional exit request ignored!\n");
			fflush(gdi_spy_stream);
		}
		return;
	}
	exit_request = 1;
#ifdef CREATE_EXIT_CALLBACK_THREAD
	if (gdi_spy) {
		fprintf(gdi_spy_stream, "CreateThread(start_exit_command)\n");
		fflush(gdi_spy_stream);
	}
	/* Create the thread that will cause gdb to exit. */
	hThreadHandle = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) start_exit_command, NULL, 0, &dwThreadId);
	if (hThreadHandle == NULL) {
		warning("The exit thread has not been created!\n");
	} else {
		CloseHandle(hThreadHandle);
	}
#else
	start_exit_command(NULL);
#endif
}
#endif

extern void begin_progress_bar(char *message);
extern void advance_progress_bar(int progress_indicator);
extern void end_progress_bar(void);

static int feedback_maximum_value;
static int feedback_start = 0;

/* DI_CB_FEEDBACK callback */
static void
gdicb_feedback (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	DiFeedbackCommandT cmd;
	DiReturnT *pdrStat;
	DiUInt32T numArg;
	DiStringT szLabel;
	
	if (dcCallbackType != DI_CB_FEEDBACK)
		error("Bad gdicb_feedback argument: dcCallbackType=%d.", dcCallbackType);
	
	va_start(args, dcCallbackType);
	cmd = va_arg(args, DiFeedbackCommandT);
	pdrStat = va_arg(args, DiReturnT *);
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
			">>DI_CB_FEEDBACK Callback=0x%x dcCallbackType=0x%x ",
			gdicb_feedback, dcCallbackType);
    }
	
	switch (cmd)
	{
	case DI_FBC_START:
		numArg = va_arg(args, DiUInt32T);
		szLabel = va_arg(args, DiStringT);
		if (gdi_spy)
		{
			fprintf(gdi_spy_stream,
					"cmd=DI_FBC_START numArg=%d szLabel=%s\n", numArg, szLabel);
			fflush(gdi_spy_stream);
		}
		if (numArg <= 0)
			error("Bad gdicb_feedback argument: DI_FBC_START numArg=%d.", numArg);
		else
			feedback_maximum_value = numArg;
		feedback_start = 1;
		if (is_progress_bar_display_enabled())
			begin_progress_bar(szLabel);
		break;

	case DI_FBC_TICK:
		numArg = va_arg(args, DiUInt32T);
		szLabel = va_arg(args, DiStringT);
		if (gdi_spy)
		{
			if (szLabel != NULL)
				fprintf(gdi_spy_stream,
						"cmd=DI_FBC_TICK numArg=%d szLabel=%s\n", numArg, szLabel);
			else
				fprintf(gdi_spy_stream, "cmd=DI_FBC_TICK numArg=%d\n", numArg);
			fflush(gdi_spy_stream);
		}
		if (!feedback_start)
			error("No gdicb_feedback start.");
		if (numArg <= 0 || numArg > feedback_maximum_value)
			error("Bad gdicb_feedback argument: DI_FBC_TICK numArg=%d.", numArg);
		if (is_progress_bar_display_enabled())
		{
			if (szLabel != NULL) begin_progress_bar(szLabel);
			advance_progress_bar((100 * numArg) / feedback_maximum_value);
		}
		break;
		
	case DI_FBC_STOP:
		if (gdi_spy)
		{
			fprintf(gdi_spy_stream, "cmd=DI_FBC_STOP\n");
			fflush(gdi_spy_stream);
		}
		if (!feedback_start)
			error("No gdicb_feedback start.");
		feedback_start = 0;
		if (is_progress_bar_display_enabled())
			end_progress_bar();
		break;
		
	default:
		error("Bad gdicb_feedback argument: cmd=%d.", cmd);
	}

	*pdrStat = DI_OK;
	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, "<<DI_CB_FEEDBACK Callback drStat=%d\n", *pdrStat);
		fflush(gdi_spy_stream);
	}
	va_end (args);
}

unsigned int external_hardware_breakpoint_number = 0;

/* Begin ICD */
/* This variable is used to store the number of external breakpoints
   that have been set without GDB knowing it by a GDI direct access command
   such as:
   gdi <ICD break command>
*/

/* DI_CB_EXTERNBPSET callback */
static void
gdicb_externbpset (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	
	DiBpCommandT cmd;
	DiUInt32T dnBpId;
	DiBpT dbBreakpointInfo;
	DiStringT szMsg;
	DiBoolT fUdOriginatedBp;
	DiReturnT *pdrStatus;
	
	va_start (args, dcCallbackType);
	cmd = va_arg (args, DiBpCommandT);
	dnBpId = va_arg (args, DiUInt32T);
	dbBreakpointInfo = va_arg (args, DiBpT);
	szMsg = va_arg (args, DiStringT);
	fUdOriginatedBp = va_arg (args, DiBoolT);
	pdrStatus = va_arg (args, DiReturnT *);
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">>DI_CB_EXTERNBPSET Callback=0x%x dcCallbackType=0x%x cmd=%s \
dnBpId=%d dbBreakpoint={dbtBpType=%x} szMsg=%s fUdOriginatedBp=%d\n",
				gdicb_externbpset,
				dcCallbackType,
				(cmd == DI_BREAKPOINT_SET ? "set" : "remove"),
				dnBpId,
				dbBreakpointInfo.dbtBpType,
				(szMsg == NULL ? "" : szMsg),
				fUdOriginatedBp);
		fflush(gdi_spy_stream);
    }
	
	*pdrStatus = DI_OK;
	if (cmd == DI_BREAKPOINT_SET)
	{
		external_hardware_breakpoint_number++;
	}
	else if (cmd == DI_BREAKPOINT_REMOVE)
	{
		external_hardware_breakpoint_number--;
	}
	else
		*pdrStatus = DI_ERR_FATAL;
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				"<<DI_CB_EXTERNBPSET Callback drStatus=%d\n", *pdrStatus);
		fflush(gdi_spy_stream);
    }
	va_end (args);
}
/* End ICD */

/* Reuse the gdb disassembling info parameterization:
   the disassembling output function is redirected to the buffer passed
   as one of the callback parameters.
   See the initialization of this info structure at the file bottom */
disassemble_info tm_gdi_insn_info;
/* pointer to the callback disassembling result area */
static char * gdi_cb_disass_pt;
static int gdi_cb_disass_buffer_size;
static int gdi_cb_disass_max_buffer_size;
/* this is the disassembling output function dedicated to the callback:
   it assumes that the result area pointer has been previously set (MC: sorry,
   but I failed reusing the FILE * parameter for a memory area pointer) */
void
#ifdef ANSI_PROTOTYPES
gdi_cb_disass_print (FILE *stream, const char *format, ...)
#else
gdi_cb_disass_print (va_alist)
     va_dcl
#endif
{
  va_list args;
  int char_number;
  char *linebuffer;
#ifdef ANSI_PROTOTYPES
  va_start (args, format);
#else
  FILE *stream;
  char *format;

  va_start (args);
  stream = va_arg (args, FILE *);
  format = va_arg (args, char *);
#endif
  char_number = vasprintf (&linebuffer, format, args);
  if (char_number > (gdi_cb_disass_max_buffer_size - gdi_cb_disass_buffer_size - 1))
	char_number = gdi_cb_disass_max_buffer_size - gdi_cb_disass_buffer_size - 1;
  strncpy (gdi_cb_disass_pt, linebuffer, char_number);
  free (linebuffer);
  gdi_cb_disass_pt += char_number;
  gdi_cb_disass_buffer_size += char_number;
  va_end (args);
}

#define MAXIMUM_BUFFER_SIZE 256

/* DI_CB_DISASSEMBLE callback */
/* Enhancement to GDI version 126 !!! */
void gdicb_disassemble (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	
	DiAddrT addr;
	DiMemValueT *pdmvBuffer;
	DiStringT *result;
	DiUInt32T maximumResultSize = MAXIMUM_BUFFER_SIZE;
	DiReturnT *pdrStat;
	
	CORE_ADDR memaddr;
	
	va_start (args, dcCallbackType);
	addr = va_arg (args, DiAddrT);
	pdmvBuffer = va_arg (args, DiMemValueT *);
	result = va_arg (args, DiStringT *);
	if (debug_instrument_supports_disassemble_callback_additional_argument())
		maximumResultSize = va_arg (args, DiUInt32T);
	pdrStat = va_arg (args, DiReturnT *);
	
	DiAddrT2CORE (&addr, &memaddr);
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, "<<DI_CB_DISASSEMBLE Callback=0x%x dcCallbackType=0x%x addr=0x%04x maxsize=%d\n",
				gdicb_disassemble, dcCallbackType, memaddr, maximumResultSize);
		fflush(gdi_spy_stream);
    }
	
	if (pdmvBuffer) {
		/* Get instruction bytes from pdmvBuffer. */
		int i, number_of_bytes = 0;
		DiMemValueT *current_pdmvBuffer = pdmvBuffer;
		char *instruction_buffer, *current_instruction_buffer;

		while (current_pdmvBuffer && current_pdmvBuffer->dvsStatus == DI_VS_DEFINED) {
			number_of_bytes++;
			current_pdmvBuffer++;
		}
		instruction_buffer = (char *)xmalloc(number_of_bytes);
		current_instruction_buffer = instruction_buffer;
		current_pdmvBuffer = pdmvBuffer;
		for (i = 0; i < number_of_bytes; i++) {
		  *current_instruction_buffer = current_pdmvBuffer->dmValue.val[cbDiMaxUCharTsPerMemT - 1];
		  current_instruction_buffer++;
		  current_pdmvBuffer++;
		}
		tm_gdi_insn_info.buffer = instruction_buffer;
		tm_gdi_insn_info.buffer_length = number_of_bytes;
	} else {
		/* Instruction bytes will be read from memory. */
		tm_gdi_insn_info.buffer = NULL;
		tm_gdi_insn_info.buffer_length = 0;
	}

	/* Compute the disassembling result, using the standard tm_print_insn
	function, just being parameterized by the tm_gdi_insn_info structure */
	gdi_cb_disass_pt = *result;
	gdi_cb_disass_buffer_size = 0;
	gdi_cb_disass_max_buffer_size = maximumResultSize;
	(*tm_print_insn) (memaddr, &tm_gdi_insn_info);
	(*result)[gdi_cb_disass_buffer_size] = '\0';
	
	*pdrStat = DI_OK;

	if (pdmvBuffer) {
		free(tm_gdi_insn_info.buffer);
		tm_gdi_insn_info.buffer = NULL;
		tm_gdi_insn_info.buffer_length = 0;
	}

	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">>DI_CB_DISASSEMBLE Callback drStat=%d result=\"%s\"\n",
				*pdrStat, *result);
		fflush(gdi_spy_stream);
    }
	va_end (args);
}

/* from source.c */
void write_source_lines (char *result, unsigned long maximumLineSize, struct symtab *s, int line, int stopline, int noerror);

/* DI_CB_INFOLINE callback */
/* Enhancement to GDI version 126 !!! */
void gdicb_infoline (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	
	DiAddrT addr;
	DiStringT *result;
	DiUInt32T maximumLineSize = MAXIMUM_BUFFER_SIZE;
	DiReturnT *pdrStat;
	
	CORE_ADDR memaddr;
	
	va_start (args, dcCallbackType);
	addr = va_arg (args, DiAddrT);
	result = va_arg (args, DiStringT *);
	if (debug_instrument_supports_infoline_callback_additional_argument())
		maximumLineSize = va_arg (args, DiUInt32T);
	pdrStat = va_arg (args, DiReturnT *);
	
	DiAddrT2CORE (&addr, &memaddr);
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, "<<DI_CB_INFOLINE Callback=0x%x dcCallbackType=0x%x addr=0x%04x maxsize=%d\n",
				gdicb_infoline, dcCallbackType, memaddr, maximumLineSize);
		fflush(gdi_spy_stream);
    }
	
	{ 
		struct symtab_and_line sal;
		
		sal = find_pc_line (memaddr, 0);
		if (sal.symtab && sal.pc == memaddr)
		{
			char *pt = *result;
			int prefix_size;

			prefix_size = sprintf (pt, "%s:", sal.symtab->filename);
			pt += prefix_size;
			maximumLineSize -= prefix_size;
			write_source_lines (pt, maximumLineSize, sal.symtab, sal.line, sal.line + 1, 0); 
		}
	}
	
	*pdrStat = DI_OK;
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">>DI_CB_INFOLINE Callback drStat=%d result=\"%s\"\n",
				*pdrStat, *result);
		fflush(gdi_spy_stream);
    }
	va_end (args);
}

/* DI_CB_VALID_OPCODE callback */
/* Test if an instruction opcode is valid. */
/* Enhancement to GDI version 126 !!! */
void gdicb_valid_opcode (DiCallbackT dcCallbackType, ...)
{
	va_list args;
	DiUInt16T opcode;
	DiBoolT *pbResult;
	DiReturnT *pdrStat;
	char instruction_mnemonic[10];
	
	va_start (args, dcCallbackType);
	opcode = va_arg (args, DiUInt16T);
	pbResult = va_arg (args, DiBoolT *);
	pdrStat = va_arg (args, DiReturnT *);

	if (dcCallbackType != DI_CB_VALID_OPCODE)
		error("Bad gdicb_valid_opcode argument: dcCallbackType=%d.", dcCallbackType);

	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, "<<DI_CB_VALID_OPCODE Callback=0x%x dcCallbackType=0x%x opcode=0x%04x\n",
				gdicb_valid_opcode, dcCallbackType, opcode);
		fflush(gdi_spy_stream);
    }

	*pbResult = get_instr_mnemonic(opcode, instruction_mnemonic);
	*pdrStat = DI_OK;

	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">>DI_CB_VALID_OPCODE Callback drStat=%d result=%d\n",
				*pdrStat, *pbResult);
		fflush(gdi_spy_stream);
    }
	va_end (args);
}

static void processEvents PARAMS ((void))
{
  if (gdi_spy)
  {
	fprintf(gdi_spy_stream, ">>processEvents\n");
	fflush(gdi_spy_stream);
  }
  if (is_command_cancelled())
    {
      DiReturnT returnStatus;

	  if (gdi_spy)
		{
			fprintf(gdi_spy_stream, ">DiGdiCancel\n");
			fflush(gdi_spy_stream);
		}
      if (debug_user_interrupt())
		printf_filtered("send GDI command to cancel current command\n");
      returnStatus = DiGdiCancel();
      if (gdi_spy)
		{
			fprintf(gdi_spy_stream, "<DiGdiCancel %d\n", returnStatus);
			fflush(gdi_spy_stream);
		}
      error_handler(returnStatus, "DiGdiCancel");
    }
}

static DiReturnT di_add_callback PARAMS ((char *callback_name,
										  DiCallbackT callback_code,
										  void (*callback_address)(DiCallbackT, ...)))
{
	DiReturnT returnStatus;

	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">DiGdiAddCallback dcCallbackType=%s CallbackF=0x%x\n",
				callback_name, callback_address);
		fflush(gdi_spy_stream);
    }
	returnStatus = DiGdiAddCallback(callback_code, callback_address);
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, "<DiGdiAddCallback %d\n", returnStatus);
		fflush(gdi_spy_stream);
	}
	return returnStatus;
}

static DiReturnT di_gdi_get_features PARAMS ((DiFeaturesT *features))
{
	DiReturnT returnStatus;
	
	if (gdi_spy)
    {
	  fprintf(gdi_spy_stream, ">DiGdiGetFeatures pdfFeatures=0x%x\n", features);
	  fflush(gdi_spy_stream);  
	}
	memset((char *)features, 0, sizeof(DiFeaturesT));
	returnStatus = DiGdiGetFeatures(features);
	if (gdi_spy)
    {
	  print_gdi_features (returnStatus);
	  fflush(gdi_spy_stream);  
	}
	error_handler(returnStatus, "DiGdiGetFeatures");
	return returnStatus;
}

void check_compiler_target_compatibility_with_debug_target(void)
{
	if (compiler_target != mcu_core) {
		if (compiler_target == ST7_CORE) {
			st7_target_error();
		} else if (compiler_target == STM7_CORE) {
			stm7_target_error();
		} else if (compiler_target == STM7P_CORE) {
			stm7p_target_error();
		}
	}
}

static default_core(void)
{
  /* Default values */
  mcu_core = ST7_CORE;
  mcu_core_variant = ST7_CORE;
}

static int get_mcu(char *reserved_features, char *specified_mcu_name)
{
  int mcu_name_ok = 1;
  default_core();
  if (specified_mcu_name && reserved_features != NULL)
	{
	  /* Expected argument: "McuCore=ST7 McuName=ST7xxx". */
	  char *mcu_name;

	  /* MCU core: ST7 or STM7 or STM7P. */
	  if (strstr(reserved_features, "McuCore=ST7")) {
		mcu_core = ST7_CORE;
		mcu_core_variant = ST7_CORE;
	  } else if (strstr(reserved_features, "McuCore=STM7P_TC")) {
		/* STM7P test chip */
		mcu_core = STM7P_CORE;
		mcu_core_variant = STM7PV1_CORE;
	  } else if (strstr(reserved_features, "McuCore=STM7P")) {
		mcu_core = STM7P_CORE;
		mcu_core_variant = STM7PV2_CORE;
	  } else if (strstr(reserved_features, "McuCore=STM7")) {
		mcu_core = STM7_CORE;
		mcu_core_variant = STM7_CORE;
	  } else if (strstr(reserved_features, "McuCore=STM8")) {
		/* Same as STM7P */
		mcu_core = STM7P_CORE;
		mcu_core_variant = STM7PV2_CORE;
	  }
	  /* MCU name. */
	  mcu_name = strstr(reserved_features, "McuName=");
	  if (mcu_name)
		{
		  int i;

		  mcu_name += strlen("McuName=");
		  /* The separator is a white space. */
		  i = 0;
		  while (mcu_name[i] != '\0' && mcu_name[i] != ' ')
			{
			  gdi_current_micro[i] = mcu_name[i];
			  i++;
			}
		  gdi_current_micro[i] = '\0';
		}
	  if (strcmp(gdi_current_micro, specified_mcu_name) == 0)
		{
		  check_compiler_target_compatibility_with_debug_target();
		}
	  else
		{
		  default_core();
		  strcpy(gdi_current_micro, "");
		  mcu_name_ok = 0;
		}
	}
	/* Select instruction table in function of MCU core variant. */
	select_instruction_table(mcu_core_variant);
	return mcu_name_ok;
}

CORE_ADDR MCU_reset_vector = DEFAULT_ST7_RESET_VECTOR;

static void default_reset_vector(void)
{
	MCU_reset_vector = DEFAULT_ST7_RESET_VECTOR;
}

static void get_reset_vector(char *reserved_features)
{
  int reset_vector_found = 0;
  if (reserved_features != NULL)
	{
	  /* Expected pattern: "ResetVector=0xfffe". */
	  char *reset_vector_string;

	  if (reset_vector_string= strstr(reserved_features, "ResetVector="))
		{
		  unsigned long reset_vector;

		  reset_vector_string += strlen("ResetVector=");
		  if (sscanf(reset_vector_string, "%x", &reset_vector))
			{
				reset_vector_found = 1;
				MCU_reset_vector = reset_vector;
			}
		}
	}
  if (!reset_vector_found)
	{
		default_reset_vector();
	}
}

static int map_already_read = 0;

static DiReturnT get_features PARAMS ((DiFeaturesT *features, char *specified_mcu_name))
{
	int mcu_name_ok;
	DiReturnT returnStatus = di_gdi_get_features(features);

	/* Get MCU core (ST7/STM7/STM7P) and MCU name. */
	mcu_name_ok = get_mcu(features->pReserved, specified_mcu_name);

	if (mcu_name_ok) {
		/* Get reset vector: default is 0xfffe. */
		get_reset_vector(features->pReserved);

		/* Update stepping_mode */
		/* ICD targets without any debug module don't support hardware stepping */ 
		if (debug_instrument_supports_single_step()) {
			stepping_mode = hardware_stepping_mode;
		} else {
			stepping_mode = software_stepping_mode;
		}
	} else {
		default_reset_vector();
		stepping_mode = software_stepping_mode;
	}
	map_already_read = 0;
	return returnStatus;
}

static void di_gdi_open (DiUInt32T argc, DiStringT *argv);
static void di_gdi_init_IO (DiCommSetupT commSetup);
static void di_gdi_init_register_map (void);
static void get_extended_features(void);

void new_DLL_arg(char *di_open_arg, DiStringT *argv, DiUInt32T *argc)
{
	char *arg = (char *) xmalloc((strlen(di_open_arg) + 1) * sizeof(char));
	strcpy(arg, di_open_arg);
	argv[*argc] = arg;
	(*argc)++;
}

static void di_open PARAMS ((char *args))
{
    int arg_number;
    struct argstruct arg_list;
    unsigned char *szarg;
    struct cleanup *cleanup_chain;
	int i;

	DiUInt32T argc = 0;
	DiStringT *argv = NULL;
	char *DLL_name = NULL;
	DiCommSetupT commSetup;
	int port_option = 0;
	DiReturnT returnStatus;
	int target_close = 0;
	int mcu_option = 0;
	char *specified_mcu_name = NULL;
	int gdbspy_option = 0;

	/* Begin ICD */
	/* Reset number of breakpoints set outside GDB by a GDI direct access command. */
	external_hardware_breakpoint_number = 0;
	/* End ICD */
	if (args)
	{
	  int remaining_arg_number;
	  arg_number = parse_argument (args, &arg_list, isspace);
	  cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);
	  if (arg_number != 0)
	  {
		/* Don't bother to count exactly. Take into account worst case. */
		/* Be careful: freeargv assumes that argv has n+1 elements. The (n+1)�
		   element is set to NULL. freeargv frees element one by one and stops
		   when a NULL element is found. */
	    argv = (DiStringT *) xmalloc((arg_number + 1) * sizeof(DiStringT));
		for (i = 0; i <= arg_number; i++)
		  argv[i] = NULL;
		make_cleanup((void (*) (void *))freeargv, argv);
	  }
	  remaining_arg_number = arg_number;
	  while (remaining_arg_number != 0)
	  {
	    szarg = next_arg (&arg_list); /* get next argument */
        remaining_arg_number--;
	    if (strcmp(szarg, "-dll") == 0) /* change the dll name */
		{ 
		  if (remaining_arg_number == 0)
		    error("target gdi: DLL name missing after -dll option.");
		  szarg = next_arg (&arg_list); /* get next argument */
		  remaining_arg_number--;
		  if (szarg[0] == '-')
			error("target gdi: DLL name after -dll option can't start with '-'.");
		  DLL_name = (char *) xmalloc((strlen(szarg) + 1) * sizeof(char));
		  strcpy(DLL_name, szarg);
		  make_cleanup((void (*) (void *))free, DLL_name);
		}
		else if (strcmp(szarg, "-close") == 0) /* close the debug instrument */
		{
		  if (arg_number != 1)
			error("target gdi -close: no other argument accepted.");
		  target_close = 1;
		}
	    else if (strcmp(szarg, "-mcu") == 0) /* change the mcu type */
		{
		  char *dll_arg;

		  if (remaining_arg_number == 0)
		    error("target gdi: MCU type missing after -mcu option.");
		  szarg = next_arg (&arg_list); /* get next argument */
		  remaining_arg_number--;
		  if (strcasecmp(szarg, "ST7") == 0)
			mcu_core = ST7_CORE;
		  else if (strcasecmp(szarg, "STM7") == 0)
			mcu_core = STM7_CORE;
		  else if (strcasecmp(szarg, "STM7P") == 0)
			mcu_core = STM7P_CORE;
		  else if (strcasecmp(szarg, "STM8") == 0)
			mcu_core = STM7P_CORE;
		  else
			error("target gdi: bad MCU type; choose among [ST7, STM7, STM7P, STM8].");

		  dll_arg = (char *) xmalloc((strlen(szarg) + 2) * sizeof(char));
		  strcpy(dll_arg, "-");
		  strcat(dll_arg, szarg);
		  argv[argc++] = dll_arg;
		  mcu_option = 1;
		}
		else if (strcmp(szarg, "-port") == 0)
		{
		  char *arg;

		  if (remaining_arg_number == 0)
		    error("target gdi: port name missing after -port option.");
		  szarg = next_arg (&arg_list); /* get next argument */
		  remaining_arg_number--;
		  arg = (char *) xmalloc((strlen(szarg) + 1) * sizeof(char));
		  strcpy(arg, szarg);
		  if (strncmp(szarg, "lpt", 3) == 0 || strncmp(szarg, "LPT", 3) == 0)
		  {
			commSetup.dccType = DI_COMM_PARALLEL;
			commSetup.u.Parallel.szPortName = arg;
		  }
		  else
		  {
			commSetup.dccType = DI_COMM_DEVICE_FILE;
			commSetup.u.DeviceFile.szFileName = arg;
		  }
		  port_option = 1;
		}
		else if (strcmp(szarg, "-gdbspy") == 0) /* activate GDB trace */
		{
		  /* Trace commands sent to the debug instrument by GDB. */
		  if (remaining_arg_number == 0)
		    error("target gdi: file name missing after -gdbspy option.");
		  szarg = next_arg (&arg_list); /* get next argument */
		  remaining_arg_number--;
		  if (szarg[0] == '-')
			error("target gdi: file name after -gdbspy option can't start with '-'.");
		  gdi_spy_file_name = xmalloc((strlen(szarg) + 1) * sizeof(char));
		  strcpy(gdi_spy_file_name, szarg);
		  gdbspy_option = 1;
		}
		else if (strcmp(szarg, "-noguess") == 0)
		{ 
		  /* don't guess call type if no debug information on call type is found */
		  guess_call_type = 0;
		}
		else if (strcmp(szarg, "-checkdisassembly") == 0)
		{ 
		  /* check internal disassemble operations */
		  check_disassembly = 1;
		}
		else /* argument for the called DLL */
		{
		  new_DLL_arg(szarg, argv, &argc);

		  if (strcmp(szarg, "-mcuname") == 0) {
			  if (remaining_arg_number == 0)
				error("target gdi: MCU name missing after -mcuname option.");
			  else
			  {
				szarg = next_arg (&arg_list); /* get next argument */
				remaining_arg_number--;
				new_DLL_arg(szarg, argv, &argc);
				specified_mcu_name = szarg;
			  }
		  }
		}
	  }
	}

    /* If a target was already opened, it has been already closed.
	   There is nothing to do if target_close is equal to 1. */
	if (target_close)
	{
	  do_cleanups(cleanup_chain);
	  return;
	}

	/* Only check compatibility between compiler target and mcu
		if mcu has been specified by the -mcu option. */
	if (mcu_option)
	{
		check_compiler_target_compatibility_with_debug_target();
	}

	/* No default DLL. */
	if (DLL_name == NULL)
	  error("target gdi: no DLL specified - use \"target gdi -dll <debug instrument DLL>\".");

	open_gdi_dll(DLL_name);

	if (gdbspy_option) {
		gdi_spy = 1;
		gdi_spy_stream = fopen(gdi_spy_file_name, "w");
    }

	di_gdi_open (argc, argv);

	/* set callbacks */
	returnStatus = di_add_callback ("DI_CB_EVAL_EXPR", DI_CB_EVAL_EXPR, gdicb_eval_expr);
	returnStatus = di_add_callback ("DI_CB_DEBUG", DI_CB_DEBUG, gdicb_debug);
	returnStatus = di_add_callback ("DI_CB_FEEDBACK", DI_CB_FEEDBACK, gdicb_feedback);
	returnStatus = di_add_callback ("DI_CB_DISASSEMBLE", DI_CB_DISASSEMBLE, gdicb_disassemble);
	returnStatus = di_add_callback ("DI_CB_INFOLINE", DI_CB_INFOLINE, gdicb_infoline);
	/* Begin ICD */
	returnStatus = di_add_callback ("DI_CB_EXTERNBPSET", DI_CB_EXTERNBPSET, gdicb_externbpset);
	/* End ICD */

	if (!port_option)
	{
	  commSetup.dccType = DI_COMM_PARALLEL;
	  commSetup.u.Parallel.szPortName = "LPT1";
	}
	commSetup.fCheckConnection = 1;
	di_gdi_init_IO (commSetup);
	
	get_features(&features, specified_mcu_name);

#ifdef EXIT_CALLBACK
	if (features.dcCallback & DI_CB_EXITREQUEST) {
		returnStatus = di_add_callback ("DI_CB_EXITREQUEST", DI_CB_EXITREQUEST, gdicb_exitrequest);
	}
#endif
	if (features.dcCallback & DI_CB_VALID_OPCODE) {
		returnStatus = di_add_callback ("DI_CB_VALID_OPCODE", DI_CB_VALID_OPCODE, gdicb_valid_opcode);
	}

	/* Free allocated memory */
	do_cleanups(cleanup_chain);

	if (sr_get_debug ()) /* Print useful information after connection */
	{
	  printf_filtered("identifier=%s\n", features.szIdentification);
	  printf_filtered("version=%s\n", features.szVersion);
	  printf_filtered("GDI=%x\n", features.dnVersion);
	  {
		int i = 0;
		printf_filtered("CPU derivatives\n");
		for (; i < features.dnConfigArrayItems; i++)
		  printf_filtered("\t%s\n", features.pszConfig[i]);
	  }
	}

	get_extended_features();

	di_gdi_init_register_map ();
}

int debug_instrument_supports_single_step(void)
{
	return features.fSingleStepSupport;
}

static struct extended_features_type
{
  unsigned char disassemble_new_argument;
  unsigned char gdb_info;
  unsigned char gdb_software_breakpoints;
  unsigned char infoline_new_argument;
  unsigned char non_blocking_continue;
  unsigned char purely_software_breakpoints;
  unsigned char real_time_run;
  unsigned char restart_from_breakpoint;
  unsigned char correct_init_register_map;
  unsigned char partial_watchpoint_support;
} extended_features;

static void initialize_extended_features(void)
{
  extended_features.disassemble_new_argument = 0;
  extended_features.gdb_info = 0;
  extended_features.gdb_software_breakpoints = 0;
  extended_features.infoline_new_argument = 0;
  extended_features.non_blocking_continue = 1;
  extended_features.purely_software_breakpoints = 0;
  extended_features.real_time_run = 0;
  extended_features.restart_from_breakpoint = 0;
  extended_features.correct_init_register_map = 0;
  extended_features.partial_watchpoint_support = 0;
}

static char *protected_gdi_answer PARAMS ((char *args, int *command_error));

static void get_extended_features(void)
{
  int command_error = 0;
  char *answer;

  initialize_extended_features();
  answer = protected_gdi_answer("extended_features", &command_error);
  if (command_error)
	return;

  if (strstr(answer, "ExtendedDisassemble") != NULL)
    extended_features.disassemble_new_argument = 1;
  if (strstr(answer, "GdbInfo") != NULL)
	extended_features.gdb_info = 1;
  if (strstr(answer, "GdbSoftwareBreakpointMechanism") != NULL)
    extended_features.gdb_software_breakpoints = 1;
  if (strstr(answer, "ExtendedInfoline") != NULL)
    extended_features.infoline_new_argument = 1;
  if (strstr(answer, "NonBlockingContinue") == NULL)
	extended_features.non_blocking_continue = 0;
  if (strstr(answer, "PurelySoftwareBreakpointMechanism") != NULL)
    extended_features.purely_software_breakpoints = 1;
  if (strstr(answer, "RealTimeRun") != NULL)
	extended_features.real_time_run = 1;
  if (strstr(answer, "RestartFromBreakpoint") != NULL)
	extended_features.restart_from_breakpoint = 1;
  if (strstr(answer, "CorrectInitRegisterMap") != NULL)
	extended_features.correct_init_register_map = 1;
  if (strstr(answer, "PartialWatchpointSupport") != NULL)
	extended_features.partial_watchpoint_support = 1;
}

int debug_instrument_uses_purely_software_breakpoints(void)
{
  /* Only for the simulator. */
  return extended_features.purely_software_breakpoints;
}

int debug_instrument_uses_gdb_software_breakpoints(void)
{
  /* Only for ICD if RAM available. */
  return extended_features.gdb_software_breakpoints
		/* dnNrCodeBpAvailable: number of (software) code breakpoints available */
		 && (features.dnNrCodeBpAvailable != 0);
}

int debug_instrument_has_a_limited_number_of_hardware_breakpoints(void)
{
  /* Only for ICD. */
  return !(
			/* Simulator. */
			extended_features.purely_software_breakpoints
			/* DVP2 development kit, DVP3 development kit, HDS2 emulator,
				EMU3 emulator, ICE40 emulator. */
			|| debug_instrument_has_an_illimitable_number_of_hardware_breakpoints()
			);
}

static int debug_instrument_supports_real_time_run(void)
{
  /* Only for EMU3. */
  return extended_features.real_time_run;
}

int debug_instrument_supports_restart_from_breakpoint(void)
{
  /* Only for Maxcore simulator. */
  return extended_features.restart_from_breakpoint;
}

static int debug_instrument_supports_disassemble_callback_additional_argument(void)
{
  /* Now only for EMU3 and ICD. */
  return extended_features.disassemble_new_argument;
}

static int debug_instrument_supports_infoline_callback_additional_argument(void)
{
  /* Now only for EMU3 and ICD. */
  return extended_features.infoline_new_argument;
}

static int debug_instrument_supports_gdb_info(void)
{
  /* Now only for EMU3, ICD and the simulator. */
  return extended_features.gdb_info;
}

static int debug_instrument_does_not_support_non_blocking_continue(void)
{
  /* Only for the simulator. */
  return extended_features.non_blocking_continue == 0;
}

static int debug_instrument_supports_correctly_init_register_map(void)
{
  /* For the simulator at least. */
  return extended_features.correct_init_register_map;
}

static int debug_instrument_partially_supports_watchpoints(void)
{
  /* For STM8 STICE emulator. */
  return extended_features.partial_watchpoint_support;
}

unsigned int di_get_number_of_available_hardware_code_breakpoints (void);

int debug_instrument_has_an_illimitable_number_of_hardware_breakpoints(void)
{
  unsigned int hardware_breakpoint_number_max
	= di_get_number_of_available_hardware_code_breakpoints();

  return hardware_breakpoint_number_max == (unsigned int)-1;
}

/* Decrement PC after a break */
/* Fix decr_pc_after_break for STM7+
	otherwise the message displayed after a breakpoint is:
	"Program received signal SIGTRAP, Trace/breakpoint trap.".
   For an ST7/STM7, when there is a software breakpoint, a TRAP instruction is inserted
	and the debugger stops after the TRAP instruction.
   For an STM7P, when there is a software breakpoint, a dedicated software breakpoint instruction
	is inserted and the debugger stops just before this instruction.
*/
unsigned int decr_pc_after_break(void)
{
  if (debug_instrument_uses_gdb_software_breakpoints())
	return (mcu_core == STM7P_CORE) ? 0 : BREAKPOINT_SIZE;
  else
	return 0;
}

static void di_gdi_open(DiUInt32T argc, DiStringT *argv)
{
  DiReturnT returnStatus;
  DiUInt32T gdiVersionH = 0x00000126; /* Revision 1.2.6 */
  DiUInt32T gdiVersionL = 0x00000126;
  UdProcessEventsF UdProcessEvents = processEvents;
  int i;

  if (gdi_spy)
    {
	  fprintf(gdi_spy_stream,
			  ">DiGdiOpen dnGdiVersionH=0x%08x dnGdiVersionL=0x%08x dnArgc=%d ",
			  gdiVersionH, gdiVersionL, argc);
	  fprintf(gdi_spy_stream, "szArgv=[");
	  for (i = 0; i < argc; i++)
	  {
		if (i != 0) fprintf(gdi_spy_stream, " ");
		  fprintf(gdi_spy_stream, "%s", argv[i]);
	  }
	  fprintf(gdi_spy_stream, "] ");
	  fprintf(gdi_spy_stream, "UdProcessEvents=0x%x\n", UdProcessEvents);
	  fflush(gdi_spy_stream);
	}
  returnStatus = DiGdiOpen(gdiVersionH, gdiVersionL, argc, argv, UdProcessEvents);
  if (gdi_spy)
    {
	  fprintf(gdi_spy_stream, "<DiGdiOpen %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
  error_handler(returnStatus, "DiGdiOpen");
  debug_instrument_opened = 1;
}

static void di_gdi_init_IO (DiCommSetupT commSetup)
{
  DiReturnT returnStatus;

  if (gdi_spy)
    {
	  fprintf(gdi_spy_stream,
			  ">DiGdiInitIO dcCommSetup={dccType=0x%x fCheckConnection=%d ",
			  commSetup.dccType, commSetup.fCheckConnection);
	  if (commSetup.dccType == DI_COMM_PARALLEL)
	    fprintf(gdi_spy_stream, "u.Parallel.szPortName=%s}\n", commSetup.u.Parallel.szPortName);
	  else
	    fprintf(gdi_spy_stream, "u.DeviceFile.szFileName=%s}\n", commSetup.u.DeviceFile.szFileName);
	  fflush(gdi_spy_stream);  
	}
  returnStatus = DiGdiInitIO(&commSetup);
  if (gdi_spy)
    {
	  fprintf(gdi_spy_stream,
			  "<DiGdiInitIO %d dcCommSetup={dccType=0x%x fCheckConnection=%d ",
			  returnStatus, commSetup.dccType, commSetup.fCheckConnection);
	  if (commSetup.dccType == DI_COMM_PARALLEL)
	    fprintf(gdi_spy_stream, "u.Parallel.szPortName=%s}\n", commSetup.u.Parallel.szPortName);
	  else
	    fprintf(gdi_spy_stream, "u.DeviceFile.szFileName=%s}\n", commSetup.u.DeviceFile.szFileName);
	  fflush(gdi_spy_stream);  
	}
  error_handler(returnStatus, "DiGdiInitIO");
}

static DiRegisterInfoT *pdriRegister = NULL;

static void di_gdi_init_register_map (void)
{
  DiReturnT returnStatus;
  int i;
  /* Some targets (DVP2, HDS2) may return an error for new pseudo registers:
		set register_number to NUM_REGS - 4 - ADDITIONAL_REGISTER_NUMBER. */

  int register_number = NUM_REGS - ADDITIONAL_REGISTER_NUMBER;
  if (!debug_instrument_supports_correctly_init_register_map()) {
	register_number = NUM_REGS - 4 - ADDITIONAL_REGISTER_NUMBER;
  }

  if (pdriRegister) {
	  free(pdriRegister);
  }
  pdriRegister = (DiRegisterInfoT *) xmalloc(NUM_REGS * sizeof(DiRegisterInfoT));
  for (i = 0; i < NUM_REGS; i++)
	{
	  pdriRegister[i].szRegName = reg_names[i];
	  pdriRegister[i].dnRegNumber = i;
	  pdriRegister[i].fGdiSupport = 0;
	}
  if (gdi_spy)
	{
	  fprintf(gdi_spy_stream, ">DiGdiInitRegisterMap pdriRegister[%d]\n", register_number);
	  for (i = 0; i < register_number; i++)
		{
		  fprintf(gdi_spy_stream, "{\"%s\",Number=%d}",
				  pdriRegister[i].szRegName, pdriRegister[i].dnRegNumber);
		  if (i == (register_number - 1))
			fprintf(gdi_spy_stream, "\n");
		  else
			{
			  fprintf(gdi_spy_stream, ",");
			  if (((i + 1) % 4) == 0) fprintf(gdi_spy_stream, "\n");
			}
		}
	  fprintf(gdi_spy_stream, ">DiGdiInitRegisterMap dnProgramCounter=0x%x\n", PC_REGNUM);
	  fflush(gdi_spy_stream);
	}
  returnStatus = DiGdiInitRegisterMap(register_number, pdriRegister, PC_REGNUM);
  if (gdi_spy)
	{
	  fprintf(gdi_spy_stream, "<DiGdiInitRegisterMap %d\n", returnStatus);
	  fprintf(gdi_spy_stream, "<DiGdiInitRegisterMap pdriRegister[%d]\n", register_number);
	  for (i = 0; i < register_number; i++)
		{
		  fprintf(gdi_spy_stream, "{\"%s\",Support=%d}",
				  pdriRegister[i].szRegName, pdriRegister[i].fGdiSupport);
		  if (i == (register_number - 1))
			fprintf(gdi_spy_stream, "\n");
		  else
			{
			  fprintf(gdi_spy_stream, ",");
			  if (((i + 1) % 4) == 0) fprintf(gdi_spy_stream, "\n");
			}
		}
	  fflush(gdi_spy_stream);
	}
  error_handler(returnStatus, "DiGdiInitRegisterMap");
}

/* disable GDB/GDI trace */
static void gdi_spy_close(void)
{
  if (gdi_spy) {
	gdi_spy = 0;
	fclose(gdi_spy_stream);
	gdi_spy_stream = NULL;
	free(gdi_spy_file_name);
  }
}

/* Terminate usage of the di. This may involve freeing target memory
   and closing any open files and mmap'd areas. You cannot assume di_kill
   has already been called.
   QUITTING is non-zero if we cannot hang on errors.  */

static void di_close PARAMS ((int quitting))
{
	DiReturnT returnStatus;
	   
	if (debug_instrument_opened)
	{
		if (gdi_spy)
		{
			fprintf(gdi_spy_stream, ">DiGdiClose fClose=%d\n", quitting);
			fflush(gdi_spy_stream);
		}
		returnStatus = DiGdiClose(quitting);
		if (gdi_spy)
		{
			fprintf(gdi_spy_stream, "<DiGdiClose %d\n", returnStatus);
			fflush(gdi_spy_stream);
		}
		error_handler(returnStatus, "DiGdiClose");
		gdi_spy_close();
		close_gdi_dll();
		printf_filtered ("Disconnected from the GDI Debug Instrument.\n");
		debug_instrument_opened = 0;
	}
}

/* Load program PROG into the di.
   Return non-zero if you wish the caller to handle it
   (it is done this way because most debug instruments can use generic_load
   but defining it as a callback seems awkward).  */

static int di_load PARAMS ((char *prog, int from_tty))
{
  return 1;
}

static int create_inferior = 0;

static void di_create_inferior PARAMS ((void))
{
  create_inferior = 1;
}

static void reset_create_inferior_flag PARAMS ((void))
{
  create_inferior = 0;
}

int it_is_a_run_command PARAMS ((void))
{
  return create_inferior;
}

/* Read LENGTH bytes of the emulated program's memory and store in BUF.
   Result is number of bytes read, or zero if error.  */

/* (rj)
   Don't handle a "DiMemoryRead" error in "di_memory_read" but in its calling function:
   "gdi_xfer_inferior_memory". Otherwise it is not possible to use a cache.
*/

static
int di_memory_read
	PARAMS ((CORE_ADDR mem, unsigned char *buf, int length, DiReturnT* returnStatusPtr))
{
	DiAddrT targetAddress;
	int i;
	DiMemValueT *pdmvBuffer;
	int n;
	DiReturnT returnStatus;
    struct cleanup *cleanup_chain;

	CORE2DiAddrT (mem, &targetAddress);
	pdmvBuffer = (DiMemValueT *) xmalloc(length * sizeof(DiMemValueT));
	cleanup_chain = make_cleanup((void (*) (void *))free, pdmvBuffer);
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, ">DiMemoryRead ");
		fprintf(gdi_spy_stream, "daTarget.dmsMemSpace=0x%x ", targetAddress.dmsMemSpace);
		fprintf(gdi_spy_stream, "daTarget.dlaLinAddress=0x");
		for (i = cbDiValueT - addr_size; i < cbDiValueT; i++)
			fprintf(gdi_spy_stream, "%02x", targetAddress.dlaLinAddress.v.val[i]);
		fprintf(gdi_spy_stream, " ");
		fprintf(gdi_spy_stream, "dnBufferItems=%d\n", length);
		fflush(gdi_spy_stream);
    }

	returnStatus = DiMemoryRead (targetAddress, pdmvBuffer, length);
	*returnStatusPtr = returnStatus;
		
	if (gdi_spy)
    {
		if (returnStatus != DI_OK)
			fprintf(gdi_spy_stream, "<DiMemoryRead %d\n", returnStatus);
		else
		{
			fprintf(gdi_spy_stream, "<DiMemoryRead %d pdmvBuffer[%d]",
				    returnStatus,
					length);
			if (length <= 16)
				fprintf(gdi_spy_stream, "=");
			else
				fprintf(gdi_spy_stream, "\n");
			for (n = 0; n < length; n++)
			{
				fprintf(gdi_spy_stream, "%02x", pdmvBuffer[n].dmValue.val[cbDiMaxUCharTsPerMemT - 1]);
				if (((n + 1) % 39) == 0)
					fprintf(gdi_spy_stream, "\n");
			}
			n--;
			if (((n + 1) % 39) != 0)
				fprintf(gdi_spy_stream, "\n");
		}
		fflush(gdi_spy_stream);
    }

	/* Error is now handled in gdi_xfer_inferior_memory. */

	if (returnStatus == DI_OK)
	{
	  for (n = 0; n < length; n++)
	  {
		if (pdmvBuffer->dvsStatus == DI_VS_DEFINED)
		{
		  *buf = pdmvBuffer->dmValue.val[cbDiMaxUCharTsPerMemT - 1];
		  buf++;
		  pdmvBuffer++;
		}
		else
		  break;
	  }
    }
	else
	  n = 0;

	/* Free pdmvBuffer. */
	do_cleanups(cleanup_chain);

	/* (rj) return the number of bytes read successfully. */
	return n;
}

/* Store LENGTH bytes from BUF in the emulated program's memory.
   Result is number of bytes write, or zero if error.  */

static int di_memory_write PARAMS ((CORE_ADDR mem, unsigned char *buf, int length))
{
	DiAddrT targetAddress;
	int i;
	DiMemValueT *pdmvBuffer, *currentPdmvBuffer;
	int n;
	DiReturnT returnStatus;
    struct cleanup *cleanup_chain;
	
	CORE2DiAddrT (mem, &targetAddress);
	pdmvBuffer = (DiMemValueT *) xmalloc (length * sizeof(DiMemValueT));
	cleanup_chain = make_cleanup((void (*) (void *))free, pdmvBuffer);

	currentPdmvBuffer = pdmvBuffer;
	for (n = 0; n < length; n++)
    {
		currentPdmvBuffer->dvsStatus = DI_VS_DEFINED;
		for (i = 0; i < (cbDiMaxUCharTsPerMemT - 1); i++)
			currentPdmvBuffer->dmValue.val[i] = 0;
		currentPdmvBuffer->dmValue.val[cbDiMaxUCharTsPerMemT - 1] = *buf;
		currentPdmvBuffer++;
		buf++;
    }
	
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, ">DiMemoryWrite ");
		fprintf(gdi_spy_stream, "daTarget.dmsMemSpace=0x%x ", targetAddress.dmsMemSpace);
		fprintf(gdi_spy_stream, "daTarget.dlaLinAddress=0x");
		for (i = cbDiValueT - addr_size; i < cbDiValueT; i++)
			fprintf(gdi_spy_stream, "%02x", targetAddress.dlaLinAddress.v.val[i]);
		fprintf(gdi_spy_stream, "\n");
		
		fprintf(gdi_spy_stream, ">DiMemoryWrite pdmvBuffer[%d]\n", length);
		for (n = 0; n < length; n++)
		{
			fprintf(gdi_spy_stream, "%02x", pdmvBuffer[n].dmValue.val[cbDiMaxUCharTsPerMemT - 1]);
			if (((n + 1) % 39) == 0)
				fprintf(gdi_spy_stream, "\n");
		}
		n--;
		if (((n + 1) % 39) != 0)
			fprintf(gdi_spy_stream, "\n");
		fflush(gdi_spy_stream);
	}
	
	returnStatus = DiMemoryWrite (targetAddress, pdmvBuffer, length); 
	
	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, "<DiMemoryWrite %d\n", returnStatus);
		fflush(gdi_spy_stream);
	}

    /* Free pdmvBuffer. */
	do_cleanups(cleanup_chain);

	error_handler(returnStatus, "DiMemoryWrite");
	return length;
}

/* Download LENGTH bytes from BUF in the memory of the emulator.
   The result is the number of bytes downloaded or zero in case of an error. */

static int di_memory_download PARAMS ((	DiDownloadCommandT ddcDownloadCommand,
										CORE_ADDR mem, unsigned char *buf, int length))
{
	DiBoolT fUseAuxiliaryPath = 0;
	DiAddrT targetAddress;
	DiDownloadFormatT ddfDownloadFormat;
	char *pchBuffer = (char *)buf;
	int i;
	DiReturnT returnStatus;
	
	CORE2DiAddrT(mem, &targetAddress);
	ddfDownloadFormat.dafAbsFileFormat = DI_ABSF_BINARY;
	ddfDownloadFormat.dnBufferSize = length;
	ddfDownloadFormat.daAddress = targetAddress;
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream,
				">DiMemoryDownload fUseAuxiliaryPath=%d ddcDownloadCommand=0x%x\n",
				fUseAuxiliaryPath, ddcDownloadCommand);
		fprintf(gdi_spy_stream,
				">DiMemoryDownload ddfDownloadFormat={dafAbsFileFormat=0x%x,",
				ddfDownloadFormat.dafAbsFileFormat);
		fprintf(gdi_spy_stream,
				" dnBufferSize=%d,", ddfDownloadFormat.dnBufferSize);
		fprintf(gdi_spy_stream,
				" daAddress={dmsMemSpace=%d,", targetAddress.dmsMemSpace);
		fprintf(gdi_spy_stream, " dlaLinAddress=0x");

		for (i = cbDiValueT - addr_size; i < cbDiValueT; i++)
			fprintf(gdi_spy_stream, "%02x", targetAddress.dlaLinAddress.v.val[i]);

		fprintf(gdi_spy_stream, "}}\n");

		if (ddfDownloadFormat.dnBufferSize == 0)
			fprintf(gdi_spy_stream, ">DiMemoryDownload pchBuffer=0x%x\n", pchBuffer);
		else
		{
			fprintf(gdi_spy_stream, ">DiMemoryDownload pchBuffer\n");
			for (i = 0; i < ddfDownloadFormat.dnBufferSize; i++)
			{
				fprintf(gdi_spy_stream, "%02x", *((unsigned char *)(pchBuffer + i)));
				if (((i + 1) % 39) == 0)
					fprintf(gdi_spy_stream, "\n");
			}
			i--;
			if (((i + 1) % 39) != 0)
				fprintf(gdi_spy_stream, "\n");
		}
		fflush(gdi_spy_stream);
	}
	returnStatus
		= DiMemoryDownload(fUseAuxiliaryPath, ddcDownloadCommand, ddfDownloadFormat, pchBuffer); 
	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, "<DiMemoryDownload %d\n", returnStatus);
		fflush(gdi_spy_stream);
	}

	error_handler(returnStatus, "DiMemoryDownload");
	return length;
}

static int use_download_primitive = 0;

void di_memory_download_initialize PARAMS ((void))
{
	if (use_download_primitive)
		di_memory_download(DI_DNLD_INIT, 0, NULL, 0);
}

static int di_memory_download_execute PARAMS ((CORE_ADDR mem, unsigned char *buf, int length))
{
	return di_memory_download(DI_DNLD_WRITE, mem, buf, length);
}

void di_memory_download_terminate PARAMS ((void))
{
	if (use_download_primitive)
		di_memory_download(DI_DNLD_TERMINATE, 0, NULL, 0);
}

static void get_raisonance_pseudo_register(char *name, unsigned int size, unsigned char *buffer)
{
	CORE_ADDR pseudo_reg_addr;
	if (is_raisonance_compiler_used(symfile_objfile) && get_symbol_address(name, &pseudo_reg_addr)) {
		read_memory_nobpt(pseudo_reg_addr, buffer, size);
	} else {
		unsigned int i;
		for (i=0; i<size; i++) {
			*(buffer+i) = 0;
		}
	}
}

/* Fetch register REGNO and store the raw value in BUF.  */

static void di_fetch_register PARAMS ((int regno, unsigned char *buf))
{
  DiRegisterValueT value;
  DiReturnT returnStatus;
  int reg_size;

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't read register %s: no microcontroller is selected.", reg_names[regno]);
  }

  /* (rj) read/write on the fly. */
  if (application_is_running)
	error("Can't read register %s: application is running.", reg_names[regno]);

  reg_size = REGISTER_RAW_SIZE(regno);

  /* The ST7 has no frame pointer register. */
  /* This pseudo register FRAME_POINTER is used to take into account debug information
	for local variables and parameters in stack. */
  if (regno == FRAME_POINTER_REGNUM)
	{
	  /* If there is no selected frame, don't bother to select one. Just read SP instead.
		It should only happen when target_fetch_registers (-1) is called in gdi_open.
	  */
	  if (selected_frame == NULL)
		 regno = SP_REGNUM;
	  else
	  {
	    CORE_ADDR fp_value;
	    fp_value = get_sp_after_call(selected_frame);
	    *buf = (fp_value & 0xff00) >> 8;
	    *(buf+1) = (fp_value & 0xff);
	    return;
	  }
  } else if (regno == X_A_REGNUM) {
	  unsigned char x_buffer[2];
	  di_fetch_register(X_REGNUM, x_buffer);
	  *buf = x_buffer[1];
	  di_fetch_register(A_REGNUM, buf+1);
	  return;
  } else if (regno == XH_REGNUM) {
	  unsigned char x_buffer[2];
	  di_fetch_register(X_REGNUM, x_buffer);
	  *buf = x_buffer[0];
	  return;
  } else if (regno == XL_REGNUM) {
	  unsigned char x_buffer[2];
	  di_fetch_register(X_REGNUM, x_buffer);
	  *buf = x_buffer[1];
	  return;
  } else if (regno == YH_REGNUM) {
	  unsigned char y_buffer[2];
	  di_fetch_register(Y_REGNUM, y_buffer);
	  *buf = y_buffer[0];
	  return;
  } else if (regno == YL_REGNUM) {
	  unsigned char y_buffer[2];
	  di_fetch_register(Y_REGNUM, y_buffer);
	  *buf = y_buffer[1];
	  return;
  } else if (regno == cx_X_REGNUM) {
	  CORE_ADDR pseudo_reg_addr;
	  *buf = 0;
	  if (is_cosmic_compiler_used(symfile_objfile) && get_symbol_address("c_x", &pseudo_reg_addr)) {
		read_memory_nobpt(pseudo_reg_addr, buf+1, 1);
	  } else {
		*(buf+1) = 0;
	  }
	  di_fetch_register(X_REGNUM, buf+2);
	  return;
  } else if (regno == BX_REGNUM) {
	  get_raisonance_pseudo_register("?BH", 2, buf);
	  return;
  } else if (regno == CX_REGNUM) {
	  get_raisonance_pseudo_register("?CH", 2, buf);
	  return;
  } else if (regno == BH_REGNUM) {
	  get_raisonance_pseudo_register("?BH", 1, buf);
	  return;
  } else if (regno == BL_REGNUM) {
	  get_raisonance_pseudo_register("?BL", 1, buf);
	  return;
  } else if (regno == CH_REGNUM) {
	  get_raisonance_pseudo_register("?CH", 1, buf);
	  return;
  } else if (regno == CL_REGNUM) {
	  get_raisonance_pseudo_register("?CL", 1, buf);
	  return;
  } else if (regno == BX_CX_REGNUM) {
	  get_raisonance_pseudo_register("?BH", 4, buf);
	  return;
  } else if (regno == BL_CX_REGNUM) {
	  *buf = 0;
	  get_raisonance_pseudo_register("?BL", 3, buf+1);
	  return;
  }

  if (pdriRegister[regno].fGdiSupport)
	{
	  if (gdi_spy)
		{
		  fprintf(gdi_spy_stream,
				  ">DiRegisterRead %s dnRegNumber=%x\n",
				  reg_names[regno], regno);
		  fflush(gdi_spy_stream);
		}
	  returnStatus = DiRegisterRead(regno, &value);
	  if (gdi_spy)
		{
		  int i;
		
		  fprintf(gdi_spy_stream, "<DiRegisterRead %d ", returnStatus);
		  fprintf(gdi_spy_stream, "drvValue=0x");
		  for (i = cbDiValueT - reg_size; i < cbDiValueT; i++)
			fprintf(gdi_spy_stream, "%02x", value.dvValue.val[i]);
		  fprintf(gdi_spy_stream, "\n");
		  fflush(gdi_spy_stream);
		}
	  error_handler(returnStatus, "DiRegisterRead");
	}
  else
	/* It is a GDB7 special register not supported by the DI.
	Ex: TIME register for the ST7-EMU2 emulator. */
	{
	  int i;
	  for (i = 0; i < reg_size; i++) {
		*(buf+i) = 0;
	  }
	  return;
	}
  {
	unsigned char i;

	if (reg_size > cbDiValueT)
	  error("GDB7 internal error in di_fetch_register");

	for (i = 0; i < reg_size; i++) {
	  *(buf+i) = value.dvValue.val[cbDiValueT-reg_size+i];
	}
  }
}

/* Store register REGNO from BUF (in raw format).  */

static void di_store_register PARAMS ((int regno, unsigned char *buf))
{
  unsigned char i;
  DiRegisterValueT value;
  DiReturnT returnStatus;
  int reg_size;

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't write to register %s: no microcontroller is selected.", reg_names[regno]);
  }

  /* (rj) read/write on the fly. */
  if (application_is_running)
	error("Can't write to register %s: application is running.", reg_names[regno]);

  /* The ST7 has no frame pointer register.
     This pseudo register FRAME_POINTER is used to take into account debug information
	 for local variables and parameters in stack.
     We should never enter di_store_register with regno==FRAME_POINTER_REGNUM
	 but it is better to make sure that nothing is done.
  */
  /* Don't bother to write to pseudo-registers X:A, XH, XL, YH, YL, c_x:X,
	 BX, CX, BH, BL, CH, CL, BX:CX, BL:CX
     needed only to pass debug information.
  */
  if (regno == FRAME_POINTER_REGNUM) {
	  return;
  } else if (regno == X_A_REGNUM) {
	  return;
  } else if (regno == XH_REGNUM) {
	  return;
  } else if (regno == XL_REGNUM) {
	  return;
  } else if (regno == YH_REGNUM) {
	  return;
  } else if (regno == YL_REGNUM) {
	  return;
  } else if (regno == cx_X_REGNUM) {
	  return;
  } else if (regno == BX_REGNUM) {
	  return;
  } else if (regno == CX_REGNUM) {
	  return;
  } else if (regno == BH_REGNUM) {
	  return;
  } else if (regno == BL_REGNUM) {
	  return;
  } else if (regno == CH_REGNUM) {
	  return;
  } else if (regno == CL_REGNUM) {
	  return;
  } else if (regno == BX_CX_REGNUM) {
	  return;
  } else if (regno == BL_CX_REGNUM) {
	  return;
  }

  /* DATE_LIMIT_REGNUM will be modified!!! */
  if (regno == TIME_LIMIT_REGNUM) {
    registers_changed ();
  }

  for (i = 0; i < cbDiValueT; i++) {
	value.dvValue.val[i] = 0;
  }
  reg_size = REGISTER_RAW_SIZE(regno);

  if (reg_size > cbDiValueT)
	error("GDB7 internal error in di_store_register");

  for (i = 0; i < reg_size; i++) {
	value.dvValue.val[cbDiValueT-reg_size+i] = *(buf+i);
  }

  value.dvsStatus = DI_VS_DEFINED;
	
  if (gdi_spy)
    {
	  int i;
		
	  fprintf(gdi_spy_stream,
			  ">DiRegisterWrite %s dnRegNumber=%x ",
			  reg_names[regno], regno);
	  fprintf(gdi_spy_stream, "drvValue=0x");
	  for (i = cbDiValueT - reg_size; i < cbDiValueT; i++)
		fprintf(gdi_spy_stream, "%02x", value.dvValue.val[i]);
	  fprintf(gdi_spy_stream, "\n");
	  fflush(gdi_spy_stream);
    }
  returnStatus = DiRegisterWrite(regno, value);
  if (gdi_spy)
    {
	  fprintf(gdi_spy_stream, "<DiRegisterWrite %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
  /* Do not issue error when not fatal is returned: 
  this may be a read only register on the DI.
  Ex: SP_OVERFLOW register on ST7-EMU2 emulator */
  if (returnStatus == DI_ERR_NONFATAL)
	{
	  DiStringT errorMsg = "";

	  returnStatus = di_get_error_message (&errorMsg);
	  printf_filtered ("%s\n", errorMsg);
	}
  else
	{
	  register_valid [regno] = 0;
	  error_handler(returnStatus, "DiRegisterWrite");
	}
}

/* Print some interesting information about the di.
   VERBOSE is non-zero for the wordy version.  */

void di_info PARAMS ((int verbose))
{
}

/* Fetch why the program stopped.
   SIGRC will contain either the argument to exit() or the signal number.  */
 
/* (rj) read/write on the fly. */
void reset_execution_flags PARAMS ((void));
static int HardwareSingleStep = 0;

int single_step_execution(void)
{
  return HardwareSingleStep || SoftwareSingleStep;
}

enum di_stop stop_reason_code = di_reset;
CORE_ADDR stopped_data_address;
enum data_access stopped_data_access;
char *stop_reason_message = NULL;

int stopped_by_software_breakpoint = 0;

static
void di_stop_reason
	PARAMS ((enum di_stop *reason_code, char **reason_string, int *sigrc))
{
  DiExitStatusT exitStatus;
  DiReturnT returnStatus;

  stopped_by_software_breakpoint = 0;

  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiExecGetStatus\n");
	  fflush(gdi_spy_stream);
    }
  returnStatus = DiExecGetStatus(&exitStatus);
  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiExecGetStatus %d ", returnStatus);
      fprintf(gdi_spy_stream, "<DiExecGetStatus *pdesExitStatus={");
      fprintf(gdi_spy_stream, "dscCause=%x, ", exitStatus.dscCause);
      fprintf(gdi_spy_stream, "dwBpId=%d, ", exitStatus.dwBpId);
      fprintf(gdi_spy_stream, "szReason=%s}\n", exitStatus.szReason);
	  fflush(gdi_spy_stream);
    }
  error_handler(returnStatus, "DiExecGetStatus");
  if (stop_reason_message)
	free(stop_reason_message);
  if (exitStatus.szReason != NULL)
	{
	  stop_reason_message = (char *)xmalloc((strlen(exitStatus.szReason) + 1) * sizeof(char));
	  strcpy(stop_reason_message, exitStatus.szReason);
	}
  else
	{
	  stop_reason_message = (char *)xmalloc(sizeof(char));
	  strcpy(stop_reason_message, "");
	}
  switch (exitStatus.dscCause)
    {
    case DI_WAIT_RUNNING:
      *reason_code = di_running;
	  *reason_string = exitStatus.szReason;
      break;
    case DI_WAIT_SINGLESTEP: /* Single step */
	  /* Warning: the EMU3 emulator doesn't return DI_WAIT_SINGLESTEP
		 but DI_WAIT_CODEBP. */
      *reason_code = di_single_step;
	  *sigrc = TARGET_SIGNAL_TRAP;
      break;
    case DI_WAIT_CODEBP: /* Code breakpoint */
      *reason_code = di_stopped_by_code_breakpoint;
	  *sigrc = TARGET_SIGNAL_TRAP;

	  /* The EMU3 emulator never returns DI_WAIT_SINGLESTEP.
		 FIXME: to be changed if the emulator is fixed. */
	  /* This fix is needed for the EMU3 emulator so as to ensure
		 non-blocking execution (needed for the read/write on the fly functionality)
		 when there are breakpoints with conditions or ignore counters. */
	  /* Sometimes a breakpoint is hit when hardware stepping
	     but it is still OK to change the stop reason to di_single_step
		 for our purpose. */
	  if (HardwareSingleStep
		  /* Don't change reason for ICD: we need the real reason. */
		  && !debug_instrument_has_a_limited_number_of_hardware_breakpoints())
		*reason_code = di_single_step;

	  if (strstr(stop_reason_message, "Break on software breakpoint"))
		stopped_by_software_breakpoint = 1;
      break;
	case DI_WAIT_DATABP: /* Data breakpoint */
      *reason_code = di_stopped_by_data_breakpoint;
	  *sigrc = TARGET_SIGNAL_TRAP;
	  /* Don't print any message now since a data breakpoint may have
	     an associated condition or ignore counter. */
	  if (stop_reason_message)
	  {
		char *address_string;
	    char *rest;

		if (strstr(stop_reason_message, "read") != NULL
			&& strstr(stop_reason_message, "write") != NULL)
			stopped_data_access = read_write_access;
		else if (strstr(stop_reason_message, "read") != NULL)
			stopped_data_access = read_access;
		else if (strstr(stop_reason_message, "write") != NULL)
			stopped_data_access = write_access;
		else
			stopped_data_access = undefined_access;

		address_string = strstr(stop_reason_message, "0x");
		if (address_string != NULL)
		{
		  stopped_data_address = strtoul(address_string, &rest, 16);
		  if (rest == (address_string + 2))
			stopped_data_address = -1;
		}
		else
		  stopped_data_address = -1;
	  }
	  else
		{
		  stopped_data_access = undefined_access;
		  stopped_data_address = -1;
		}
      break;
    case DI_WAIT_USER: /* User interrupt */
      *reason_code = di_stopped_by_user;
	  *sigrc = TARGET_SIGNAL_STOP;
      break;
	case DI_WAIT_EXTERNAL: /* Normal error such as illegal memory access, ... */
	case DI_WAIT_UNKNOWN: /* Abnormal error */
	  *reason_code = di_stopped_by_di_specific_feature_with_error;
	  *sigrc = TARGET_SIGNAL_USR2;
#if 0
	  /* Generate an error message for the graphic interface but don't use
	  the standard "error" function which directly returns to the command loop. */
	  fprintf_filtered (gdb_stderr, "%s%s\n", error_prefix, exitStatus.szReason);
	  gdb_flush (gdb_stderr);
#endif
      break;
    default: /* Other reason: data breakpoint, ... */
      *reason_code = di_stopped_by_di_specific_feature_with_warning;
	  *sigrc = TARGET_SIGNAL_USR1;
#if 0
      warning("%s", exitStatus.szReason);
#endif
      break;
    }
  stop_reason_code = *reason_code;
}

void di_print_stop_message PARAMS ((void))
{
  switch (stop_reason_code)
    {
	case di_reset:
      break;
	case di_running:
      break;
    case di_single_step:
      break;
    case di_stopped_by_code_breakpoint:
      break;
	case di_stopped_by_data_breakpoint:
      break;
    case di_stopped_by_user:
      warning("%s", stop_reason_message);
	  gdb_flush (gdb_stderr);
      break;
	case di_stopped_by_di_specific_feature_with_error:
	  /* Generate an error message for the graphic interface but don't use
	  the standard "error" function which directly returns to the command loop. */
	  fprintf_filtered (gdb_stderr, "%s%s\n", error_prefix, stop_reason_message);
	  gdb_flush (gdb_stderr);
      break;
    case di_stopped_by_di_specific_feature_with_warning:
      warning("%s", stop_reason_message);
	  gdb_flush (gdb_stderr);
      break;
    }
}

static int execution_stop_flag = 0;

/* Interrupt execution. */

static void di_interrupt PARAMS ((void))
{
  DiReturnT returnStatus;

  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiExecStop\n");
	  fflush(gdi_spy_stream);
    }
  returnStatus = DiExecStop();
  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiExecStop %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
  /* Don't try to interrupt another time if there is an error. */
  if (returnStatus != DI_OK) {
	execution_stop_flag = 0;
  }
  error_handler(returnStatus, "DiExecStop");
}

/* Reset the chip.  */

static void di_chip_reset PARAMS ((void))
{
  DiReturnT returnStatus;

  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiExecResetChild\n");
	  fflush(gdi_spy_stream);
    }
  returnStatus = DiExecResetChild();
  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiExecResetChild %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
  error_handler(returnStatus, "DiExecResetChild");
}

/* Run (or resume) the program.  */
void gdi_command PARAMS ((char *args));

static void di_resume PARAMS ((int step, int siggnal))
{
  DiReturnT returnStatus;

  HardwareSingleStep = 0;
  if (create_inferior)
    {
	  create_inferior = 0;
	  if (debug_instrument_supports_real_time_run())
	  {
		gdi_command("run");
		return;
	  }
	  else
		di_chip_reset();
    }
  if (step)
  {
    if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiExecSingleStep 1\n");
	  fflush(gdi_spy_stream);
    }
    returnStatus = DiExecSingleStep(1);
    if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiExecSingleStep %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
    error_handler(returnStatus, "DiExecSingleStep");
	HardwareSingleStep = 1;
  }
  else
  {
    if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiExecContinue\n");
	  fflush(gdi_spy_stream);
    }
    returnStatus = DiExecContinue();
    if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiExecContinue %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
    error_handler(returnStatus, "DiExecContinue");
  }
}

static
int di_insert_breakpoint
	PARAMS ((DiBpTypeT breakpoint_type, CORE_ADDR addr, int size, char *shadow_contents))
{
  DiReturnT returnStatus;
  DiBpResultT pdbrResult;
  DiBpT dbBreakpoint;
  long i;
 
  /*
   * filling "dbBreakpoint":
   * --------------------------
   */
  dbBreakpoint.dbtBpType = breakpoint_type;

  /* "dbBreakpoint.u.exe.daBp" contains the addresse of a code breakpoint  */
  CORE2DiAddrT (addr, &dbBreakpoint.u.exe.daBp);

  /* "dbBreakpoint.u.exe.dnSize" contains the address range of a code breakpoint  */
  dbBreakpoint.u.exe.dnSize = size;

  /* "dbBreakpoint.u.exe.dbmBpMethod" indicates the kind of breakpoint  */
  dbBreakpoint.u.exe.dbmBpMethod = DI_BPM_HARDWARE;

  /* 
   * return value = "pdbrResult.dnBpld"
   * It contains a unique number that identifie the breakpoint 
   */
  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiBreakpointSet ");
      fprintf(gdi_spy_stream, "dbBreakpoint={");
      fprintf(gdi_spy_stream, "dbtBpType=%x, ", dbBreakpoint.dbtBpType);
      fprintf(gdi_spy_stream, "u.exe={");
      fprintf(gdi_spy_stream, "daBp={");
      fprintf(gdi_spy_stream, "dmsMemSpace=%d, ", dbBreakpoint.u.exe.daBp.dmsMemSpace);

	  fprintf(gdi_spy_stream, "dlaLinAddress=");

	  for (i = cbDiValueT - addr_size; i < cbDiValueT; i++)
	    fprintf(gdi_spy_stream, "%02x", dbBreakpoint.u.exe.daBp.dlaLinAddress.v.val[i]);

	  fprintf(gdi_spy_stream, "} ");

      fprintf(gdi_spy_stream, "dnSize=%d, ", dbBreakpoint.u.exe.dnSize);
      fprintf(gdi_spy_stream, "dbmBpMethod=0x%08x}}\n", dbBreakpoint.u.exe.dbmBpMethod);
	  fflush(gdi_spy_stream);
    }
  returnStatus = DiBreakpointSet(&pdbrResult, dbBreakpoint);
  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiBreakpointSet %d ", returnStatus);
      fprintf(gdi_spy_stream, "dnBreakpointId=%d\n", pdbrResult.dnBpId);
	  fflush(gdi_spy_stream);
    }

  if (returnStatus == DI_OK)
    {
      /* transcoding of breakpoint number "pdbrResult.dnBpld" in GDB format: */
	  add_inserted_breakpoint(shadow_contents, pdbrResult.dnBpId);
      return 0;
    }
  else
    {
      error_handler(returnStatus, "DiBreakpointSet");
      return 1;
    }
}

static
DiReturnT di_clear_breakpoint
	PARAMS ((DiUInt32T DnBreakpointId))
{
  DiReturnT returnStatus;

  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiBreakpointClear dnBreakpointId=%d\n", DnBreakpointId);
	  fflush(gdi_spy_stream);
    }
  returnStatus = DiBreakpointClear(DnBreakpointId);
  if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiBreakpointClear %d\n", returnStatus);
	  fflush(gdi_spy_stream);
    }
  return returnStatus;
}

/*
	returns 0 if there is no error
*/
static
int di_remove_breakpoint
	PARAMS ((DiBpTypeT breakpoint_type, CORE_ADDR addr, int size, char *shadow_contents))
{
  DiUInt32T DnBreakpointId; 
  DiReturnT returnStatus;

  if (get_inserted_breakpoint_id(shadow_contents, &DnBreakpointId)) {
	  returnStatus = di_clear_breakpoint(DnBreakpointId);
	  if (returnStatus == DI_OK) {
		remove_inserted_breakpoint(shadow_contents);
		return 0;
	  } else {
		error_handler(returnStatus, "DiBreakpointClear");
		return 1;
	  }
  } else {
	/* No breakpoint to be removed */
	return 0;
  }
}

/* Begin ICD */
unsigned int di_get_number_of_available_hardware_code_breakpoints (void)
{
  return features.dnNrHardWareCodeBpAvailable;
}
/* End ICD */

/****************************************************************************************/

/* Here are the gdi added commands */
static void
gdi_nop PARAMS ((char *args, int from_tty))
{
  printf_filtered ("Not available for current target.\n");
}

static void gdb_reset PARAMS ((void));

static DiReturnT di_gdi_set_config PARAMS ((DiStringT szConfig))
{
	DiReturnT returnStatus;

	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, ">DiGdiSetConfig %s\n", szConfig);
		fflush(gdi_spy_stream);
	}
	returnStatus = DiGdiSetConfig(szConfig);
	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, "<DiGdiSetConfig %d\n", returnStatus);
		fflush(gdi_spy_stream);
	}
    error_handler(returnStatus, "DiGdiSetConfig");
    return returnStatus;
}

/* Return the name of the core of the selected mcu */
char *get_mcu_core_name PARAMS (())
{
	if (mcu_core == ST7_CORE) {
		return "ST7";
	} else if (mcu_core == STM7_CORE) {
		return "STM7";
	} else if (mcu_core == STM7P_CORE) {
		return "STM8";
	} else {
		error("mcucore: bad MCU core returned by debug instrument; should be one of the following cores [ST7, STM7, STM8].");
		return "";
	}
}

/*
 * mcucore management
 */

static void
gdi_mcucore PARAMS ((char *args, int from_tty))
{
  char **argv;
  struct cleanup *cleanups;

  if (sr_get_debug())
    printf_filtered("gdi_mcucore:\n");

  if (!args) /* default is like -show */
  {
    printf_filtered("%s\n", get_mcu_core_name());
	return;
  }

  if ((argv = buildargv(args)) == NULL)
	{
		nomem (0);
    }
  cleanups = make_cleanup((void (*) (char *))freeargv, (char *)argv);

  if (argv[0])
  {
    if (strcasecmp(argv[0], "-show") == 0)
	{
        printf_filtered ("%s\n", get_mcu_core_name());
    }
    else
      error("mcucore: %s not a valid argument.", argv[0]);
  }
  do_cleanups(cleanups);
}

/*
 * mcuname management
 */

static void
gdi_mcuname PARAMS ((char *args, int from_tty))
{
  char **argv;
  struct cleanup *cleanups;

  if (sr_get_debug())
    printf_filtered("gdi_mcuname:\n");

  if (!args) /* default is like -show */
  {
    printf_filtered("%s\n", gdi_current_micro);
	return;
  }

  if ((argv = buildargv(args)) == NULL)
	{
		nomem (0);
    }
  cleanups = make_cleanup((void (*) (char *))freeargv, (char *)argv);

  if (argv[0])
  {
    if (strcasecmp(argv[0], "-list") == 0)
	{
      int i;
      for (i = 0; i < features.dnConfigArrayItems; i++)
	  {
        printf_filtered ("%s\n", features.pszConfig[i]);
	  }
    }
    else if (strcasecmp(argv[0], "-show") == 0)
	{
        printf_filtered ("%s\n", gdi_current_micro);
    }
    else if (strcasecmp(argv[0], "-set") == 0)
	{
      int i;

	  if (argv[1])
	  {
	    /* check against current micro list */
        for (i = 0; i < features.dnConfigArrayItems; i++)
		{
		  if (features.pszConfig[i])
		  {
			if (strcasecmp (argv[1], features.pszConfig[i]) == 0)
			{
				strcpy (gdi_current_micro, argv[1]);
				di_gdi_set_config(gdi_current_micro);

				/* After a change of CPUs, ask again for the GDI features. */
				get_features(&features, gdi_current_micro);

				/* After a change of CPUs, the CPU is reset. */
				gdb_reset();

				break;
			}
		  }
		  else
			  error("Bad information retrieved from the Debug Instrument (DiGdiGetFeatures).");
		}
	    if (i == features.dnConfigArrayItems)
	      error("mcuname: %s not supported by the current Debug Instrument.", argv[1]);
	  }
	}
    else
      error("mcuname: %s not a valid argument.", argv[0]);
  }
  do_cleanups(cleanups);
}

/*
 * mcumap management
 */

/* We need to allocate memory map item structure before calling
   DiMemoryGetMap or DiMemorySetMap. At dll inialization, we start
   allocating 30 items. If this is not sufficient, we will double
   this value each time this is needed. */
static DiMemoryMapItemT *mmi = NULL;
static int mmimax = 30;
static DiMemoryMapT current_map;

static DiReturnT di_memory_get_map PARAMS ((DiMemoryMapT *pdmmMap))
{
	DiReturnT returnStatus;

	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, ">DiMemoryGetMap\n");
		fflush(gdi_spy_stream);
	}			
	returnStatus = DiMemoryGetMap(pdmmMap);
	if (gdi_spy)
	{	
		int i, j;
		fprintf(gdi_spy_stream, "<DiMemoryGetMap %d\n", returnStatus);
		
		fprintf(gdi_spy_stream, "<DiMemoryGetMap dmmMap.dnNrItems=%d\n", pdmmMap->dnNrItems);
		/* If there is an error, pdmmMap is not set by the GDI DLL. */
		if (returnStatus == DI_OK)
		{
			for (i = 0; i < pdmmMap->dnNrItems; i++)
			{
				fprintf(gdi_spy_stream, ">DiMemoryGetMap dmmMap.pdmmiItems[%d]={", i);
				fprintf(gdi_spy_stream, "darRange={");
				fprintf(gdi_spy_stream, "daStart={");
				fprintf(gdi_spy_stream, "dmsMemSpace=%d, ", pdmmMap->pdmmiItems[i].darRange.daStart.dmsMemSpace);
			
				fprintf(gdi_spy_stream, "dlaLinAddress=0x");

				for (j = cbDiValueT - addr_size; j < cbDiValueT; j++)
					fprintf(gdi_spy_stream, "%02x",
							pdmmMap->pdmmiItems[i].darRange.daStart.dlaLinAddress.v.val[j]);

				fprintf(gdi_spy_stream, " ");
				
				fprintf(gdi_spy_stream, "dnSize=%d}, ", pdmmMap->pdmmiItems[i].darRange.dnSize);
				fprintf(gdi_spy_stream, "szMemType=%s, ", pdmmMap->pdmmiItems[i].szMemType);
				fprintf(gdi_spy_stream, "daAccess=%d}\n", pdmmMap->pdmmiItems[i].daAccess);
			}
		}
		fflush(gdi_spy_stream);
	}
	return returnStatus;
}

static DiReturnT di_memory_set_map PARAMS ((DiMemoryMapT dmmMap))
{
	DiReturnT returnStatus;
 
	if (gdi_spy)
	{
		int i, j;
				
		fprintf(gdi_spy_stream, ">DiMemorySetMap dmmMap.dnNrItems=%d\n", dmmMap.dnNrItems);
		for (i = 0; i < dmmMap.dnNrItems; i++)
		{
			fprintf(gdi_spy_stream, ">DiMemorySetMap dmmMap.pdmmiItems[%d]={", i);
			fprintf(gdi_spy_stream, "darRange={");
			fprintf(gdi_spy_stream, "daStart={");
			fprintf(gdi_spy_stream,
				    "dmsMemSpace=%d, ", dmmMap.pdmmiItems[i].darRange.daStart.dmsMemSpace);					
			fprintf(gdi_spy_stream, "dlaLinAddress=0x");

			for (j = cbDiValueT - addr_size; j < cbDiValueT; j++)
				fprintf(gdi_spy_stream, "%02x",
						dmmMap.pdmmiItems[i].darRange.daStart.dlaLinAddress.v.val[j]);

			fprintf(gdi_spy_stream, " ");
					
			fprintf(gdi_spy_stream, "dnSize=%d}, ", dmmMap.pdmmiItems[i].darRange.dnSize);
			fprintf(gdi_spy_stream, "szMemType=%s, ", dmmMap.pdmmiItems[i].szMemType);
			fprintf(gdi_spy_stream, "daAccess=%d}\n", dmmMap.pdmmiItems[i].daAccess);
		}
		fflush(gdi_spy_stream);
	}
	returnStatus = DiMemorySetMap(dmmMap);
	if (gdi_spy)
	{
		fprintf(gdi_spy_stream, "<DiMemorySetMap %d\n", returnStatus);
		fflush(gdi_spy_stream);
	}
	error_handler(returnStatus, "DiMemorySetMap");
	return returnStatus;
}

static void get_memory_map(DiMemoryMapT *cur_map_ptr)
{
  DiReturnT returnStatus;

  if (map_already_read) {
	return;
  }
  cur_map_ptr->dnNrItems = mmimax;
  if (mmi == NULL)
	mmi = (DiMemoryMapItemT *)xmalloc(mmimax * sizeof(DiMemoryMapItemT));
  cur_map_ptr->pdmmiItems = mmi;

  returnStatus = di_memory_get_map(cur_map_ptr);
			
  while (returnStatus == DI_ERR_NONFATAL) /* mmi overflow */
	{
	  mmimax = mmimax * 2;
	  mmi = (DiMemoryMapItemT *)xrealloc(mmi, mmimax * sizeof(DiMemoryMapItemT));
	  cur_map_ptr->dnNrItems = mmimax;
	  cur_map_ptr->pdmmiItems = mmi;
	  returnStatus = di_memory_get_map(cur_map_ptr);
	}

  error_handler(returnStatus, "DiMemoryGetMap");
  map_already_read = 1;
}

static void
gdi_mcumap PARAMS ((char *args, int from_tty))
{
	char **argv;
	struct cleanup *cleanups;
	DiReturnT returnStatus;
	
	if (sr_get_debug ())
		printf_filtered ("gdi_mcumap:\n");	

	if (!args) /* default is like -show */
	{
		args = alloca (strlen("-show") + 1);
		strcpy (args, "-show");
	}
	
	if ((argv = buildargv (args)) == NULL)
	{
		nomem (0);
    }
	cleanups = make_cleanup ((void (*) (char *))freeargv, (char *)argv);

	if (strcasecmp(argv[0], "-list") == 0)
	{
		int i;
		for (i = 0; i < features.dnMemTypeArrayItems; i++)
		{
			printf_filtered ("%s\n", features.pszMemoryType[i]);
		}
	}
	else if (strcasecmp(argv[0], "-show") == 0)
	{
		int i;
		get_memory_map(&current_map);
		for (i = 0; i < current_map.dnNrItems; i++)
		{
			CORE_ADDR start;
			DiAddrT2CORE(&(mmi[i].darRange.daStart), &start);
			printf_filtered ("[0x%04x-0x%04x]:%s\n", start,
								start + mmi[i].darRange.dnSize - 1,
								mmi[i].szMemType);
		}
	}
	else if (strcasecmp(argv[0], "-set") == 0)
	{
		int i;
		int argc;
		char *arg, *saved_arg;
		int syntax_error = 0;
		DiMemoryMapT set_map;

		/* (rj) read/write on the fly. */
		if (application_is_running)
			error("Can't map memory: application is running.");

		/* If the stack region is modified, stack min and stack max must be updated.
		   Two additional pseudo-registers SP_OVERFLOW and SP_UNDERFLOW are used
		   for the simulator and the development kit. */
		registers_changed ();

		map_already_read = 0;
		set_map.dnNrItems = 0;
		if (mmi == NULL)
			mmi = xmalloc (mmimax * sizeof(DiMemoryMapItemT));
		set_map.pdmmiItems = mmi;

		for (argc=1; argv[argc] != NULL; argc++)
		{
			CORE_ADDR start_addr, end_addr;
			DiStringT memtype;

			arg = argv[argc];
			saved_arg = arg;

			/* Expected syntax:
			   [0x1000-0x2000]:ROM
			   */
			if (*arg != '[')
			{
				syntax_error = 1;
				break;
			}
			start_addr = strtoul (arg+1, &arg, 0);
			if ((*(arg-1) == '[') /* No start address */ || (*arg != '-'))
			{
				syntax_error = 1;
				break;
			}
			end_addr = strtoul (arg+1, &arg, 0);
			if ((*(arg-1) == '-') /* No end address */ || (strncmp(arg, "]:", 2) != 0))
			{
				syntax_error = 1;
				break;
			}
			arg += 2;
				
			/* check against current memory type list */
			for (i = 0; i < features.dnMemTypeArrayItems; i++)
			{
				if (!strcasecmp (arg, features.pszMemoryType[i]))
				{
					memtype = features.pszMemoryType[i];
					break;
				}
			}
			if (i == features.dnMemTypeArrayItems)
				error ("%s memory type not supported by the current Debug Instrument.",
					   arg);
				
			/* Add on DI memory item */
			if (set_map.dnNrItems >= mmimax) /* mmi overflow */
			{
				mmimax = mmimax * 2;
				mmi = realloc (mmi, mmimax * sizeof(DiMemoryMapItemT));
				set_map.pdmmiItems = mmi;
			}
			CORE2DiAddrT (start_addr, &(mmi[set_map.dnNrItems].darRange.daStart));
			mmi[set_map.dnNrItems].darRange.dnSize = end_addr - start_addr + 1;
			mmi[set_map.dnNrItems].szMemType = memtype;
			mmi[set_map.dnNrItems].daAccess = DI_ACC_NONE; /* ### Not yet implemented */
			set_map.dnNrItems = set_map.dnNrItems + 1;
		}
		if (syntax_error)
			error("mcumap: %s not a valid argument.", saved_arg);

		di_memory_set_map(set_map);
	}
	else
		error ("mcumap: %s not a valid argument.", argv[0]);
	do_cleanups (cleanups);
}

/*
 * gdi management
 */

void enable_progress_bar_display (void);
void disable_progress_bar_display (void);

/* avoid buffer allocation each time Direct Access is called */
static char *toread = NULL;
static DiUInt32T toreadmax = 4096;

static ssleep(unsigned int t)
{
#ifdef unix
	sleep(t);
#endif
#ifdef WIN32
	Sleep(1000 * t);
#endif
}
extern void enable_command_cancel (void);
extern void enable_progress_bar_display (void);

static DiReturnT di_direct_command_call PARAMS ((DiStringT args))
{
	DiReturnT returnStatus;

	if (gdi_spy)
    {
      fprintf(gdi_spy_stream, ">DiDirectCommand szCommand=\"%s\"\n", args);
      fflush(gdi_spy_stream);
    }
	returnStatus = DiDirectCommand(args, NULL);
	if (gdi_spy)
    {
      fprintf(gdi_spy_stream, "<DiDirectCommand %d\n", returnStatus);
      fflush(gdi_spy_stream);
    }
	return returnStatus;
}

void di_direct_command PARAMS ((DiStringT args))
{
	DiReturnT returnStatus;

	returnStatus = di_direct_command_call (args);
	error_handler(returnStatus, "DiDirectCommand");
}

static DiReturnT di_direct_read PARAMS ((DiUInt32T *nread))
{
	DiReturnT returnStatus;

	if (toread == NULL)
		toread = xmalloc (toreadmax);

	/* Ask for the maximum size minus one to be able to add an '\0'
	   at the end of the "toread" buffer. */
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, ">DiDirectReadNoWait dnNrToRead=%d\n", toreadmax - 1);
		fflush(gdi_spy_stream);
    }
	returnStatus = DiDirectReadNoWait (toreadmax - 1, toread, nread);
	if (gdi_spy)
    {
		fprintf(gdi_spy_stream, "<DiDirectReadNoWait %d dnNrRead=%d\n", returnStatus, *nread);
		if (*nread != 0)
			fprintf(gdi_spy_stream, "<DiDirectReadNoWait pcBuffer=\"%s\"\n", toread);
		fflush(gdi_spy_stream);
    }
	return returnStatus;
}

static char *
gdi_answer PARAMS ((char *args))
{
	DiUInt32T nread = 0;
	DiReturnT returnStatus;

	di_direct_command (args);
	returnStatus = di_direct_read (&nread);
	error_handler(returnStatus, "DiDirectReadNoWait");
	if (returnStatus == DI_ERR_NONFATAL || nread == (toreadmax - 1))
		error("GDB7 internal error in gdi_answer: answer too long.");
	*(toread + nread) = '\0';
	return toread;
}

static char *
protected_gdi_answer PARAMS ((char *args, int *command_error))
{
	DiUInt32T nread = 0;
	DiReturnT returnStatus;

	returnStatus = di_direct_command_call (args);
	if (returnStatus != DI_OK)
	{
	  *command_error = 1;
	  return NULL;
	}
	returnStatus = di_direct_read (&nread);
	error_handler(returnStatus, "DiDirectReadNoWait");
	if (returnStatus == DI_ERR_NONFATAL || nread == (toreadmax - 1))
		error("GDB7 internal error in gdi_answer: answer too long.");
	*(toread + nread) = '\0';
	*command_error = 0;
	return toread;
}

static void gdi_reset PARAMS ((char *args, int from_tty));

void
gdi_command PARAMS ((char *args))
{
	DiUInt32T nread = 0;
	DiReturnT returnStatus;

	if (sr_get_debug ())
		printf_filtered ("gdi %s\n", args);

	di_direct_command (args);

	returnStatus = di_direct_read (&nread);

	/* We ask for (toreadmax - 1) characters to be able to add a '\0'
		at the end of the "toread" buffer. */
	while (returnStatus == DI_ERR_NONFATAL || nread == (toreadmax - 1))
	/* toread overflow */
	{
		if (nread != 0)
		{
		  *(toread + nread) = '\0';
		  printf_filtered ("%s", toread);
		}
		toreadmax = toreadmax * 2;
		toread = realloc (toread, toreadmax);
		returnStatus = di_direct_read (&nread);
	}
	error_handler(returnStatus, "DiDirectReadNoWait");
	if (nread != 0)
	{
	  *(toread + nread) = '\0';
	  printf_filtered ("%s\n", toread);
	}
	if (debug_instrument_supports_gdb_info())
	{
		char *answer = gdi_answer("gdb_info");

		if (strstr(answer, "VectorTableChange")) {
			/* Recompute reset vector. */
			di_gdi_get_features(&features);
			get_reset_vector(features.pReserved);
		}
		if (strstr(answer, "ChipReset")) {
			/* Invalidate register cache and recompute current program counter. */
			gdb_reset();
		}
		if (strstr(answer, "MemoryWrite")) {
			/* Invalidate memory cache: "gdi fillmem" modifies the memory. */
			cache_valid = 0;
		}
	}
}

static void
gdi_gdi PARAMS ((char *args, int from_tty))
{
	struct cleanup *cleanups;
	
	if (!args) /* default is nothing */
		return;
	
	cleanups = make_cleanup(cleanup_marker, (char *)NULL);
	enable_command_cancel ();
	enable_progress_bar_display ();	
	gdi_command(args);
	do_cleanups(cleanups);
}

/********************************************************************************/

/*
 * reset management
 */

static void reset_execution_stop_flag PARAMS ((void));

/* (rj) read/write on the fly. */
void reset_execution_flags PARAMS ((void))
{
  reset_execution_stop_flag ();
  read_write_on_the_fly_mode = 0;
  application_is_running = 0;
  proceed_counter = 0;
}

/* Reset execution flags if an error occurs after the program execution has stopped.
   Otherwise we get the message: "Program is already running." when we want to resume
   the execution of the program after for example a next which triggered an error. */
void reset_execution_flags_if_error_after_stop PARAMS ((void))
{
  if (!application_is_running)
	reset_execution_flags ();
}

static void
gdi_reset PARAMS ((char *args, int from_tty))
{
  DiReturnT returnStatus;

  if (sr_get_debug ())
    printf_filtered ("gdi_reset:\n");

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't reset microcontroller: no microcontroller is selected.");
  }

  di_chip_reset ();

  gdb_reset ();
}

/*
 * save_memory management
 */

 /* Function to save a range of memory into a file.
    save_memory addr size format; */
#define true 1 /* ###  bfd.h seems wrong on this */
void st_save_memory (CORE_ADDR memaddr, int count, char * filename, char * format)
{
	/* Now create the file */
	bfd *pBfd;
	asection *sect;
	char *buffer;
	struct cleanup *cleanups;
	boolean ok;

	if ((pBfd = bfd_openw (filename, format)) == NULL) 
		error ("Cannot open file %s in format %s", filename, format);
	if (bfd_set_format (pBfd, bfd_object) != true)
		goto error_return;
	if (bfd_set_start_address (pBfd, 0) != true)
		goto error_return;
	// if (bfd_set_file_flags (pBfd, 0) != true) goto error_return;
	// if (bfd_set_arch_mach (pBfd, bfd_arch_st7, 0) != true) goto error_return;

	if ((sect = bfd_make_section (pBfd, "single")) == NULL)
		goto error_return;
	if (bfd_set_section_size (pBfd, sect, count) != true)
		goto error_return;
	bfd_set_section_vma (pBfd, sect, memaddr);
	if (bfd_set_section_flags (pBfd, sect, SEC_HAS_CONTENTS|SEC_ALLOC|SEC_LOAD) != true)
		goto error_return;

	buffer = (char *) xmalloc (count * sizeof(char));
	cleanups = make_cleanup (free, (char *)buffer);
	target_read_memory (memaddr, buffer, count);
	ok = bfd_set_section_contents (pBfd, sect, buffer, 0, count);
	do_cleanups (cleanups);

	if (ok == true)
	{
		bfd_close (pBfd);
		return;
	}	
error_return:
	bfd_close (pBfd);
	error ("Cannot save memory to file %s in format %s", filename, format);
}

static void
gdi_save_memory PARAMS ((char *args, int from_tty))
{
  CORE_ADDR memaddr;
  unsigned long count;
  int base;
  char *filename;
  char *format;
  
  char **argv;
  struct cleanup *cleanups;

  if (sr_get_debug ())
    printf_filtered ("gdi_save_memory:\n");

  if (args == NULL)
    error("save_memory has at least 3 arguments.");
 
  if ((argv = buildargv(args)) == NULL)
    nomem(0);
  cleanups = make_cleanup(freeargv, (char *)argv);

  /* address argument */
  memaddr = parse_and_eval_address(*argv);
 
  /* count argument */
  argv++;
  if (*argv == NULL)
    error("save_memory has at least 3 arguments.");
  if (!(count = strtoul (*argv, NULL, 0)))
    error("save_memory: bad second argument: count=%s.", *argv);
 
  /* filename argument: accept quoted pathname */
  argv++;
  if (*argv == NULL)
    error("save_memory has at least 3 arguments.");
  filename = *argv;
 
  argv++;
  if (*argv != NULL)
    {
      format = *argv;
      argv++;
      if (*argv != NULL)
        error("save_memory has at most 4 arguments.");
    }
  else
    format = "srec";

  enable_command_cancel ();
  enable_progress_bar_display ();

  st_save_memory (memaddr, count, filename, format);
 
  do_cleanups(cleanups);
}

/*****************************************************************/
/* From now, this implementation is taken from remote-sim.c 4.17 */
/* Prototypes */

static void dump_mem PARAMS ((char *buf, int len));

static void gdi_fetch_register PARAMS ((int regno));

static void gdi_store_register PARAMS ((int regno));

static void gdi_kill PARAMS ((void));

static void gdi_load PARAMS ((char *prog, int from_tty));

static void gdi_create_inferior PARAMS ((char *exec_file, char *args, char **env));

static void gdi_open PARAMS ((char *args, int from_tty));

static void gdi_close PARAMS ((int quitting));

static void gdi_detach PARAMS ((char *args, int from_tty));

static void gdi_resume PARAMS ((int pid, int step, enum target_signal siggnal));

static int gdi_wait PARAMS ((int pid, struct target_waitstatus *status));

static void gdi_prepare_to_store PARAMS ((void));

static int gdi_xfer_inferior_memory PARAMS ((CORE_ADDR memaddr,
						char *myaddr, int len,
						int write,
						struct target_ops *target));

static void gdi_files_info PARAMS ((struct target_ops *target));

static void gdi_mourn_inferior PARAMS ((void));

static void gdi_stop PARAMS ((void));


/* Forward data declarations */
extern struct target_ops gdi_ops;

static int program_loaded = 0;

static void
dump_mem (buf, len)
     char *buf;
     int len;
{
  if (len <= 8)
    {
      if (len == 8 || len == 4)
	{
	  long l[2];
	  memcpy (l, buf, len);
	  printf_filtered ("\t0x%x", l[0]);
	  printf_filtered (len == 8 ? " 0x%x\n" : "\n", l[1]);
	}
      else
	{
	  int i;
	  printf_filtered ("\t");
	  for (i = 0; i < len; i++)
	    printf_filtered ("0x%x ", buf[i]);
	  printf_filtered ("\n");
	}
    }
}

/**********************************************************************/

static int debug_cancellation = 0;
int debug_user_interrupt(void)
{
  return debug_cancellation;
}

static void (*saved_sigint_handler) (int);
static void restore_interrupt_handler (void);
void modify_interrupt_handler (void (*new_interrupt_handler)(int))
{
  saved_sigint_handler = (void (*)(int)) signal(SIGINT, new_interrupt_handler);
  make_cleanup ((void (*) (void *))restore_interrupt_handler, NULL);
}

void restore_interrupt_handler (void)
{
  signal(SIGINT, saved_sigint_handler);
}

void set_execution_stop_flag (void)
{
  /* For the simulator, the DiExecContinue command is blocking.
	 The simulator doesn't run independently as an emulator
	 or a development kit board does.
	 We have to send the DiExecStop command as soon as the SIGINT signal is received.
	 */
  if (debug_instrument_does_not_support_non_blocking_continue())
	di_interrupt ();
  else
	execution_stop_flag = 1;

  if (debug_user_interrupt())
    printf_filtered("execution stop request\n");
}

static void set_execution_stop_flag_routine (int signo)
{
  signal(SIGINT, set_execution_stop_flag_routine);
  set_execution_stop_flag ();
}

static void reset_execution_stop_flag (void)
{
  execution_stop_flag = 0;
}

extern void user_interrupt_handler (int signo);
void enable_user_interrupt (void)
{
  reset_execution_stop_flag ();
  modify_interrupt_handler (set_execution_stop_flag_routine);
  make_cleanup ((void (*) (void *))reset_execution_stop_flag, NULL);
}
	 
static int command_cancel_flag = 0;
int is_command_cancelled (void)
{
  return command_cancel_flag;
}
static void set_command_cancel_flag (int signo)
{
  if (debug_user_interrupt())
    printf_filtered("command cancellation request\n");
  signal(SIGINT, set_command_cancel_flag);
  command_cancel_flag = 1;
}
static void reset_command_cancel_flag (void)
{
  command_cancel_flag = 0;
}

void enable_command_cancel (void)
{
  reset_command_cancel_flag ();
  modify_interrupt_handler (set_command_cancel_flag);
  make_cleanup ((void (*) (void *))reset_command_cancel_flag, NULL);
}

static int progress_bar_display = 0;

void enable_progress_bar_display (void)
{
  progress_bar_display = 1;
  make_cleanup ((void (*) (void *))disable_progress_bar_display, NULL);
}

void disable_progress_bar_display (void)
{
  progress_bar_display = 0;
}

int is_progress_bar_display_enabled (void)
{
  return progress_bar_display;
}
/*********************************************************************/
static void
gdi_fetch_register (regno)
int regno;
{
  if (regno == -1) 
    {
      for (regno = 0; regno < NUM_REGS; regno++)
	gdi_fetch_register (regno);
    }
  else if (regno >= NUM_REGS)
	{
	  error("Can't read register: unexpected register number %d.", regno);
	}
  else if (reg_names[regno] != NULL && *reg_names[regno] != '\0')
    {
      char buf[MAX_REGISTER_RAW_SIZE];

      di_fetch_register (regno, buf);
      supply_register (regno, buf);
      if (sr_get_debug ())
	{
	  printf_filtered ("gdi_fetch_register: %d", regno);
	  /* FIXME: We could print something more intelligible.  */
	  dump_mem (buf, REGISTER_RAW_SIZE (regno));
	}
    }
}


static void
gdi_store_register (regno)
int regno;
{
  if (regno  == -1) 
    {
      for (regno = 0; regno < NUM_REGS; regno++)
	gdi_store_register (regno);
    }
  else if (regno >= NUM_REGS)
	{
	  error("Can't write to register: unexpected register number %d.", regno);
	}
  else if (reg_names[regno] != NULL && *reg_names[regno] != '\0')
    {
      /* FIXME: Until read_register() returns LONGEST, we have this.  */
      char tmp[MAX_REGISTER_RAW_SIZE];
      read_register_gen (regno, tmp);
      di_store_register (regno, tmp);
	  /* reading after storing is necessary for registers that have read only bits */
      di_fetch_register (regno, tmp);
      supply_register (regno, tmp);
	  if (regno == A_REGNUM) {
		/* if A has changed, invalidate X:A. */
		register_valid[X_A_REGNUM] = 0;
	  } else if (regno == X_REGNUM) {
		/* if X has changed, invalidate X:A, XH, XL and c_x:X. */
		register_valid[X_A_REGNUM] = 0;
		register_valid[XH_REGNUM] = 0;
		register_valid[XL_REGNUM] = 0;
		register_valid[cx_X_REGNUM] = 0;
	  } else if (regno == Y_REGNUM) {
		/* if Y has changed, invalidate YH and YL. */
		register_valid[YH_REGNUM] = 0;
		register_valid[YL_REGNUM] = 0;
	  }
      if (sr_get_debug ())
		{
			printf_filtered ("gdi_store_register: %d", regno);
			/* FIXME: We could print something more intelligible.  */
			dump_mem (tmp, REGISTER_RAW_SIZE (regno));
		}
    }
}

/* Kill the running program.  This may involve closing any open files
   and releasing other resources acquired by the simulated program.  */

static void
gdi_kill (void)
{
  if (sr_get_debug ())
    printf_filtered ("gdi_kill\n");

  inferior_pid = 0;
}
 
static void reset_use_download_primitive (void)
{
  use_download_primitive = 0;
}

/* Load an executable file into the target process.  This is expected to
   not only bring new code into the target process, but also to update
   GDB's symbol tables to match.  */

static void
gdi_load (prog, from_tty)
     char *prog;
     int from_tty;
{
  char **argv;
  struct cleanup *cleanups;
  char *file_name;

  if (sr_get_debug ())
    printf_filtered ("gdi_load: prog \"%s\"\n", prog);

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't load program: no microcontroller is selected.");
  }

#if 0 /* Fix reload which prevents debugger from continuing execution */
  inferior_pid = 0;
#endif

  /* buildargv function is used here like for symbol_file_command in order
     to accept quoted filenames */
  if ((argv = buildargv (prog)) == NULL)
  {
    nomem (0);
  }
  cleanups = make_cleanup (freeargv, (char *) argv);

  file_name = *argv;
  argv++;
  if (*argv != NULL)
    error("load command has only one argument.");

  enable_command_cancel ();
  enable_progress_bar_display ();

  if (di_load (file_name, from_tty) != 0)
	{
	  if (features.ddfDownloadFormat.dafAbsFileFormat != DI_ABSF_NONE)
		{
		  use_download_primitive = 1;
		  make_cleanup (reset_use_download_primitive, NULL);
		}
		generic_load (file_name, from_tty);
	}

  program_loaded = 1;

#if 0 /* Fix reload which prevents debugger from continuing execution */
  /* take care of "info stack" before a run or a chip reset. */
  target_has_stack = 0;
#endif

  do_cleanups (cleanups);
}

void reinit_reset_address (void);

static void reset_stop_reason(void)
{
  /* Fix get_execution_status after the following sequence:
	run_no_wait
	stop_no_wait
	reset
	get_execution_status
	*/
  stop_reason_code = di_reset;
}

static void gdb_reset PARAMS ((void))
{
  gdi_kill ();

  /* (rj) read/write on the fly. */
  reset_execution_flags();

  /* Clean up saved state that will become invalid. */
  registers_changed ();

  inferior_pid = ST_INFERIOR_PID;
  target_has_stack = 1;

  reinit_frame_cache ();

  reinit_reset_address ();

  init_wait_for_inferior ();

  /* Fix get_execution_status after a breakpoint. */
  clear_proceed_status();

  /* Fix get_execution_status after a user stop. */
  reset_stop_reason();

  stop_pc = read_pc ();
}

/* Start an inferior process and set inferior_pid to its pid.
   EXEC_FILE is the file to run.
   ARGS is a string containing the arguments to the program.
   ENV is the environment vector to pass.  Errors reported with error().
   On VxWorks and various standalone systems, we ignore exec_file.  */
/* This is called not only when we first attach, but also when the
   user types "run" after having attached.  */

static void
gdi_create_inferior (exec_file, args, env)
     char *exec_file;
     char *args;
     char **env;
{
  struct cleanup *cleanups;

#if 0
  if (exec_file == 0 || exec_bfd == 0)
    warning ("No exec file specified.");
  if (! program_loaded)
    warning ("No program loaded.");
#endif

  if (sr_get_debug ())
    printf_filtered ("gdi_create_inferior: exec_file \"%s\", args \"%s\"\n",
						(exec_file ? exec_file: "(NULL)"), args);

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't execute program: no microcontroller is selected.");
  }

  reinit_reset_address ();

  gdi_kill ();	 
  remove_breakpoints ();
  init_wait_for_inferior ();

  di_create_inferior ();
  cleanups = make_cleanup ((void (*) (char *))reset_create_inferior_flag, NULL);

  inferior_pid = ST_INFERIOR_PID;

  /* Set target_has_stack. */
  target_has_stack = 1;

  insert_breakpoints (); /* Needed to get correct instruction in cache */

  /* Clean up from the last time we were running.  */
  clear_proceed_status ();

  /* Fix reload which prevents debugger from continuing execution */
  /* Start address is no more written to pc by generic_load */
  /* Necessary otherwise there is an error when executing: */
  /*	prev_instruction_is_a_call = is_a_call_instruction(read_pc ()); */
  /* in "proceed" because PC=0xffff! */
  write_pc(MCU_reset_address);

  proceed ((CORE_ADDR)-1, TARGET_SIGNAL_DEFAULT, 0);
  discard_cleanups (cleanups);
}

void make_gdi_commands_available(void);

/* The open routine takes the rest of the parameters from the command,
   and (if successful) pushes a new target onto the stack.
   Targets should supply this routine, if only to provide an error message.  */
/* Called when selecting the simulator. EG: (gdb) target gdi name.  */

static void
gdi_open (args, from_tty)
     char *args;
     int from_tty;
{
  if (sr_get_debug ())
    printf_filtered ("gdi_open: args \"%s\"\n", args ? args : "(null)");

  /* Remove current simulator if one exists.  Only do this if the simulator
     has been opened because gdi_close requires it.
     This is important because the call to push_target below will cause
     sim_close to be called if the simulator is already open, but push_target
     is called after sim_open!  We can't move the call to push_target before
     the call to sim_open because sim_open may invoke `error'.  */
  if (debug_instrument_opened != 0)
    unpush_target (&gdi_ops);

  /* (rj) read/write on the fly. */
  /* Be careful. We need to reset application_is_running in the case of
     a communication error. Otherwise it is impossible to get connected again
	 to the debug instrument. */
  reset_execution_flags();
  strcpy(gdi_current_micro, "");

  di_open (args);

  if (debug_instrument_opened != 0)
	{
	  push_target (&gdi_ops);

	  /* Reset "inferior_pid" to 0: at initialization, the stack is invalid.
		 "inferior_pid" is set to ST_INFERIOR_PID by run (gdi_create_inferior)
		 and reset (gdi_reset). */
	  inferior_pid = 0;

	  reinit_frame_cache ();

#if 0
	  /* Make some configuration initialization for DVP2 and HDS2*/
	  if (strcmp(gdi_current_micro, "") == 0 && features.pszConfig != NULL)
		{
		  strcpy(gdi_current_micro, features.pszConfig[0]);
		  di_gdi_set_config(gdi_current_micro);
		}
#endif
	  if (strcmp(gdi_current_micro, "") != 0) {
		/* reinitialize frame first now that we are using selected_frame in di_fetch_register */
		target_fetch_registers (-1);
	  }

	  printf_filtered ("Connected to the GDI Debug Instrument.\n");

	  make_gdi_commands_available();
	}
}

/* Make gdi commands available */
void make_gdi_commands_available(void)
{
/*
 * mcucore management
 */
  delete_cmd ("mcucore", &cmdlist);
  add_com ("mcucore", class_gdi,
	   (void (*) (char *, int))gdi_mcucore,
	   "display the mcu core.\n\
Usage:\n\
mcucore\n\
        -show\n\
shows the core name of the selected mcu.");

/*
 * mcuname management
 */
  delete_cmd ("mcuname", &cmdlist);
  add_com ("mcuname", class_gdi,
	   (void (*) (char *, int))gdi_mcuname,
	   "display or select the mcu name.\n\
Usage:\n\
mcuname -list\n\
        -show\n\
        -set <name>\n\
Basic parameters :\n\
-list makes appear the list (one per line) of all mcu names available.\n\
-show makes appear the name of the current mcu name selected (-show is the default).\n\
-set <name> makes the DI select a new mcu name.");

/*
 * mcumap management
 */
  delete_cmd ("mcumap", &cmdlist);
  add_com ("mcumap", class_gdi,
	   (void (*) (char *, int))gdi_mcumap,
	   "display or set the mcu memory mapping.\n\
Usage:\n\
mcumap -list\n\
       -show\n\
       -set <memory description>\n\
Basic parameters :\n\
-list makes appear the list of all memory types (one per line)\n\
which can be configured.\n\
-show makes appear for each memory range its current type\n\
(ex:[0x0000-0x00ff]:RAM),one per line (-show is the default).\n\
- setmakes the DI select a newmemory description. The\n\
<memory description> is giving the type of each range like -show\n\
syntax, but separated by a blank in this case.");

/*
 * gdi management
 */
  delete_cmd ("gdi", &cmdlist);
  add_com ("gdi", class_gdi,
	   (void (*) (char *, int))gdi_gdi,
	   "execute one Direct DI Access command.\n\
Usage:\n\
gdi <direct access command>\n\
The command result is displayed on gdb standard output.\n\
Type \"gdi help\" to get information on the available DI Access commands.");

/*
 * reset management
 */
  delete_cmd ("reset", &cmdlist);
  add_com ("reset", class_gdi,
	   (void (*) (char *, int))gdi_reset,
	   "Reset the ST7 micro.\n\
Usage:\n\
reset");

/*
 * save_memory management
 */
  delete_cmd ("save_memory", &cmdlist);
  add_com ("save_memory", class_gdi,
	   (void (*) (char *, int))gdi_save_memory,
       "Saves the contents of a range of memory to a file\n\
Usage:\n\
\tsave_memory <from address> <count> <filename> [<format>]\n\
\t\t<from address> is the start address of the memory block to save.\n\
\t\t<count> is the size of the memory block to save.\n\
\t\t<filename> is the name of the file to create.\n\
\t\t<format> is the file format keyword:\n\
\t\t\tsrec for Motorola Srecord format\n\
\t\t\tihex for Intel Hexa format\n\
\t\t\t(default format is srec)");
}

/* Does whatever cleanup is required for a target that we are no longer
   going to be calling.  Argument says whether we are quitting gdb and
   should not get hung in case of errors, or whether we want a clean
   termination even if it takes a while.  This routine is automatically
   always called just before a routine is popped off the target stack.
   Closing file descriptors and freeing memory are typical things it should
   do.  */
/* Close out all files and local state before this target loses control. */

static void
gdi_close (quitting)
     int quitting;
{
  if (sr_get_debug ())
    printf_filtered ("gdi_close: quitting %d\n", quitting);

  program_loaded = 0;

  di_close (quitting);

/*
 * mcucore management
 */

  delete_cmd ("mcucore", &cmdlist);
  add_com ("mcucore", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\", and then\n\
type \"help mcucore\" for full documentation.");

/*
 * mcuname management
 */

  delete_cmd ("mcuname", &cmdlist);
  add_com ("mcuname", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\", and then\n\
type \"help mcuname\" for full documentation.");

/*
 * mcumap management
 */

  delete_cmd ("mcumap", &cmdlist);
  add_com ("mcumap", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\", and then\n\
type \"help mcumap\" for full documentation.");

/*
 * gdi management
 */

  delete_cmd ("gdi", &cmdlist);
  add_com ("gdi", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\", and then\n\
type \"gdi help\" for full documentation.");

/*
 * reset management
 */

  delete_cmd ("reset", &cmdlist);
  add_com ("reset", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\", and then\n\
type \"help reset\" for full documentation.");

/*
 * save_memory management
 */

  delete_cmd ("save_memory", &cmdlist);
  add_com ("save_memory", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\", and then\n\
type \"help diversion\" for full documentation.");
}

/* Takes a program previously attached to and detaches it.
   The program may resume execution (some targets do, some don't) and will
   no longer stop on signals, etc.  We better not have left any breakpoints
   in the program or it'll die when it hits one.  ARGS is arguments
   typed by the user (e.g. a signal to send the process).  FROM_TTY
   says whether to be verbose or not.  */
/* Terminate the open connection to the remote debugger.
   Use this when you want to detach and do something else with your gdb.  */

static void
gdi_detach (args,from_tty)
     char *args;
     int from_tty;
{
  if (sr_get_debug ())
    printf_filtered ("gdi_detach: args \"%s\"\n", args);

  pop_target ();		/* calls gdi_close to do the real work */
  if (from_tty)
    printf_filtered ("Ending GDI debugging.\n");
}

#if 0 /* (rj) new handler is set_execution_stop_flag_routine */
void user_interrupt_handler (int signo)
{
#ifdef HOST_STVISUAL /* user interrupt thread handling */
#if 0
	printf_filtered("current thread identifier = %x\n", GetCurrentThreadId());
#endif
	if (hGdiApiMutex)
    if (WaitForSingleObject(hGdiApiMutex, 2000) == WAIT_FAILED)
    {
		int err = GetLastError ();
        if (sr_get_debug ())
		  printf_filtered ("In User Interrupt handler: WaitForSingleObject error %d\n", err);
	}
#endif /* WIN32 */
    di_interrupt ();
#ifdef HOST_STVISUAL /* user interrupt thread handling */
	if (hGdiApiMutex)
	if (ReleaseMutex (hGdiApiMutex))
    {
		int err = GetLastError ();
        if (sr_get_debug ())
		  printf_filtered ("In User Interrupt handler: ReleaseMutex error %d\n", err);
	}
#endif /* WIN32 */
}
#endif

/* Resume execution of the target process.  STEP says whether to single-step
   or to run free; SIGGNAL is the signal value (e.g. SIGINT) to be given
   to the target, or zero for no signal.  */

static enum target_signal resume_siggnal;
static int resume_step;
/* (rj) read/write on the fly. */
int application_stopped_by_breakpoint = 0;

static void
gdi_resume (pid, step, siggnal)
     int pid, step;
     enum target_signal siggnal;
{
  if (sr_get_debug ())
    printf_filtered ("gdi_resume: step %d, signal %d\n", step, siggnal);

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't execute program: no microcontroller is selected.");
  }

  if (inferior_pid != ST_INFERIOR_PID)
    error ("The program is not being run.");

#if 0 /* (rj) done now in proceed */
  /* Install temporarily user stop treatment as SIGINT handler*/
  prev_sigint = signal (SIGINT, user_interrupt_handler);
#endif

  /* (rj) read/write on the fly. */
  application_stopped_by_breakpoint = 0;

  di_resume (step, siggnal);

  resume_siggnal = siggnal;
  resume_step = step;
}

/* Notify the simulator of an asynchronous request to stop.
   
   The simulator shall ensure that the stop request is eventually
   delivered to the simulator.  If the call is made while the
   simulator is not running then the stop request is processed when
   the simulator is next resumed.

   For simulators that do not support this operation, just abort */

static void
gdi_stop (void)
{
	quit ();
}

char ST7_running_mode[100];
extern int stepping;

static void millisleep(int ms)
     /* ms: number of milliseconds. */
{
#ifdef unix
  /* Unit = microsecond. */
  usleep(ms * 1000);
#endif
#ifdef WIN32
  /* Unit = millisecond. */
  Sleep(ms);
#endif
}

/* Update wait status */
static void update_wait_status(enum di_stop reason_code, int sigrc, struct target_waitstatus *status)
{
  /* Test if the application is running in read/write on the fly mode. */
  if (reason_code == di_running)
	status->kind = TARGET_WAITKIND_RUNNING;
  /* We are stopped. */
  else
    status->kind = TARGET_WAITKIND_STOPPED;
  status->value.sig = sigrc;
}

/* Wait for inferior process to do something. Return pid of child,
   or -1 in case of error; store status through argument pointer STATUS,
   just as `wait' would. */
static int
gdi_wait (pid, status)
     int pid;
     struct target_waitstatus *status;
{
  int sigrc = 0;
  enum di_stop reason_code = di_running;
  char *reason_string;

  if (sr_get_debug ())
    printf_filtered ("gdi_wait\n");

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't execute program: no microcontroller is selected.");
  }

  /* (rj) read/write on the fly. */
  application_stopped_by_breakpoint = 0;

  while (1)
  {
	if (stepping)
	  /* Give back control to WINDOWS otherwise the "stop application" icon
	     isn't refreshed and a next over a line containing a loop
		 can't be interrupted. */
	  millisleep(0);
	else
	  /* Wait 50 milliseconds before asking whether the emulator has stopped or not
	     otherwise gdb7 uses all the CPU. */
	  millisleep(50);

	application_is_running = 0;
    di_stop_reason (&reason_code, &reason_string, &sigrc);
	if (reason_code == di_running)
	  application_is_running = 1;
	else
	  break;
	/* Send interrupt command only after having tested
	   that the application is still running. */
	if (execution_stop_flag) {
	  /* Update wait status in case of error:
		 if the application can't be interrupted, DiExecStop returns an error.
		 Halt and wfi instructions can't be interrupted by the STM8 SWIM ICD debugger. */
	  update_wait_status(reason_code, sigrc, status);
	  di_interrupt ();
	}
	/* (rj) read/write on the fly. */
	/* If the application is running in read/write on the fly mode,
	   exit from the while(1) loop;
	   but test before whether the user wants to stop the execution. */
	else if (read_write_on_the_fly_mode)
		{
		  if (reason_string == NULL)
		    strcpy(ST7_running_mode, "Running");
		  else
			strcpy(ST7_running_mode, reason_string);
		  break;
		}
  }

  /* If the program was already stopped due to another reason,
     change the reason to user stop so as to be sure to stop the debugger. */
  if (sigrc != TARGET_SIGNAL_STOP && execution_stop_flag == 1)
	{
	  char additional_stop_reason_message[100];

      sigrc = TARGET_SIGNAL_STOP;
	  stop_reason_code = di_stopped_by_user;
	  free(stop_reason_message);
      sprintf(additional_stop_reason_message,
	          "User stop while chip already stopped at address 0x%x\n",
	          (unsigned long)read_pc());
      stop_reason_message = (char *)xmalloc((strlen(additional_stop_reason_message) + 1)
		                                    * sizeof(char));
	  strcpy(stop_reason_message, additional_stop_reason_message);
	}

  /* Update wait status */
  update_wait_status(reason_code, sigrc, status);

  /* (rj) read/write on the fly. */
  /* Read/write on the fly is only possible after the two non-blocking execution commands:
		- run_no_wait
		- continue_no_wait
	*/
  if (read_write_on_the_fly_mode)
	{
	  /* The variable "application_stopped_by_breakpoint" is used
	     to handle breakpoints with conditions or ignore counters
		 while running in read/write on the fly mode. */
	  if (sigrc == TARGET_SIGNAL_TRAP
		  /* Take care: if we have executed a software single step (continue_no_wait
			after a stop due to a user breakpoint), the stop reason is no more
			single step as for hardware single step but code breakpoint!
			We just want to be in the same case as when hardware stepping.
			*/
		  && !SoftwareSingleStep
	      && (reason_code == di_stopped_by_code_breakpoint
			  || reason_code == di_stopped_by_data_breakpoint))
		application_stopped_by_breakpoint = 1;
	}
  return inferior_pid;
}

/* Get ready to modify the registers array.  On machines which store
   individual registers, this doesn't need to do anything.  On machines
   which store all the registers in one fell swoop, this makes sure
   that registers contains all the registers from the program being
   debugged.  */

static void
gdi_prepare_to_store (void)
{
  /* Do nothing, since we can store individual regs */
}

static int
gdi_xfer_inferior_memory (CORE_ADDR memaddr, char *myaddr, int len, int write, struct target_ops *target)
{

  if (sr_get_debug ())
    {
      printf_filtered ("gdi_xfer_inferior_memory: myaddr 0x%x, memaddr 0x%x, len %d, write %d\n",
		       myaddr, memaddr, len, write);
      if (sr_get_debug () && write)
		dump_mem(myaddr, len);
    }

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't access memory at 0x%x: no microcontroller is selected.", memaddr);
  }

  if (write)
    {
	  if (use_download_primitive)
		len = di_memory_download_execute (memaddr, myaddr, len);
	  else
		len = di_memory_write (memaddr, myaddr, len);
	  /* If memory has changed, invalidate c_x:X, BX, CX, BH, BL, CH, CL, BX:CX, BL:CX.
	     Don't bother to check whether we have modified c_x, ?BH, ?BL, ?CH, ?CL or not.
	  */
	  register_valid[cx_X_REGNUM] = 0;

	  register_valid[BX_REGNUM] = 0;
	  register_valid[CX_REGNUM] = 0;
	  register_valid[BH_REGNUM] = 0;
	  register_valid[BL_REGNUM] = 0;
	  register_valid[CH_REGNUM] = 0;
	  register_valid[CL_REGNUM] = 0;
	  register_valid[BX_CX_REGNUM] = 0;
	  register_valid[BL_CX_REGNUM] = 0;
    }
  else 
	{	  
	  DiReturnT returnStatus = DI_OK;

	  /* The cache is only used by the "x" command */
	  if (cache_enabled)
		{
		  if (cache_valid
		      && (memaddr >= cache_startaddr)
		      && (memaddr + len <= cache_startaddr + cache_size))
			{
			  /* cache hit ! */
			  int n;
			  for (n = 0; n < len; n++) {
				myaddr[n] = cache_buffer[memaddr - cache_startaddr + n];
			  }
			}
		  else
			{
			  /* control cache implementation */
			  /* don't worry about peripheral registers
				 cache_valid is reset by the "x" command in do_examine */
			  if ( /* do not exceed memory size! */
				   ((memaddr + cache_size - 1) < MEMORY_SIZE) && (len <= cache_size) )
				{
				  /* fill a new cache buffer */
				  int number_of_read_bytes;

				  number_of_read_bytes = di_memory_read (memaddr, cache_buffer, cache_size, &returnStatus);

				  if (number_of_read_bytes != cache_size) /* memory problem, do not use cache */
					{
					  len = di_memory_read (memaddr, myaddr, len, &returnStatus);
					}
				  else
					{
					  int n;

					  for (n = 0; n < len; n++)
						myaddr[n] = cache_buffer[n];
					  cache_startaddr = memaddr;
					  cache_valid = 1;
					}
				}
			  else
				{
				  len = di_memory_read (memaddr, myaddr, len, &returnStatus);
				}
			}
		}
	  else
		{
		  len = di_memory_read (memaddr, myaddr, len, &returnStatus);
		}
      if (sr_get_debug () && len > 0)
		dump_mem(myaddr, len);

	  error_handler(returnStatus, "DiMemoryRead");
	}

   return len;
}

static void
gdi_files_info (target)
     struct target_ops *target;
{
  char *file = "nothing";

  if (exec_bfd)
    file = bfd_get_filename (exec_bfd);

  if (sr_get_debug ())
    printf_filtered ("gdi_files_info: file \"%s\"\n", file);

  if (exec_bfd)
    {
      printf_filtered ("\tAttached to %s running program %s\n",
		       target_shortname, file);
      di_info (0);
    }
}

/* Clear the simulator's notion of what the break points are.  */

static void
gdi_mourn_inferior (void) 
{ 
  if (sr_get_debug ())
    printf_filtered ("gdi_mourn_inferior:\n");

  remove_breakpoints ();
  generic_mourn_inferior ();
}

/* As "daAccess" is not implemented correctly in most DLLs, use "szMemType" instead.
   For the moment, "daAccess" is implemented correctly only in st7sim.dll.
*/
static int is_memory_map_item_readable(DiMemoryMapItemT memory_map_item)
{
#if 0
	return (memory_map_item.daAccess & DI_ACC_READ) == DI_ACC_READ
			|| (memory_map_item.daAccess & DI_ACC_EXECUTE) == DI_ACC_EXECUTE;
#endif
	if (stricmp(memory_map_item.szMemType, "NEM") == 0
		|| stricmp(memory_map_item.szMemType, "NER") == 0
		|| stricmp(memory_map_item.szMemType, "NONE") == 0
		|| stricmp(memory_map_item.szMemType, "RESERVED") == 0) {
		return 0;
	} else {
		return 1;
	}
}

static int is_memory_zone_readable(CORE_ADDR address, unsigned int length)
{
  int i;
  get_memory_map(&current_map);
  /* Start with the last element as most of the time, the FLASH will be located at the end of the list. */
  for (i = current_map.dnNrItems - 1; i >= 0; i--)
	{
	  CORE_ADDR mmi_start_address;
	  unsigned long mmi_size;

	  DiAddrT2CORE(&(mmi[i].darRange.daStart), &mmi_start_address);
	  mmi_size = mmi[i].darRange.dnSize;
	  if (mmi_start_address <= address && address <= (mmi_start_address + mmi_size - 1)) {
		if (is_memory_map_item_readable(mmi[i])) {
		  if ((address + length) <= (mmi_start_address + mmi_size)) {
			/* whole range can be read. */
			return 1;
		  } else {
			/* continue check for range end. */
			CORE_ADDR next_address = mmi_start_address + mmi_size;
			return is_memory_zone_readable(next_address, length - (next_address - address));
		  }
		} else {
		  /* can't be read. */
		  return 0;
		}
	  }
	}
  return 0;
}

int is_disassemble_possible(CORE_ADDR address)
{
	return is_memory_zone_readable(address, MAXILEN);
}

/* Some emulator drivers (st7emu4.dll at least) need to be fixed as daAccess is set to DI_ACC_WRITE
   for all memory map items (even for non existent memory). */
static int is_memory_map_item_writable(DiMemoryMapItemT memory_map_item)
{
	return ((memory_map_item.daAccess & DI_ACC_WRITE) == DI_ACC_WRITE);
}

int is_memory_writable(CORE_ADDR address)
{
  int i;
  get_memory_map(&current_map);
  /* Start with the last element as most of the time, the FLASH will be located at the end of the list. */
  for (i = current_map.dnNrItems - 1; i >= 0; i--)
	{
	  CORE_ADDR mmi_start_address;
	  unsigned long mmi_size;

	  DiAddrT2CORE(&(mmi[i].darRange.daStart), &mmi_start_address);
	  mmi_size = mmi[i].darRange.dnSize;
	  if (mmi_start_address <= address && address <= (mmi_start_address + mmi_size - 1))
		return is_memory_map_item_writable(mmi[i]);
	}
  return 0;
}

static int
gdi_insert_breakpoint(CORE_ADDR addr, char *shadow_contents)
{
  if (sr_get_debug())
    printf_filtered("gdi_insert_breakpoint: addr 0x%x\n", addr);

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't insert breakpoint at 0x%x: no microcontroller is selected.", addr);
  }

  if (debug_instrument_uses_gdb_software_breakpoints()
	  && software_breakpoint_implementation_method(shadow_contents))
	return memory_insert_breakpoint(addr, instruction_backup(shadow_contents));
  else
	return di_insert_breakpoint(DI_BPT_EXECUTE, addr, 1, shadow_contents);
}

static int
gdi_remove_breakpoint (CORE_ADDR addr, char *shadow_contents)
{
  if (sr_get_debug())
    printf_filtered("gdi_remove_breakpoint: addr 0x%x\n", addr);

  if (strcmp(gdi_current_micro, "") == 0) {
	error("Can't remove breakpoint at 0x%x: no microcontroller is selected.", addr);
  }

  if (debug_instrument_uses_gdb_software_breakpoints()
	  && software_breakpoint_implementation_method(shadow_contents))
	return memory_remove_breakpoint(addr, instruction_backup(shadow_contents));
  else
    return di_remove_breakpoint(DI_BPT_EXECUTE, addr, 1, shadow_contents);
}

int
st7_insert_watchpoint
PARAMS ((CORE_ADDR addr, int len, int read_write, char *shadow_contents))
{
  /* memory breakpoint */
  if (sr_get_debug ())
    {
      printf_filtered ("st7_insert_watchpoint: addr 0x%x, len %d, read_write %d\n",
		               addr, len, read_write);
    }
 
  if (!debug_instrument_opened)
  {
    gdi_nop	("", 0);
	return 1;
  }

  if (read_write == 0)
	di_insert_breakpoint(DI_BPT_WRITE, addr, len, shadow_contents);
  else if (read_write == 1)
	di_insert_breakpoint(DI_BPT_READ, addr, len, shadow_contents);
  else if (read_write == 2)
	{ 
	  di_insert_breakpoint(DI_BPT_READ, addr, len, shadow_contents);
	  di_insert_breakpoint(DI_BPT_WRITE, addr, len, shadow_contents);
	}
  else
	error("GDB7 internal error in st7_insert_watchpoint.");

  return 0;
}

int
st7_remove_watchpoint
	PARAMS ((CORE_ADDR addr, int len, int read_write, char *shadow_contents))
{
  if (sr_get_debug ())
    {
      printf_filtered ("st7_remove_watchpoint: type %d addr 0x%x, len %d\n",
					   read_write, addr, len);
    }

  if (!debug_instrument_opened)
  {
    gdi_nop	("", 0);
	return 1;
  }

  if (read_write == 0)
	di_remove_breakpoint(DI_BPT_WRITE, addr, len, shadow_contents);
   else if (read_write == 1)
	di_remove_breakpoint(DI_BPT_READ, addr, len, shadow_contents);
  else if (read_write == 2)
	{
	  di_remove_breakpoint(DI_BPT_READ, addr, len, shadow_contents);
	  di_remove_breakpoint(DI_BPT_WRITE, addr, len, shadow_contents);
	}
  else
	error("GDB7 internal error in st7_remove_watchpoint.");

  return 0;
}

int target_region_ok_for_watchpoint PARAMS ((CORE_ADDR addr, int len))
{
  if (debug_instrument_partially_supports_watchpoints()) {
	char command[100], *pcommand, *result;
	pcommand = &command[0];
	sprintf(pcommand, "is_watchpoint_supported 0x%x %d", addr, len);
	result = gdi_answer(pcommand);
	if (strcmp(result, "yes") == 0) {
	  return 1;
	} else {
	  return 0;
	}
  } else {
	return 1;
  }
}

/* Define the target subroutine names */

struct target_ops gdi_ops = {
  "gdi",			/* to_shortname */
  "GDI Debug Instrument",			/* to_longname */
  "Use the OMI-GDI Debug Instrument",  /* to_doc */
  gdi_open,			/* to_open */
  gdi_close,			/* to_close */
  NULL,				/* to_attach */
  gdi_detach,		/* to_detach */
  gdi_resume,		/* to_resume */
  gdi_wait,			/* to_wait */
  gdi_fetch_register,	/* to_fetch_registers */
  gdi_store_register,	/* to_store_registers */
  gdi_prepare_to_store,	/* to_prepare_to_store */
  gdi_xfer_inferior_memory,	/* to_xfer_memory */
  gdi_files_info,		/* to_files_info */
  gdi_insert_breakpoint,	/* to_insert_breakpoint */
  gdi_remove_breakpoint,	/* to_remove_breakpoint */
  NULL,				/* to_terminal_init */
  NULL,				/* to_terminal_inferior */
  NULL,				/* to_terminal_ours_for_output */
  NULL,				/* to_terminal_ours */
  NULL,				/* to_terminal_info */
  gdi_kill,			/* to_kill */
  gdi_load,			/* to_load */
  NULL,				/* to_lookup_symbol */
  gdi_create_inferior,	/* to_create_inferior */ 
  gdi_mourn_inferior,	/* to_mourn_inferior */
  0,				/* to_can_run */
  0,				/* to_notice_signals */
  0,				/* to_thread_alive */
  gdi_stop,			/* to_stop */
  process_stratum,		/* to_stratum */
  NULL,				/* to_next */
  1,				/* to_has_all_memory */
  1,				/* to_has_memory */

  /* 0: to take care of "info stack" before a run or a chip reset. */
  0,				/* to_has_stack */

  1,				/* to_has_registers */
  1,				/* to_has_execution */
  NULL,				/* sections */
  NULL,				/* sections_end */
  OPS_MAGIC,			/* to_magic */
};

static void spyopen_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;

  if (strcmp(target_shortname, "gdi") != 0)
    error("spyopen not available for current target: %s.", target_shortname);
  if (gdi_spy)
	error("spyopen: GDI primitives are already traced in %s.", gdi_spy_file_name);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number != 1)
    error("spyopen has one argument: a file name.");

  szarg = next_arg (&arg_list); /* get next argument */

  gdi_spy_file_name = xmalloc((strlen(szarg) + 1) * sizeof(char));
  strcpy(gdi_spy_file_name, szarg);
  gdi_spy = 1; /* enable GDB/GDI trace */
  gdi_spy_stream = fopen(gdi_spy_file_name, "w");
  /* Free arglist. */
  do_cleanups(old_cleanup_chain);	
}

static void
spyclose_command(char *args, int from_tty)
{
  if (strcmp(target_shortname, "gdi") != 0)
    error("spyclose not available for current target: %s.", target_shortname);

  if (!gdi_spy)
	error("spyclose: GDI primitives are not being traced.");

  if (args != NULL)
    error("spyclose has no arguments.");

  gdi_spy_close();
}

static void getchar_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  char answer;
  CORE_ADDR char_addr; 
  int length;

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);
  if (arg_number != 1) {
    error("getchar has one argument: a char address.");
  }
  szarg = next_arg (&arg_list); /* get next argument */
  char_addr = parse_and_eval_address_1 (&szarg);

  getchar_from_printf_pipe(&answer);

  if (answer == EOF) {
	error("Read error");
  }

  length = di_memory_write(char_addr, &answer, 1);
  if (length != 1) {
	  error("Couldn't write at 0x%x", char_addr);
  }

  /* Free arglist. */
  do_cleanups(old_cleanup_chain);	
}

struct file_type {
	int number;
	FILE *stream;
	struct file_type *next;
};
static struct file_type *file_list = NULL;

static void add_opened_file(int file_number, FILE *file_stream)
{
	struct file_type *new_file = (struct file_type *)xmalloc(sizeof(struct file_type));
	new_file->number = file_number;
	new_file->stream = file_stream;
	new_file->next = file_list;
	file_list = new_file;
}

static FILE *get_opened_file(int file_number)
{
	struct file_type *file = file_list;
	while (file) {
		if (file->number == file_number) {
			return file->stream;
		} else {
			file = file->next;
		}
	}
	return NULL;
}

static void remove_opened_file(int file_number)
{
	struct file_type *file = file_list;
	struct file_type *previous_file = NULL;

	while (file) {
		if (file->number == file_number) {
			if (previous_file) {
				previous_file->next = file->next;
			} else {
				file_list = file->next;
			}
			free(file);
			return;
		} else {
			previous_file = file;
			file = file->next;
		}
	}
	error("Can't find file number %d", file_number);
}

static int is_valid_file_number(const char *string, long *number)
{
  long value;
  char *string_end = NULL;

  value = strtol (string, &string_end, 0);
  if (number) {
    *number = value;
  }
  return (string_end && string_end != string && (*string_end == '\0') && value > 0);
}

static void fopen_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  int file_number;
  FILE *file_stream;

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);
  if (arg_number != 2) {
    error("fopen has two arguments: a file number and a file name.");
  }
  szarg = next_arg(&arg_list); /* get next argument */
  if (!is_valid_file_number(szarg, &file_number)) {
	  error("Incorrect file number %s", szarg);
  }
  szarg = next_arg(&arg_list); /* get next argument */
  file_stream = fopen(szarg, "r");

  if (file_stream == NULL) {
	error("fopen error");
  } else {
	add_opened_file(file_number, file_stream);
  }

  /* Free arglist. */
  do_cleanups(old_cleanup_chain);	
}

static void fclose_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  int file_number;
  FILE *file_stream;
  int result;

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);
  if (arg_number != 1) {
    error("fclose has one argument: a file number.");
  }
  szarg = next_arg(&arg_list); /* get next argument */
  if (!is_valid_file_number(szarg, &file_number)) {
	  error("Incorrect file number %s", szarg);
  }

  file_stream = get_opened_file(file_number);
  if (file_stream == NULL) {
	error("Can't find file number %d", file_number);
  }

  result = fclose(file_stream);

  if (result == EOF) {
	error("fclose error");
  } else {
	remove_opened_file(file_number);
  }

  /* Free arglist. */
  do_cleanups(old_cleanup_chain);	
}

static void fgetc_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  int file_number;
  FILE *file_stream;
  char answer;
  CORE_ADDR char_addr; 
  int length;

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);
  if (arg_number != 2) {
    error("fgetc has two arguments: a file number and a char address.");
  }
  szarg = next_arg(&arg_list); /* get next argument */
  if (!is_valid_file_number(szarg, &file_number)) {
	  error("Incorrect file number %s", szarg);
  }
  szarg = next_arg(&arg_list); /* get next argument */
  char_addr = parse_and_eval_address_1(&szarg);

  file_stream = get_opened_file(file_number);
  if (file_stream == NULL) {
	error("Can't find file number %d", file_number);
  }

  answer = fgetc(file_stream);
  if (answer == EOF) {
	error("Read error");
  }

  length = di_memory_write(char_addr, &answer, 1);
  if (length != 1) {
	  error("Couldn't write at 0x%x", char_addr);
  }

  /* Free arglist. */
  do_cleanups(old_cleanup_chain);	
}

void
_initialize_remote_gdi ()
{
  extern int debug_progress_bar_display;

  add_target (&gdi_ops);


  /* initialize disassembler parameters needed for the DI_CB_DISASSEMBLE callback */
  INIT_DISASSEMBLE_INFO_NO_ARCH (tm_gdi_insn_info, (FILE *) NULL, (fprintf_ftype)gdi_cb_disass_print);
  tm_gdi_insn_info.flavour = bfd_target_unknown_flavour;
  tm_gdi_insn_info.read_memory_func = dis_asm_read_memory;
  tm_gdi_insn_info.memory_error_func = dis_asm_memory_error;
  tm_gdi_insn_info.print_address_func = dis_asm_print_address;


/* create new command class gdi */  
add_cmd ("st-gdi", class_gdi, NO_FUNCTION, "Special GDI commands.", &cmdlist);

/*
 * mcucore management
 */

  add_com ("mcucore", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\" and then\n\
type \"help mcucore\" for full documentation.");

/*
 * mcuname management
 */

  add_com ("mcuname", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\" and then\n\
type \"help mcuname\" for full documentation.");

/*
 * mcumap management
 */

  add_com ("mcumap", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\" and then\n\
type \"help mcumap\" for full documentation.");

/*
 * gdi management
 */

  add_com ("gdi", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\" and then\n\
type \"gdi help\" for full documentation.");

/*
 * reset management
 */

  add_com ("reset", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\" and then\n\
type \"help reset\" for full documentation.");

/*
 * save_memory management
 */

  add_com ("save_memory", class_gdi, gdi_nop,
	   "GDI target must be specified.\n\
Type \"target gdi\" and then\n\
type \"help diversion\" for full documentation.");

  add_show_from_set(add_set_cmd ("debugcancel", class_gdi, var_boolean,
									(char *) &debug_cancellation,
									"Set command cancellation debug mode.",
									&setlist),
					&showlist);
  add_show_from_set(add_set_cmd ("debugprogressbar", class_gdi, var_boolean,
									(char *) &debug_progress_bar_display,
									"Set progress bar debug mode.",
									&setlist),
					&showlist);

  add_com ("spyopen", class_gdi, spyopen_command,
		   "Store GDI accesses into a trace file.\n\
Syntax:\n\
\tspyopen <file name>");

  add_com ("spyclose", class_gdi, spyclose_command,
		   "Close GDI trace file.\n\
Syntax:\n\
\tspyclose");

  add_com ("getchar", class_gdi, getchar_command,
		   "Store a char read from stdin into a char.\n\
Syntax:\n\
\tgetchar <char address>");

  add_com ("fopen", class_gdi, fopen_command,
		   "Open a file defined by its name and associate a file stream to a specified file number.\n\
Syntax:\n\
\tfopen <file number> <file name>");

  add_com ("fclose", class_gdi, fclose_command,
		   "Close a file stream associated to a file number.\n\
Syntax:\n\
\tfclose <file number>");

  add_com ("fgetc", class_gdi, fgetc_command,
		   "Store a char read from a file associated to a file number into a char.\n\
Syntax:\n\
\tfgetc <file number> <char address>");
}
