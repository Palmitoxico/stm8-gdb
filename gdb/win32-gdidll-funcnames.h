/*	Copyright (C) 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

#ifndef WIN2_GDIDLL_FUNCNAMES_H
#define WIN2_GDIDLL_FUNCNAMES_H

#define DiGdiOpenExportedName "_DiGdiOpen@20"
#define DiGdiCloseExportedName "_DiGdiClose@4"
#define DiGdiGetFeaturesExportedName "_DiGdiGetFeatures@4"
#define DiGdiInitIOExportedName "_DiGdiInitIO@4"
#define DiGdiInitRegisterMapExportedName "_DiGdiInitRegisterMap@12"
#define DiTraceGetNrOfNewFramesExportedName "_DiTraceGetNrOfNewFrames@12"
#define DiDirectReadNoWaitExportedName "_DiDirectReadNoWait@12"
#define DiTracePrintRawInfoExportedName "_DiTracePrintRawInfo@12"
#define DiExecContinueUntilExportedName "_DiExecContinueUntil@12"
#define DiRegisterClassCreateExportedName "_DiRegisterClassCreate@12"
#define DiErrorGetMessageExportedName "_DiErrorGetMessage@4"
#define DiMemorySetMapExportedName "_DiMemorySetMap@8"
#define DiMemoryDownloadExportedName "_DiMemoryDownload@32"
#define DiMemoryWriteExportedName "_DiMemoryWrite@20"
#define DiMemoryReadExportedName "_DiMemoryRead@20"
#define DiRegisterWriteExportedName "_DiRegisterWrite@16"
#define DiRegisterReadExportedName "_DiRegisterRead@8"
#define DiBreakpointSetExportedName "_DiBreakpointSet@32"
#define DiBreakpointClearExportedName "_DiBreakpointClear@4"
#define DiBreakpointClearAllExportedName "_DiBreakpointClearAll@0"
#define DiGdiCancelExportedName "_DiGdiCancel@0"
#define DiExecStopExportedName "_DiExecStop@0"
#define DiExecContinueExportedName "_DiExecContinue@0"
#define DiExecResetChildExportedName "_DiExecResetChild@0"
#define DiExecContinueBackgroundExportedName "_DiExecContinueBackground@0"
#define DiCommGetAcceptableSettingsExportedName "_DiCommGetAcceptableSettings@16"
#define DiTraceSwitchOnExportedName "_DiTraceSwitchOn@4"
#define DiCoverageGetInfoExportedName "_DiCoverageGetInfo@20"
#define DiMeeGetFeaturesExportedName "_DiMeeGetFeatures@8"
#define DiMemorySetCpuMapExportedName "_DiMemorySetCpuMap@8"
#define DiGdiAddCallbackExportedName "_DiGdiAddCallback@8"
#define DiStateSaveExportedName "_DiStateSave@8"
#define DiMeeDisconnectExportedName "_DiMeeDisconnect@8"
#define DiStateRestoreExportedName "_DiStateRestore@8"
#define DiRegisterClassWriteExportedName "_DiRegisterClassWrite@8"
#define DiMeeInitIOExportedName "_DiMeeInitIO@8"
#define DiRegisterClassReadExportedName "_DiRegisterClassRead@8"
#define DiDirectCommandExportedName "_DiDirectCommand@8"
#define DiTraceGetInstructionsExportedName "_DiTraceGetInstructions@8"
#define DiStateOpenExportedName "_DiStateOpen@8"
#define DiGdiInitMemorySpaceMapExportedName "_DiGdiInitMemorySpaceMap@8"
#define DiDirectAddMenuItemExportedName "_DiDirectAddMenuItem@8"
#define DiProfilingSwitchOnExportedName "_DiProfilingSwitchOn@4"
#define DiMeeSelectExportedName "_DiMeeSelect@4"
#define DiExecSingleStepExportedName "_DiExecSingleStep@4"
#define DiGdiSetConfigExportedName "_DiGdiSetConfig@4"
#define DiCoverageSwitchOnExportedName "_DiCoverageSwitchOn@4"
#define DiMemoryGetCpuMapExportedName "_DiMemoryGetCpuMap@4"
#define DiExecGetStatusExportedName "_DiExecGetStatus@4"
#define DiMemoryGetMapExportedName "_DiMemoryGetMap@4"
#define DiProfileGetInfoExportedName "_DiProfileGetInfo@4"
#define DiGdiSynchronizeExportedName "_DiGdiSynchronize@4"
#define DiMeeEnumExecEnvExportedName "_DiMeeEnumExecEnv@4"
#define DiCpuCurrentExportedName "_DiCpuCurrent@4"
#define DiCpuSelectExportedName "_DiCpuSelect@4"
#define DiProcessExportedName "_DiProcess@4"
#define DiStateCloseExportedName "_DiStateClose@4"
#define DiMeeConnectExportedName "_DiMeeConnect@4"
#define DiRegisterClassDeleteExportedName "_DiRegisterClassDelete@4"
#define DiGdiVersionExportedName "_DiGdiVersion@4"

#endif
