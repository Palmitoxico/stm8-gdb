/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* Read a symbol table in HI-CROSS format.

   Copyright SGS-THOMSON 1995

*/

/* Read symbols from an HI-CROSS file. */


#include "defs.h"
#include "bfd.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "symfile.h"
#include "objfiles.h"
#include "buildsym.h"
#include "complaints.h"
#include "gdb-stabs.h"

#include <fcntl.h>

/* Just in case they're not defined in stdio.h. */
#ifndef SEEK_END
#define SEEK_END 2
#endif
#ifndef O_BINARY
#define O_BINARY 0
#endif

#if 1
#define TRUNCATE_SOURCE_PATHNAME
#endif

#include "hicross.h"
#include "libhicross.h"

#include <sys/types.h>

#include "gdbcore.h"
#include <sys/stat.h>

extern int readnow_symbol_files;

typedef struct HI_Sym_struct {
          struct symbol *sym_ptr;
		  enum function_call_type call_type;
          boolean exported;
		  unsigned long size;
} HI_Sym_type;

extern int
get_filename_and_charpos PARAMS((struct symtab *, char **));

static void
hicross_new_init PARAMS ((struct objfile *));

static void
hicross_symfile_init PARAMS ((struct objfile *));

static void
hicross_symfile_read PARAMS ((struct objfile *, struct section_offsets *, int));

static void
hicross_symfile_finish PARAMS ((struct objfile *));

static void
hicross_psymtab_read PARAMS ((bfd *,
			      struct section_offsets *,
			      struct objfile *));

static void
hicross_msymtab_read PARAMS ((bfd *,  CORE_ADDR, struct objfile *));

static struct section_offsets *
hicross_symfile_offsets PARAMS ((struct objfile *, CORE_ADDR));

static void
record_minimal_symbol PARAMS ((char *, CORE_ADDR, enum minimal_symbol_type,
			       struct objfile *));


/* Initialize anything that needs initializing when a completely new symbol
   file is specified (not just adding some symbols from another file, e.g. a
   shared library).
*/

static void
hicross_new_init (struct objfile *ignore)
{
  buildsym_new_init ();
}


/* hicross_symfile_init ()
   is the HICROSS-specific initialization routine for reading symbols.
   It is passed a struct objfile which contains, among other things,
   the BFD for the file whose symbols are being read, and a slot for
   a pointer to "private data" which we fill with cookies and other
   treats for hicross_symfile_read ().

*/

static void
hicross_symfile_init (struct objfile *objfile)
{
  /* HICROSS objects may be reordered, so set OBJF_REORDERED.  If we
     find this causes a significant slowdown in gdb then we could
     set it in the debug symbol readers only when necessary.  */
  objfile->flags |= OBJF_REORDERED;

  init_entry_point_info (objfile);
}

static void
record_minimal_symbol (char *name,
					   CORE_ADDR address,
                       enum minimal_symbol_type ms_type,
                       struct objfile *objfile)
{
  name = obsavestring (name, strlen (name), &objfile -> symbol_obstack);
  prim_record_minimal_symbol (name, address, ms_type, objfile
#ifdef TARGET_ST18950
			      , INVALID_DATA_SPACE
#endif /* DATA_SPACE */
			      );
}


/* Each partial symbol table entry contains a pointer to private data
   for the read_symtab() function to use when expanding a partial
   symbol table entry to a full symbol table entry.

   For hiread this structure contains the offset to the beginning of
   the module informations that this
   psymtab represents and a pointer to the BFD that the psymtab was
   created from.
   It also contains the name of the object file to open for building
   symtab information  */

#define PST_PRIVATE(p) ((struct symloc *)(p)->read_symtab_private)
#define MODULE_OFFSET(p) (PST_PRIVATE(p)->module_offset)
#define DEBUG_SWAP(p) (PST_PRIVATE(p)->debug_swap)
#define DEBUG_INFO(p) (PST_PRIVATE(p)->debug_info)
#define LANGUAGE(p) (PST_PRIVATE(p)->pst_language)
#define OBJECT_FILENAME(p) (PST_PRIVATE(p)->object_filename)

struct symloc
{
  unsigned int module_offset; /* begining of the HI-CROSS module infos */
  const struct hicross_debug_swap *debug_swap;
  struct hicross_debug_info *debug_info;
  enum language pst_language;
  char *object_filename;
};

/* Things we import explicitly from other modules */

extern int info_verbose;

/* How to parse debugging information for CUR_BFD.  */

static const struct hicross_debug_swap *debug_swap;

/* Pointers to debugging information for CUR_BFD.  */

static struct hicross_debug_info *debug_info;

/* symfile_bfd_open is coming from symfile.c. Sorry it was static ! */
bfd *
symfile_bfd_open (char *name);

/************************ local functions *****************************/


/* start_hiobject is taken from start_stabs in stabsread.c */
/* Initialize anything that needs initializing at the same time as
   start_symtab() is called. */

static void start_hiobject ()
{
  type_vector_length = 0;
  type_vector = (struct type **) 0;
}

/* Call after end_symtab() */

static void end_hiobject ()
{
  if (type_vector)
    {
      free ((char *) type_vector);
    }
  type_vector = 0;
  type_vector_length = 0;
}

/* hicross_builtin_type is taken from rs6000_builtin_type in stabsread.c */
/* HI-CROSS format uses a set of builtin types, indicated by a char value.
   Return the proper type node for a given builtin typenum. */

static struct type *
hicross_builtin_type (char typenum, struct objfile *objfile)
{
  /* We recognize types numbered from 'A' to 'z'  */
  /* This includes an empty slot for type number -0.  */
  struct type *rettype = NULL;

  if (typenum >= 'z' || typenum < 'A')
    {
      static struct complaint msg = {"\
        Builtin-type %d: not supported for HI-CROSS",
				     0, 0};
      complain (&msg, typenum);
      return builtin_type_error;
    }

  switch (typenum)
    {
    case 'C':
      rettype = builtin_type_unsigned_char;
      break;
    case 'c':
      rettype = builtin_type_signed_char; 
      break;
    case 'S':
      rettype = builtin_type_unsigned_short;
      break;
    case 's':
      rettype = builtin_type_short;
      break;
    case 'I':
      rettype = builtin_type_unsigned_int;
      break;
    case 'i':
      rettype = builtin_type_int;
      break;
    case 'L':
      rettype = builtin_type_unsigned_long;
      break;
    case 'l':
      rettype = builtin_type_long;
      break;
    case 'v':
      rettype = builtin_type_void;
      break;
    case 'b':
      rettype = init_type (TYPE_CODE_INT, 1, 0, "byte", objfile);
      break;
    case 'w':
      rettype = init_type (TYPE_CODE_INT, 2, 0, "word", objfile);
      break;
    case 'W':
      rettype = init_type (TYPE_CODE_INT, 4, 0, "doubleword", objfile);
      break;
    case 'f':
      /* IEEE single precision (32 bit).  */
      rettype = builtin_type_float;
      break;
    case 'd':
      /* This is like a float for ST7 target */
      rettype = builtin_type_double;
      break;
    case 'D':
      /* This is like a float for ST7 target */
      rettype = builtin_type_long_double;
      break;
    default :
      {
	static struct complaint msg = {"\
          Builtin-type %c: not supported for ST7",
				     0, 0};
     	complain (&msg, typenum);
	return builtin_type_error;
      }
    }
  return rettype;
}


/* taken from dbx_lookup_type in stabsread.c */   
/* Lookup a structunion type.  Return the address of the slot
   where the type for that index is stored.

   This can be used for finding the type associated with that index
   or for associating a new type with the type.  */ 

static struct type **
hicross_lookup_type (int index)
{
  unsigned old_len;

  /* Work around Hiware bug: index may be -1 in debug informations,
     for example with incomplete struct infos */
  if (index == -1)
    return &builtin_type_error;

  /* Type is defined outside of header files.
	 Find it in this object file's type vector.  */
  if (index >= type_vector_length)
  {
    old_len = type_vector_length;
    if (old_len == 0)
    {
      type_vector_length = INITIAL_TYPE_VECTOR_LENGTH;
      type_vector = (struct type **)
      malloc (type_vector_length * sizeof (struct type *));
    }
    while (index >= type_vector_length)
    {
      type_vector_length *= 2;
    }
    type_vector = (struct type **)
    xrealloc ((char *) type_vector,
              (type_vector_length * sizeof (struct type *)));
    memset (&type_vector[old_len], 0,
            (type_vector_length - old_len) * sizeof (struct type *));
  }
  return (&type_vector[index]);
}

/* taken from dbx_alloc_type in stabsread.c */   
/* Make sure there is a type allocated for index
   and return the type object.
   This can create an empty (zeroed) type object.
   index -1 to return a new type object that is not
   put into the type vector, and so may not be referred to by number. */

static struct type *
hicross_alloc_type (int index, struct objfile *objfile)
{
  register struct type **type_addr;

  if (index == -1)
  {
    return (alloc_type (objfile));
  }

  type_addr = hicross_lookup_type (index);

  /* If we are referring to a type not known at all yet,
     allocate an empty type for it.
     We will fill it in later if we find out how.  */
  if (*type_addr == 0)
  {
    *type_addr = alloc_type (objfile);
  }

  return (*type_addr);
}

 
/* taken from read_type in stabsread.c */   
/* Read type information or a type definition; return the type. */

struct type *
hicross_read_type (hicross_type_component_type *ti,
				   unsigned long NumberOfEnum,
				   enum language lang)
{
  struct type *type = 0;

  switch (ti->kind) {
    case '/' : /* Typedef */
      {
        struct pending *ppt;
        struct symbol *sym;
        int i;


        /* First check to see whether the type has already been
        declared. */

        for (ppt = file_symbols; ppt; ppt = ppt->next)
          for (i = 0; i < ppt->nsyms; i++)
          {
            sym = ppt->symbol[i];

            if (SYMBOL_CLASS (sym) == LOC_TYPEDEF
            && STREQ (SYMBOL_NAME (sym), ti->u.def.name))
              return SYMBOL_TYPE (sym);
          }

        /* build a typedef symbol */
        sym = (struct symbol *) 
          obstack_alloc (&current_objfile -> symbol_obstack, sizeof (struct symbol));
        memset (sym, 0, sizeof (struct symbol));

        SYMBOL_LINE(sym) = 0;            /* unknown */
        SYMBOL_LANGUAGE (sym) = lang;
        SYMBOL_CLASS (sym) = LOC_TYPEDEF;
        SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;
        SYMBOL_NAME (sym) = obsavestring (ti->u.def.name, strlen (ti->u.def.name),
                      &current_objfile -> symbol_obstack);
        SYMBOL_TYPE (sym) = hicross_read_type (ti->next, NumberOfEnum, lang);
        if (TYPE_NAME (SYMBOL_TYPE (sym)) == NULL)
          if (TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_PTR
          || TYPE_CODE (SYMBOL_TYPE (sym)) == TYPE_CODE_FUNC)
          {
            /* If we are giving a name to a type such as "pointer to
            foo" or "function returning foo", we better not set
            the TYPE_NAME.  If the program contains "typedef char
            *caddr_t;", we don't want all variables of type char
            * to print as caddr_t.  This is not just a
            consequence of GDB's type management; PCC and GCC (at
            least through version 2.4) both output variables of
            either type char * or caddr_t with the type number
            defined in the 't' symbol for caddr_t.  If a future
            compiler cleans this up it GDB is not ready for it
            yet, but if it becomes ready we somehow need to
            disable this check (without breaking the PCC/GCC2.4
            case).

            Sigh.

            Fortunately, this check seems not to be necessary
            for anything except pointers or functions.  */
          }
          else
		  {
			  type = SYMBOL_TYPE (sym);

			  if (TYPE_CODE (type) == TYPE_CODE_STRUCT /* C struct record */
				  || TYPE_CODE (type) == TYPE_CODE_UNION /* C union */
				  || TYPE_CODE (type) == TYPE_CODE_ENUM) /* Enumeration type */
				TYPE_TAG_NAME (type) = SYMBOL_NAME (sym);
			  else
				TYPE_NAME (type) = SYMBOL_NAME (sym);
		  }
        add_symbol_to_list (sym, &file_symbols);

        return SYMBOL_TYPE (sym);
      }

    case '(' : /* Function */
      return lookup_function_type (hicross_read_type (ti->u.fun.Return, NumberOfEnum, lang));
    case '*': /* near Pointer */
      return lookup_near_pointer_type (hicross_read_type (ti->next, NumberOfEnum, lang));
    case '@': /* tiny Pointer */
      return lookup_tiny_pointer_type (hicross_read_type (ti->next, NumberOfEnum, lang));
    case 'X': /* Struct */
      type = *hicross_lookup_type (NumberOfEnum + ti->u.su.Index);
      TYPE_CODE (type) = TYPE_CODE_STRUCT;
      return type;
    case 'U': /* Union */
      type = *hicross_lookup_type (NumberOfEnum + ti->u.su.Index);
      TYPE_CODE (type) = TYPE_CODE_UNION;
      return type;
    case 'E': /* Enum */
      type = *hicross_lookup_type (ti->u.enu.Index);
      TYPE_CODE (type) = TYPE_CODE_ENUM;
      return type;
    case '[': /* Array */
      {
        struct type *index_type, *element_type, *range_type;
        int lower, upper;

        index_type = builtin_type_int;
        lower = ti->u.arr.RangeBeg;
        upper = ti->u.arr.ElementNB - 1;
        element_type = hicross_read_type (ti->next, NumberOfEnum, lang);
        range_type = alloc_type (current_objfile);
        range_type =
          create_range_type (range_type, index_type, lower, upper);
        return (create_array_type (alloc_type (current_objfile),
                                   element_type, range_type));
      }

    default :
      return hicross_builtin_type (ti->kind, current_objfile);
  }
        
}

/* hicross_define_symbol is taken from define_symbol from stabsread.c */

static struct symbol *
hicross_define_symbol (hicross_object_type *oi,
	                   unsigned long NumberOfEnum,
                       enum language lang,
	                   unsigned long *size)
{
  struct symbol *sym;
  hicross_type_component_type * ti;

  sym = (struct symbol *) 
    obstack_alloc (&current_objfile -> symbol_obstack, sizeof (struct symbol));
  memset (sym, 0, sizeof (struct symbol));

  SYMBOL_LINE(sym) = 0;			/* unknown */
  SYMBOL_LANGUAGE (sym) = lang;
  switch (oi->k.kind) {
     case VARIABLETAG :
       SYMBOL_VALUE_ADDRESS (sym) = oi->k.u.v.Address;
       SYMBOL_CLASS (sym) = LOC_STATIC;
       break;
     case STRINGTAG :
       SYMBOL_VALUE_ADDRESS (sym) = oi->k.u.s.Position.Address;
       SYMBOL_CLASS (sym) = LOC_STATIC;
       break;
     case PROCEDURETAG :
       SYMBOL_CLASS (sym) = LOC_BLOCK;
	   SYMBOL_VALUE_ADDRESS(sym) = oi->k.u.p.Position.Address;
	   *size = oi->k.u.p.Position.Size;
#if 0
#define TRAP_PROC_FLAG (1L << 30)
	   if (oi->k.u.p.FrameOffset & TRAP_PROC_FLAG)
		{
		   extern void add_interrupt_address_to_list(CORE_ADDR);
		   add_interrupt_address_to_list(oi->k.u.p.Position.Address);
		}
#endif
       break;
     case LABELTAG :
       SYMBOL_VALUE_ADDRESS (sym) = oi->k.u.l.Address;
       SYMBOL_CLASS (sym) = LOC_LABEL;
       break;
     case CONSTTAG :
       SYMBOL_VALUE (sym) = oi->k.u.k.Value;
       SYMBOL_CLASS (sym) = LOC_CONST;
       break;
     default : return NULL;
  }

  /* Read object type in the executable file. */
  (*debug_swap->swap_objtype_in) (current_objfile->obfd, &oi->objtype_offset, &ti);
  SYMBOL_TYPE (sym) = hicross_read_type (ti, NumberOfEnum, lang);

  SYMBOL_NAMESPACE (sym) = VAR_NAMESPACE;

  if (oi->exported == true) {
    /* remove leading '_' while saving global symbol name */
    SYMBOL_NAME (sym) = obsavestring (oi->ObjName + 1,
                                      strlen (oi->ObjName + 1),
				      &current_objfile -> symbol_obstack);
  }
  else {
    SYMBOL_NAME (sym) = obsavestring (oi->ObjName, strlen (oi->ObjName),
				      &current_objfile -> symbol_obstack);
  }

  return sym;
}


/* psymtab_to_symtab_1 is taken from mdebugread.c */

/* Ancillary function to psymtab_to_symtab().  Does all the work
   for turning the partial symtab PST into a symtab, recurring
   first on all dependent psymtabs.  The argument FILENAME is
   only passed so we can see in debug stack traces what file
   is being read.
*/

static void
psymtab_to_symtab_1 (struct partial_symtab *pst, char *filename)
{
  unsigned long mptr, i, j;
  boolean remember_line_misusing = false;
  struct symtab *st;

  if (pst->readin)
    return;
  pst->readin = 1;

  /* Read in all partial symbtabs on which this one is dependent.
     NOTE that we do have circular dependencies, sigh.  We solved
     that by setting pst->readin before this point.  */

  for (i = 0; i < pst->number_of_dependencies; i++)
    if (!pst->dependencies[i]->readin)
      {
	/* Inform about additional files to be read in.  */
	if (info_verbose)
	  {
	    fputs_filtered (" ", gdb_stdout);
	    wrap_here ("");
	    fputs_filtered ("and ", gdb_stdout);
	    wrap_here ("");
	    printf_filtered ("%s...",
			     pst->dependencies[i]->filename);
	    wrap_here ("");	/* Flush output */
	    gdb_flush (gdb_stdout);
	  }
	/* We only pass the filename for debug purposes */
	psymtab_to_symtab_1 (pst->dependencies[i],
			     pst->dependencies[i]->filename);
      }

  /* Do nothing if this is a dummy psymtab.  */

  if (pst->n_global_syms == 0 && pst->n_static_syms == 0
      && pst->textlow == 0 && pst->texthigh == 0)
    return;

  /* Now prepare reading the symbols for this symtab */

  mptr = MODULE_OFFSET (pst);
  debug_swap = DEBUG_SWAP (pst);
  debug_info = DEBUG_INFO (pst);
  current_objfile = pst->objfile;

  /* See comment in parse_partial_symbols about the @stabs sentinel. */
  processing_gcc_compilation = 0;

  start_hiobject ();
  start_symtab (OBJECT_FILENAME (pst), NULL, pst->textlow);
  /* start_symtab created a subfile for the filename.
     We have to eliminate it now. Correct subfiles will be set
     at object file parsing */
  {
    struct subfile *nextsub = current_subfile->next;
    free ((PTR) current_subfile->name);
    free ((PTR) current_subfile);
    current_subfile = NULL;
    subfiles = nextsub;
  }

  /* Now we start the most complicated part of the work :
     we have to open the HI-CROSS object file corresponding to this
     psymtab in order to get the information about the source file name,
     the source line table, the type definitions ...
  */
  {
    bfd *abs_bfd = current_objfile -> obfd; /* absolute file bfd pointer */
    bfd *obj_bfd = symfile_bfd_open (OBJECT_FILENAME (pst)); /* obj file bfd pointer */
    HI_Sym_type *HI_Sym;
    char *object_dirname = NULL;
    char *eod;
    
#define MAX_UNIT_KEY 100 /* ### Maximum unit key inside object file */
    /* Translation table between a unit key and a subfile */
    struct subfile * subfile_table[MAX_UNIT_KEY];
    struct subfile * my_subfile;
    unsigned long code_address, source_pos;


    /* Initialize object_dirname to be the directory of the current
       object file. This will be used to initialize the dirname of
       all corresponding source files. Will remain NULL if no dirname
       can be extracted */
    if (OBJECT_FILENAME (pst) != basename (OBJECT_FILENAME (pst)))
    {
       object_dirname =
         obstack_alloc (&current_objfile->symbol_obstack, strlen(OBJECT_FILENAME (pst)) + 1);
       strcpy (object_dirname, OBJECT_FILENAME (pst));
       eod = basename (object_dirname);
	   if (*(eod -1) == ':')
         *eod = 0;
       else
         *(eod-1) = 0;
    }
	else if (current_objfile->name != basename (current_objfile->name))
    {
       object_dirname =
         obstack_alloc (&current_objfile->symbol_obstack, strlen(current_objfile->name) + 1);
       strcpy (object_dirname, current_objfile->name);
       eod = basename (object_dirname);
	   if (*(eod -1) == ':')
         *eod = 0;
       else
         *(eod-1) = 0;
    }
     
    /* --- Parse the source units --- */
  {
    hicross_unit_type ui;
    unsigned long i, nbunits, offset;

    nbunits = HICROSS_DATA(obj_bfd)->hd.NBofUnits;
    offset = HICROSS_DATA(obj_bfd)->unit_list_file_offset;

  /* ### Bug in ST7 HICROSS format : UnitKey are wrong. */
  /* Found a workaround that works only with option -readnow */
  if (readnow_symbol_files)
    for (i = 0; i < nbunits; i++) {
	  char *pt;
      (*debug_swap->swap_unit_in) (obj_bfd, &offset, &ui);
      pt = obstack_alloc (&current_objfile->symbol_obstack, strlen(basename (ui.UnitName)) + 1);
	  strcpy (pt, basename (ui.UnitName));
      start_subfile (pt, object_dirname);
      /* ### try to repair the bug: ui.UnitKey is unreliable
      subfile_table[ui.UnitKey] = current_subfile; */
      subfile_table[i] = current_subfile;
      current_subfile->language = LANGUAGE (pst);
    }
  else
    {
	  char *source_pathname;

	  /* Read unit name in Unit List block located in Directory block. */
      (*debug_swap->swap_unit_in) (obj_bfd, &offset, &ui);

#ifdef TRUNCATE_SOURCE_PATHNAME
	  /* Temporary patch: remove directory from pathname. */
	  source_pathname = obstack_alloc (&current_objfile->symbol_obstack, strlen(basename (ui.UnitName)) + 1);
	  strcpy (source_pathname, basename (ui.UnitName));
#else
	  /* Keep the whole pathname: see bug MBTst06407 in the HDS3 DDTS database. */
	  source_pathname = obstack_alloc (&current_objfile->symbol_obstack, strlen(ui.UnitName) + 1);
	  strcpy (source_pathname, ui.UnitName);
#endif
#if 0 /* Keep letter case. */
      /* Force unit filename to lower case. */
	  {
        char *p;
        for (p = source_pathname; p < source_pathname + strlen( source_pathname ); p++ )
		{
          if( isupper( *p ) )
            *p = _tolower( *p );
		}
	  }
#endif
      start_subfile (source_pathname, object_dirname);

      subfile_table[0] = current_subfile;
      current_subfile->language = LANGUAGE (pst);
	  record_debugformat("HI-CROSS");
	}
  }

  /* --- Parse the enum information --- */
  {
	unsigned long i, j, offset;
	hicross_enumeration_type ei;
	hicross_name_value_pair_type nvi;

	offset = HICROSS_DATA(obj_bfd)->enumeration_offset;
	
	/* perform a first pass to allocate enumeration type containers in
       order to support forward references */
    for (i = 0; i < HICROSS_DATA (obj_bfd)->hd.NBofEnumerations; i++) {
        struct type * type;
        type = hicross_alloc_type (i, current_objfile);
    }

	/* now fill the enum types with field information */
    for (i = 0; i < HICROSS_DATA (obj_bfd)->hd.NBofEnumerations; i++) {
      (*debug_swap->swap_enum_in) (obj_bfd, &offset, &ei);
      {
        struct type * type;

        type = *hicross_lookup_type (i);

        TYPE_LENGTH (type) = TYPE_LENGTH (builtin_type_unsigned_int);
		TYPE_FLAGS (type) = TYPE_FLAG_UNSIGNED;
        TYPE_NFIELDS (type) = ei.NBofPairs;
        TYPE_FIELDS (type) = (struct field *)
          TYPE_ALLOC (type, sizeof (struct field) * ei.NBofPairs);
        memset (TYPE_FIELDS (type), 0, sizeof (struct field) * ei.NBofPairs);

        for (j = 0; j < ei.NBofPairs; j++) {
          (*debug_swap->swap_name_value_pair_in) (obj_bfd, &offset, &nvi);
          TYPE_FIELD_NAME (type, j) =
            (char *)obstack_alloc (&current_objfile->type_obstack, strlen (nvi.Name) + 1);
          strcpy (TYPE_FIELD_NAME (type, j), nvi.Name);
		  TYPE_FIELD_TYPE (type, j) = NULL;
		  TYPE_FIELD_BITPOS (type, j) = nvi.Value;
		  TYPE_FIELD_BITSIZE (type, j) = 0;
        }
      }
	}
  }

    /* --- Parse the struct union information --- */
  {
    unsigned long i, j, offset;
    hicross_struct_union_type sui;
    hicross_field_type fi;
	unsigned long NumberOfEnum = HICROSS_DATA (obj_bfd)->hd.NBofEnumerations;

    offset = HICROSS_DATA(obj_bfd)->struct_union_offset;

    /* perform a first pass to allocate struct/union type containers in
       order to support forward references */
    for (i = 0; i < HICROSS_DATA (obj_bfd)->hd.NBofStructUnions; i++) {
        struct type * type;
        type = hicross_alloc_type (i + NumberOfEnum, current_objfile);
    }

    /* now fill the struct/union types with field information */
    for (i = 0; i < HICROSS_DATA (obj_bfd)->hd.NBofStructUnions; i++) {
      (*debug_swap->swap_struct_union_in) (obj_bfd, &offset, &sui);
      {
        struct type * type;

        type = *hicross_lookup_type (i + NumberOfEnum);

        INIT_CPLUS_SPECIFIC(type);
        TYPE_LENGTH (type) = sui.TotalSize;
        TYPE_NFIELDS (type) = sui.NBofObjects;
        TYPE_FIELDS (type) = (struct field *)
          TYPE_ALLOC (type, sizeof (struct field) * sui.NBofObjects);
        memset (TYPE_FIELDS (type), 0, sizeof (struct field) * sui.NBofObjects);

        for (j = 0; j < sui.NBofObjects; j++) {
          (*debug_swap->swap_field_in) (obj_bfd, &offset, &fi);
          TYPE_FIELD_NAME (type, j) =
            (char *)obstack_alloc (&current_objfile->type_obstack, strlen (fi.ObjectName) + 1);
          strcpy (TYPE_FIELD_NAME (type, j), fi.ObjectName);
          TYPE_FIELD_TYPE (type, j) =
            hicross_read_type (fi.ObjectType, NumberOfEnum, LANGUAGE (pst));
          TYPE_FIELD_BITPOS (type, j) = fi.Offset * TARGET_CHAR_BIT;
          if (fi.bitfield) {
            TYPE_FIELD_BITSIZE (type, j) = fi.BitFieldWidth;
            TYPE_FIELD_BITPOS (type, j) += 
              (TARGET_CHAR_BIT * fi.Size) - fi.LeasSignificantBit - fi.BitFieldWidth;
          }
        }
      }
    }
  }

    /* Allocate HI_Sym table */
  {
    unsigned int nb = HICROSS_DATA(obj_bfd)->hd.NBofObjects;
    HI_Sym = (HI_Sym_type *) alloca (sizeof (HI_Sym_type) *
                                     nb);
    memset (HI_Sym, 0, sizeof (HI_Sym_type) *
                       nb);
  }


    /* --- Parse the absolute module symbols  --- */
  {
  /* Now read the symbols from the HI-CROSS absolute module in HI_Sym table
     indexed with Hiware object number.
     This can be done only after object file parsing in order to
     get the right struct and union type definitions. */
    unsigned long optr;
    hicross_module_type mi;
    hicross_object_type oi;
	unsigned long NumberOfEnum = HICROSS_DATA (obj_bfd)->hd.NBofEnumerations;

	/* "mptr" is the module offset in the module list in the Module Directory block. */
    (*debug_swap->swap_module_in) (abs_bfd, &mptr, &mi);

    optr = mi.objlist_offset;

    /* For each hicross object (of the chosen object module)
	   listed in the object list (beginning with "ObjListTAG") of the absolute module. */
    for (j = 0; j < mi.NBofObjects; j++) {

      (*debug_swap->swap_object_in) (abs_bfd, &optr, &oi);
      if (oi.k.kind != STRINGTAG) /* eliminates initialization strings */
	  {
        HI_Sym[oi.ObjNB].sym_ptr =
          hicross_define_symbol (&oi, NumberOfEnum, LANGUAGE (pst), &(HI_Sym[oi.ObjNB].size));
        HI_Sym[oi.ObjNB].exported = oi.exported;
#define TRAP_PROC_FLAG (1L << 30)
		if (oi.k.kind == PROCEDURETAG)
		{
			if (oi.k.u.p.FrameOffset & TRAP_PROC_FLAG)
				HI_Sym[oi.ObjNB].call_type = INTERRUPT_HANDLER;
			else
				HI_Sym[oi.ObjNB].call_type = NEAR_CALL;
		}
	  }
    }
  }

    /* --- Parse the object module symbols  --- */

  {
    /* We parse the symbols of the object module for various purposes.
	   For a PROCEDURETAG, we parse its linecode information.
	   For a PROCEDURETAG, we retrieve its local symbols, move them in the local
	   symbol list of the procedure. They are removed at the same time 
	   from HI_Sym.
	   Finally, remaining symbols in HI_Sym are moved to global or static
	   lists. */
    unsigned long i, j, offset;
    unsigned long offset2; 
    struct symbol * proc_sym;
    hicross_source_position_type source_info;
    hicross_proc_source_code_pair_type source_code_pair;
    hicross_object_summary_type hicross_symbol;
	hicross_proc_locals_type proc_locals;
	hicross_proc_PC_SP_list_type proc_PC_SP_list;
	struct block * proc_block;

    offset = HICROSS_DATA(obj_bfd)->object_list_file_offset;
	for (i = 0; i < HICROSS_DATA(obj_bfd)->hd.NBofObjects; i++) {

      (*debug_swap->swap_object_summary_in) (obj_bfd, &offset, &hicross_symbol);
	  offset2 = hicross_symbol.FilePosition; 

      /* +++ Parse linecode infos +++ */
      if ((hicross_symbol.kind == PROCEDURETAG) && 
          (proc_sym = HI_Sym[i].sym_ptr) &&
          (*debug_swap->swap_proc_source_position_in)(obj_bfd, &offset2, &source_info)) {

      /* ### Bug in ST7 HICROSS format : UnitKey are wrong. */
	  /* Found a workaround that works only with option -readnow */
      if (readnow_symbol_files)
        my_subfile = subfile_table[source_info.Unit];
      else
        my_subfile = subfile_table[0];

        /* Calculate here the initial code address of the procedure from the
           symbol name */
        code_address = SYMBOL_VALUE_ADDRESS(HI_Sym[i].sym_ptr);
        source_pos = source_info.SourceStart;

        for (j = 0; j < source_info.NBofSourceCodePairs; j++) {
          (*debug_swap->swap_proc_source_code_pair_in) (obj_bfd, &offset2, &source_code_pair);

          /* HI-CROSS MISUSE : we record here (source position, address)
             instead of (source line, address) because
             we are not able to translate a source position into a source
             line before opening source file. This information will be
             translated and patched later just after source opening. */

          remember_line_misusing = true;

          code_address = code_address + source_code_pair.DeltaCodePos;
          source_pos = source_pos + source_code_pair.DeltaSourcePos;
          record_line (my_subfile, source_pos, code_address);
        }
      }

      /* +++ Parse information on local variables +++ */

      if ((hicross_symbol.kind == PROCEDURETAG) && 
          (proc_sym = HI_Sym[i].sym_ptr)) {

        register struct context_stack *new;

        new = push_context (0, SYMBOL_VALUE_ADDRESS(proc_sym));

        (*debug_swap->swap_proc_locals_in)(obj_bfd, &offset2, &proc_locals);
		for (j = proc_locals.first; j <= proc_locals.last; j++) {
		  if (HI_Sym[j].sym_ptr != NULL) /* do not add hiware eliminated symbols */
            add_symbol_to_list (HI_Sym[j].sym_ptr, &local_symbols);
		  HI_Sym[j].sym_ptr = NULL; /* remove symbol from HI_Sym */
		}

        /* Now starts a tedious work: parameters cannot be found
		   from the proc info list. The only way we have to recognize
		   them in the symbol list is to look at the symbol name.
		   <name of the Nth parameter>=_<name of the function>p<N-1> */
		{
		  const char *procname = SYMBOL_NAME(proc_sym);
		  const unsigned int procname_length = strlen (procname);

		  for (j = 0; j < HICROSS_DATA(obj_bfd)->hd.NBofObjects; j++) {
            char * name;

		    if (!HI_Sym[j].sym_ptr) continue;
			if (SYMBOL_NAMESPACE (HI_Sym[j].sym_ptr) != VAR_NAMESPACE) continue;

		    name = SYMBOL_NAME(HI_Sym[j].sym_ptr);
		    if (!strncmp (procname,
		                  name+1, /* skip first '_' */
		                  procname_length))
			{
			 /* Check that remaining chars are like p<N> */
			 int wrongsym = 0;
			 name = name + procname_length + 1;
			 if (*name++ != 'p') continue;
			 while (*name != '\0')
			   if (!isdigit (*name++))
			   {
			     wrongsym = 1;
			     break;
               }
             if (wrongsym) continue;

             add_symbol_to_list (HI_Sym[j].sym_ptr, &local_symbols);
			 HI_Sym[j].sym_ptr = NULL; /* remove symbol from HI_Sym */
			 break;
			}
		  }
		}

	  /* +++ Parse information on SP PC pairs +++ */

      if ((hicross_symbol.kind == PROCEDURETAG) && 
          (proc_sym = HI_Sym[i].sym_ptr)) {
        (*debug_swap->swap_proc_SP_PC_list_in)(obj_bfd, &offset2, &proc_PC_SP_list);
		}

      new = pop_context ();
      proc_block = finish_block (proc_sym, &local_symbols, new->old_blocks,
                                 SYMBOL_VALUE_ADDRESS(proc_sym),
                                 SYMBOL_VALUE_ADDRESS(proc_sym) + HI_Sym[i].size,
                                 current_objfile);
      BLOCK_CALL_TYPE(proc_block) = HI_Sym[i].call_type;

	  /* +++ Store (PC,SP) pairs in the block dedicated to the procedure. +++ */
	  proc_block->nPC_SP_pairs = proc_PC_SP_list.NBofPC_SP_pairs;
	  if (proc_block->nPC_SP_pairs)
		{
		  proc_block->PC_SP_pairs =
            (hicross_proc_PC_SP_pair_type *)
		      obstack_alloc(&current_objfile->symbol_obstack,
                            proc_block->nPC_SP_pairs * sizeof(hicross_proc_PC_SP_pair_type));
	      memcpy(proc_block->PC_SP_pairs, proc_PC_SP_list.PC_SP_pairs,
		         proc_block->nPC_SP_pairs * sizeof(hicross_proc_PC_SP_pair_type));
	      free(proc_PC_SP_list.PC_SP_pairs);
		}

      /* +++ Update source position of the symbol +++ */
#if 0 /* NOT YET IMPLEMENTED */
#endif

      }
    }

    /* --- Treat remaining global and static symbols --- */
    for (i = 0; i<HICROSS_DATA(obj_bfd)->hd.NBofObjects; i++)
      if (HI_Sym[i].sym_ptr)
        if (HI_Sym[i].exported)
          add_symbol_to_list (HI_Sym[i].sym_ptr, &global_symbols);
        else
          add_symbol_to_list (HI_Sym[i].sym_ptr, &file_symbols);

  }

  /* Cleanup the object file reding */
  bfd_close (obj_bfd);
  }


  /* set sort_linevec to 1 because hi-cross linecodes may have been got
     in wrong order. Idem for bolcks */
  st = end_symtab (pst->texthigh, pst->objfile, SECT_OFF_TEXT);
  end_hiobject ();

  /* Sort the symbol table now, we are done adding symbols to it.
     We must do this before parse_procedure calls lookup_symbol.  */
  sort_symtab_syms (st);
  
  /* Now link the psymtab and the symtab.  */
  pst->symtab = st;

  current_objfile = NULL;
}

/* hicross_psymtab_to_symtab is taken from mdebugread.c */

/* Exported procedure: Builds a symtab from the PST partial one.
   Restores the environment in effect when PST was created, delegates
   most of the work to an ancillary procedure, and sorts
   and reorders the symtab list at the end */

static void
hicross_psymtab_to_symtab (struct partial_symtab *pst)
{

  if (!pst)
    return;

  if (info_verbose)
    {
      printf_filtered ("Reading in symbols for %s...", pst->filename);
      gdb_flush (gdb_stdout);
    }

  psymtab_to_symtab_1 (pst, pst->filename);

  if (info_verbose)
    printf_filtered ("done.\n");
}

/* hicross_end_psymtab is taken from end_psymtab in dbxread.c */

/* Close off the current usage of PST.  
   Returns PST or NULL if the partial symtab was empty and thrown away.

   FIXME:  List variables and peculiarities of same.  */

static struct partial_symtab *
hicross_end_psymtab (struct partial_symtab *pst,
                     char **include_list,
                     int num_includes,
                     struct partial_symtab **dependency_list,
                     int number_dependencies)
{
  int i;
  struct partial_symtab *p1;
  struct objfile *objfile = pst -> objfile;

  pst->n_global_syms =
    objfile->global_psymbols.next - (objfile->global_psymbols.list + pst->globals_offset);
  pst->n_static_syms =
    objfile->static_psymbols.next - (objfile->static_psymbols.list + pst->statics_offset);

  pst->number_of_dependencies = number_dependencies;
  if (number_dependencies)
    {
      pst->dependencies = (struct partial_symtab **)
	obstack_alloc (&objfile->psymbol_obstack,
		       number_dependencies * sizeof (struct partial_symtab *));
      memcpy (pst->dependencies, dependency_list,
	     number_dependencies * sizeof (struct partial_symtab *));
    }
  else
    pst->dependencies = 0;

  for (i = 0; i < num_includes; i++)
    {
      struct partial_symtab *subpst =
	allocate_psymtab (include_list[i], objfile);

      subpst->section_offsets = pst->section_offsets;
      subpst->read_symtab_private =
	  (char *) obstack_alloc (&objfile->psymbol_obstack,
				  sizeof (struct symloc));
      /* ### NYI LDSYMOFF(subpst) =
      LDSYMLEN(subpst) =
	  subpst->textlow =
	    subpst->texthigh = 0; */

      /* We could save slight bits of space by only making one of these,
	 shared by the entire set of include files.  FIXME-someday.  */
      subpst->dependencies = (struct partial_symtab **)
	obstack_alloc (&objfile->psymbol_obstack,
		       sizeof (struct partial_symtab *));
      subpst->dependencies[0] = pst;
      subpst->number_of_dependencies = 1;

      subpst->globals_offset =
	subpst->n_global_syms =
	  subpst->statics_offset =
	    subpst->n_static_syms = 0;

      subpst->readin = 0;
      subpst->symtab = 0;
      subpst->read_symtab = pst->read_symtab;
    }

  sort_pst_symbols (pst);

  /* If there is already a psymtab or symtab for a file of this name, remove it.
     (If there is a symtab, more drastic things also happen.)
     This happens in VxWorks.  */
  free_named_symtabs (pst->filename);

  if (num_includes == 0
   && number_dependencies == 0
   && pst->n_global_syms == 0
   && pst->n_static_syms == 0) {
    /* Throw away this psymtab, it's empty.  We can't deallocate it, since
       it is on the obstack, but we can forget to chain it on the list.  */
    struct partial_symtab *prev_pst;

    /* First, snip it out of the psymtab chain */

    if (pst->objfile->psymtabs == pst)
      pst->objfile->psymtabs = pst->next;
    else
      for (prev_pst = pst->objfile->psymtabs; prev_pst; prev_pst = pst->next)
	if (prev_pst->next == pst)
	  prev_pst->next = pst->next;

    /* Next, put it on a free list for recycling */

    pst->next = pst->objfile->free_psymtabs;
    pst->objfile->free_psymtabs = pst;

    /* Indicate that psymtab was thrown away.  */
    pst = (struct partial_symtab *)NULL;
  }
  return pst;
}

/*
LOCAL FUNCTION

	read_hicross_number -- read an hicross number from a file

SYNOPSIS

	long read_hicross_number (FILE *fp)

DESCRIPTION

	read, decode and return an hicross number from the current position of the
	file pointed by fp.
*/

static long
read_hicross_number (FILE *fp)
{
#define STOP_BIT 0x80
#define SIGN_BIT 0x40

  char byte = fgetc(fp);
  unsigned long num = 0;
  unsigned long i = 1;
  while ((byte & STOP_BIT) == 0) {
     num = num + byte * i;
     i = i * 128;
     byte = fgetc(fp);
  }
  num = num + (byte & 0x3F) * i;
  if (byte & SIGN_BIT)
    return -num;
  else
    return num;
}

/*

LOCAL FUNCTION

	hicross_find_source_name -- read the source name from an object file

SYNOPSIS

	int hicross_find_source_name (char *source_name, char *object_name)

DESCRIPTION

	Given an object file name, open the object file, check the time stamp
	with the absolute file abfd and find inside the source name.
	The base name of the source file is returned in source_name.
	Return 1 if OK or return 0 if
	the information cannot be found, either because the object file does
	not exist or is invalid.
*/

static int 
hicross_find_source_name (bfd *abfd, char *source_name, char *object_name) 
{
  char *absolute_name;
  int desc, i;
  FILE *fp;
  unsigned long offset;
  long dummy;
  char result[200] = "";
  char * pt;
  struct stat st;

  /* Look down path for it, allocate 2nd new malloc'd copy.  */
  desc = openp (getenv ("PATH"), 1, object_name, O_RDONLY | O_BINARY, 0, &absolute_name);

  if (desc < 0)
    {
      /* Didn't work.  Try using just the basename. */
      char *p = basename (object_name);
      if (p != object_name)
        desc = openp (getenv ("PATH"), 1, p, O_RDONLY | O_BINARY, 0, &absolute_name);
      if (desc >= 0)
        strcpy (object_name, p); /* UGLY but necessary to clean object name */
    }

  if (desc < 0)
      return 0;

  if (fstat (desc, &st) < 0)
  {
    close (desc);
    return 0;
  }

  /* Check that the abs file is more recent than the obj file */
  {
    long exec_mtime;
    exec_mtime = bfd_get_mtime(abfd);
    if (exec_mtime && exec_mtime < st.st_mtime)
    {
      warning ("Object file %s is more recent than the executable file. Debug information are ignored.\n",
                 object_name);
      close (desc);
      return 0;
    }
  }

  fp = fdopen (desc, "rb");

  /* get the directory size */
  fseek (fp, -4, SEEK_END);
  for (i = 0, offset = 0; i < 4; i++)
    offset = fgetc (fp) + offset * 256; 
  /* go to beginning of Directory */
  fseek (fp, -offset, SEEK_END);
  /* parse informations until UnitList */
  if (fgetc (fp) != 0x44) /* DirectoryTAG */
  {
    fclose (fp);
    close (desc);
    return 0;
  }
  dummy = read_hicross_number (fp); /* Enumeration-ObjPosNUMBER */
  dummy = read_hicross_number (fp); /* StructUnion-ObjPosNUMBER */
  dummy = read_hicross_number (fp); /* TypeSizes-ObjPosNUMBER */
  dummy = fgetc (fp); /* Model */
  dummy = fgetc (fp); /* ProgrammingLanguage */
  dummy = fgetc (fp); /* Processor */
  if (fgetc (fp) != 0x55) /* UnitListTAG */
  {
    fclose (fp);
    close (desc);
    return 0;
  }
  dummy = read_hicross_number (fp); /* NUMBEROfUnits */
  i = 0;
  while ((result[i++] = (char) fgetc (fp)) != '\0');
  pt = result;
#ifdef TRUNCATE_SOURCE_PATHNAME
  /* Temporary patch: remove directory from pathname. */
  pt = basename (pt);
#endif
  strcpy (source_name, pt);
#if 0 /* Keep letter case. */
  /* Force source name to lower case. */
  for (pt = source_name; pt < source_name + strlen( source_name ); pt++ )
  {
     if( isupper( *pt ) )
        *pt = _tolower( *pt );
  }
#endif

  fclose (fp);
  close (desc);
  return 1; /* OK */
}

/*

LOCAL FUNCTION

	hicross_psymtab_read -- read the symbol table of a HI-CROSS file

SYNOPSIS

	void hicross_psymtab_read (bfd *abfd, 
				               struct section_offsets *addr,
			                   struct objfile *objfile)

DESCRIPTION

	Given an open bfd, a base address to relocate symbols to, build the
	partial_symtab list of the objfile along with adding all the global
	function and data symbols to the minimal symbol table.

	For the HI-CROSS format, there is one psymtab per module linked
	inside the absolute file. The filename of one psymtab is the
	the object module filename and not the source filename as
	usual for other formats. The aim of one psymtab is to hold
	the maximum number of pieces of information about one module
	whithout opening it.
	Module objects will be opened and read later at symtab building.
*/

static void
hicross_psymtab_read (bfd *abfd,
                      struct section_offsets *addr,
                      struct objfile *objfile)
{
  struct partial_symtab *pst;
  hicross_module_type mi;
  hicross_object_type oi;
  unsigned long i,j;
  unsigned long mptr, saved_mptr, optr;
  unsigned long value;
  enum address_class class;
  enum minimal_symbol_type ms_type;

  char source_name[200];
  int object_valid = 1;

  debug_swap = &HICROSS_BACKEND (abfd)->debug_swap;
  debug_info = &HICROSS_DATA (abfd)->debug_info;

  mptr = HICROSS_DATA (abfd)->module_list_file_offset;
  
  /* for each hicross object module. */
  for (i = 0; i < HICROSS_DATA (abfd)->hm.NBofModules; i++) {

      saved_mptr = mptr;

      (*debug_swap->swap_module_in) (abfd, &mptr, &mi);

      /* We have a serious problem with the HICROSS format:
	     we need now the name of the source file to make it appear
		 correctly in the psymtab information.
         For that we have to open quickly the object module just to get the
		 source name */
	  strcpy (source_name, "");
      object_valid = hicross_find_source_name (abfd, source_name, mi.ModuleName);

      pst = start_psymtab_common (objfile, addr,
				  source_name,
				  mi.LowAddr,
				  objfile->global_psymbols.next,
				  objfile->static_psymbols.next);
      pst->read_symtab_private = ((char *)
				  obstack_alloc (&objfile->psymbol_obstack,
						 sizeof (struct symloc)));
      memset ((PTR) pst->read_symtab_private, 0, sizeof (struct symloc));


      if (object_valid)
	  {
        /* The way to turn this into a symtab is to call... */
        pst->read_symtab = hicross_psymtab_to_symtab;
        MODULE_OFFSET (pst) = saved_mptr; /* remember the module position */
        DEBUG_SWAP (pst) = debug_swap;
        DEBUG_INFO (pst) = debug_info;
		OBJECT_FILENAME (pst) = obstack_alloc (&objfile->psymbol_obstack,
			                                   strlen (mi.ModuleName) +1);
		strcpy (OBJECT_FILENAME (pst), mi.ModuleName); /* remember the object file name */
#if 0
        /* force object filename to lower case */
		{
          char *pt;
          for (pt = OBJECT_FILENAME (pst); pt < OBJECT_FILENAME (pst) + strlen( OBJECT_FILENAME (pst) ); pt++ )
		  {
            if( isupper( *pt ) )
              *pt = _tolower( *pt );
		  }
		}
#endif
	  }
	  else
	    /* prevent any symtab read */
	    pst->readin = 1;

      switch (mi.Language) {
        case 'C' : LANGUAGE (pst) = language_c;
                   break;
        case 'A' : LANGUAGE (pst) = language_asm;
                   break;
        default : LANGUAGE (pst) = language_unknown;
      }

      pst->texthigh = mi.HighAddr;

      /* Now get the psymbols */

      /* Add the entry point symbol _ENTRY_POINT */
	      prim_record_minimal_symbol ("_ENTRY_POINT",
					abfd->start_address,
					mst_text,
					objfile);
      
      optr = mi.objlist_offset;

      /* for each hicross module */
      for (j = 0; j < mi.NBofObjects; j++) {

        (*debug_swap->swap_object_in) (abfd, &optr, &oi);

        switch (oi.k.kind) {
           case VARIABLETAG :
              value = oi.k.u.v.Address;
              ms_type = mst_data;
              class = LOC_STATIC;
              break;
	   case STRINGTAG :
	          continue; /* Do nothing. This is the initialisation part of a string */
	   case PROCEDURETAG :
	          value = oi.k.u.p.Position.Address;
              ms_type = mst_text;
              class = LOC_BLOCK;
              break;
	   case LABELTAG :
	          value = oi.k.u.l.Address;
              ms_type = mst_text;
              class = LOC_BLOCK;
              break;
	   case CONSTTAG :
	          value = oi.k.u.k.Value;
              ms_type = mst_text;
              class = LOC_CONST;
              break;
           default : return;
        }

    if (value == 0xffffffff)
       continue; /* Do nothing. This is an optimized away symbol */ 

	/* Use this gdb symbol */
    if (oi.exported == true) {
          /* remove leading '_' while saving global symbol name */
      /* Build a minimal symbol */
      prim_record_minimal_symbol (oi.ObjName + 1,
					value,
					ms_type,
					objfile
#ifdef TARGET_ST18950
			      , INVALID_DATA_SPACE
#endif /* DATA_SPACE */
					);
	  if (object_valid)
	    add_psymbol_to_list (oi.ObjName + 1,
		strlen (oi.ObjName + 1),
		VAR_NAMESPACE, class,
                &objfile->global_psymbols,
                0,
				(CORE_ADDR) value,
		LANGUAGE(pst), objfile);
        }
	else {
      /* Build a minimal symbol */
      prim_record_minimal_symbol (oi.ObjName,
					value,
					ms_type,
					objfile
#ifdef TARGET_ST18950
			      , INVALID_DATA_SPACE
#endif /* DATA_SPACE */
					);
	  if (object_valid)
	    add_psymbol_to_list (oi.ObjName, strlen (oi.ObjName),
		VAR_NAMESPACE, class,
                &objfile->static_psymbols,
                (long) value,
				(CORE_ADDR) 0,
		LANGUAGE (pst), objfile);
        }

      }

      hicross_end_psymtab (pst, NULL, 0, NULL, 0);

  }

  /* we initialize the <unknown type> description to some default value in order to
     avoid crashes for ptype command. */
  INIT_CPLUS_SPECIFIC(builtin_type_error);
}


/*

LOCAL FUNCTION

	hicross_msymtab_read -- read the symbol table of a HI-CROSS file

SYNOPSIS

	void hicross_msymtab_read (bfd *abfd,
	                           CORE_ADDR addr,
			                   struct objfile *objfile)

DESCRIPTION

	Given an open bfd, a base address to relocate symbols to,
	add all the global function and data symbols to the minimal symbol table.
*/

static void
hicross_msymtab_read (bfd *abfd, CORE_ADDR addr, struct objfile *objfile)
{
  unsigned int storage_needed;
  asymbol *sym;
  asymbol **symbol_table;
  unsigned int number_of_symbols;
  unsigned int i;
  struct cleanup *back_to;
  CORE_ADDR symaddr;
  enum minimal_symbol_type ms_type;
  
  storage_needed = bfd_get_symtab_upper_bound (abfd);
  if (storage_needed > 0)
    {
      symbol_table = (asymbol **) xmalloc (storage_needed);
      back_to = make_cleanup (free, symbol_table);
      number_of_symbols = bfd_canonicalize_symtab (abfd, symbol_table); 
  
      for (i = 0; i < number_of_symbols; i++)
	{
	  sym = symbol_table[i];
	  if (sym -> flags & BSF_GLOBAL)
	    {
	      /* Bfd symbols are section relative. */
	      symaddr = sym -> value + sym -> section -> vma;
	      /* Relocate all non-absolute symbols by base address.  */
	      if (sym -> section != &bfd_abs_section)
		{
		  symaddr += addr;
		}

	      if (sym -> section == &bfd_abs_section)
		{
		  ms_type = mst_abs;
		}

	      /* For non-absolute symbols, use the type of the section
		 they are relative to, to intuit text/data.  Bfd provides
		 no way of figuring this out for absolute symbols. */
	      else if (sym -> section -> flags & SEC_CODE)
		{
		  ms_type = mst_text;
		}
	      else if (sym -> section -> flags & SEC_DATA)
		{
		  ms_type = mst_data;
		}
	      else
		{
		  ms_type = mst_unknown;
		}
	      record_minimal_symbol ((char *) sym -> name, symaddr, ms_type,
				     objfile);
	    }
	}
      do_cleanups (back_to);
    }
}


/* Scan and build partial symbols for a symbol file.
   We have been initialized by a call to hicross_symfile_init, which 
   currently does nothing.

   SECTION_OFFSETS is a set of offsets to apply to relocate the symbols
   in each section.  We simplify it down to a single offset for all
   symbols.  FIXME.

   MAINLINE is true if we are reading the main symbol
   table (as opposed to a shared lib or dynamically loaded file).

   This function only does the minimum work necessary for letting the
   user "name" things symbolically; it does not read the entire symtab.
   Instead, it reads the external and static symbols and puts them in partial
   symbol tables.  When more extensive information is requested of a
   file, the corresponding partial symbol table is mutated into a full
   fledged symbol table by going back and reading the symbols
   for real. */

static void
hicross_symfile_read (struct objfile *objfile,
                      struct section_offsets *section_offsets,
                      int mainline)
{
  bfd *abfd = objfile -> obfd;
  struct cleanup *back_to;
  CORE_ADDR offset;
#if 0
  extern void init_interrupt_address_list(void);
  init_interrupt_address_list();
#endif
  init_minimal_symbol_collection ();
  back_to = make_cleanup (discard_minimal_symbols, 0);

  /* FIXME, should take a section_offsets param, not just an offset.  */

  offset = ANOFFSET (section_offsets, 0);

  /* Process the HI-CROSS export records, which become the bfd's canonical symbol
     table. */

#if 0 /* msymtab read is now merged inside psymtab read */
  hicross_msymtab_read (abfd, offset, objfile);
#endif
  hicross_psymtab_read (abfd, section_offsets, objfile);

  /* FIXME:  We could locate and read the optional native debugging format
     here and add the symbols to the minimal symbol table. */

  if (!have_partial_symbols ())
    {
      wrap_here ("");
      printf_filtered ("(no debugging symbols found)...");
      wrap_here ("");
    }

  /* Install any minimal symbols that have been collected as the current
     minimal symbols for this objfile. */

  install_minimal_symbols (objfile);

  do_cleanups (back_to);
}


/* Perform any local cleanups required when we are done with a particular
   objfile.  I.E, we are in the process of discarding all symbol information
   for an objfile, freeing up all memory held for it, and unlinking the
   objfile struct from the global list of known objfiles. */

static void
hicross_symfile_finish (struct objfile *objfile)
{
  if (objfile -> sym_private != NULL)
    {
      mfree (objfile -> md, objfile -> sym_private);
    }
}

/* HI-CROSS specific parsing routine for section offsets.
   FIXME:  This may or may not be necessary.  All the symbol readers seem
   to have similar code.  See if it can be generalized and moved elsewhere. */

static
struct section_offsets *
hicross_symfile_offsets (struct objfile *objfile, CORE_ADDR addr)
{
  struct section_offsets *section_offsets;
  int i;

  objfile->num_sections = SECT_OFF_MAX;
  section_offsets = (struct section_offsets *)
    obstack_alloc (&objfile -> psymbol_obstack,
		   sizeof (struct section_offsets) +
		   sizeof (section_offsets->offsets) * (SECT_OFF_MAX-1));

  for (i = 0; i < SECT_OFF_MAX; i++)
    {
      ANOFFSET (section_offsets, i) = addr;
    }
  
  return (section_offsets);
}


/* Register that we are able to handle HICROSS file format. */

static struct sym_fns hicross_sym_fns =
{
  bfd_target_hicross_flavour,
  hicross_new_init,		/* sym_new_init: init anything gbl to entire symtab */
  hicross_symfile_init,	/* sym_init: read initial info, setup for sym_read() */
  hicross_symfile_read,	/* sym_read: read a symbol file into symtab */
  hicross_symfile_finish,	/* sym_finish: finished with file, cleanup */
  hicross_symfile_offsets,	/* sym_offsets:  Translate ext. to int. relocation */
  NULL			/* next: pointer to next struct sym_fns */
};

void
_initialize_hiread ()
{
  add_symtab_fns (&hicross_sym_fns);
}
