/*	Copyright (C) 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */


/* Communication between GDB and STVisualDebug (PM) */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <windows.h>
#include "defs.h"
#include <setjmp.h>
#include "top.h"

/* External functions */
void InitSTVisualDebugCom();
void CleanSTVisualDebugCom();

#if 0
HANDLE  hGdiApiMutex = 0;
#endif

/* Internal functions and variables */
static BOOL writePipe(HANDLE  hPipe, char *szBuffer);
static DWORD threadWaitingForEvents(LPVOID lpParam);
static int initPipes();
static void installThreadWaitingForEvents();
static void testSTVisualDebugPipes();
static int trapSIGKILL();
static void pass_signal(int);

static HANDLE handleWorkingPipe=0;
static HANDLE handlePrintfPipe=0;
static HANDLE handleGetcharPipe=0;
static HANDLE hThreadSignal=0; 

/* ## Be careful, setting tests may trigger unstable crashes inside warning calls */
static int TestSTVisualDebugCom = 0;


/*
 * Initialize signals and pipes for communication between GDB and STVD
 */
void InitSTVisualDebugCom()  
{
  /*
   * Install the thread waiting for events sent by GUI
   */
  installThreadWaitingForEvents();

  /* Init "handleWorkingPipe", "handlePrintfPipe" and "handleGetcharPipe" */
  if (! initPipes()) {
      if (TestSTVisualDebugCom) {
        warning ("Error in initializing handleWorkingPipe, handlePrintfPipe and handleGetcharPipe");
	  }
  }

  if (TestSTVisualDebugCom) {
      testSTVisualDebugPipes();
  }
}

BOOL readPipe (HANDLE hPipe, char *pBuffer, DWORD numberOfBytesToRead) 
{
  DWORD numberOfBytesRead;
 
  if (!hPipe || !ReadFile (hPipe, pBuffer, numberOfBytesToRead, &numberOfBytesRead, NULL)) {
	return FALSE;
  } else {
	return TRUE;
  }
}

BOOL writePipe (HANDLE hPipe, char *pBuffer) 
{
  DWORD numberOfBytesWritten;
 
  if (!hPipe || !WriteFile (hPipe, pBuffer, strlen(pBuffer), &numberOfBytesWritten, NULL)) {
	return FALSE;
  } else {
	return TRUE;
  }
}

/*
 * Init "handleWorkingPipe", "handlePrintfPipe" and "handleGetcharPipe"
 * "handleWorkingPipe" is used to give information of progression
 * "handlePrintfPipe" allows to display output of "printf"
 * "handleGetcharPipe" allows to get input char for "getchar"
 */
int initPipes()
{
	char lpBuffer[255];
	DWORD reply;

	/*
	 * Get handleWorkingPipe:
	 */
	reply = GetEnvironmentVariable("handleWorkingPipe", lpBuffer, sizeof(lpBuffer));
	if (reply == 0) {
		if (TestSTVisualDebugCom) {
			warning("The environment variable handleWorkingPipe is not defined!\n");
		}
	} else {
		handleWorkingPipe = (HANDLE)(atoi((char*)lpBuffer));
	}

	/*
	 * Get handlePrintfPipe:
	 */
	reply = GetEnvironmentVariable("handlePrintfPipe", lpBuffer, sizeof(lpBuffer));
	if (reply == 0) {
		if (TestSTVisualDebugCom) {
			warning ("The environment variable handlePrintfPipe is not defined!\n");
		}
	} else {
		handlePrintfPipe = (HANDLE)(atoi((char*)lpBuffer));
	}

	/*
	 * Get handleGetcharPipe:
	 */
	reply = GetEnvironmentVariable("handleGetcharPipe", lpBuffer, sizeof(lpBuffer));
	if (reply == 0) {
		if (TestSTVisualDebugCom) {
			warning ("The environment variable handleGetcharPipe is not defined!\n");
		}
	} else {
		handleGetcharPipe = (HANDLE)(atoi((char*)lpBuffer));
	}

	return 1;
}


/* 
 * Testing pipes and events: 
 */
void testSTVisualDebugPipes()
{
  char lpBuffer[255];
  DWORD reply;

  /* Write in stdout: */
  fprintf(stdout, "Gdb: Stdout pipe is OK.\n");
  fflush(stdout);

  /* Write in stderr: */
  fprintf(stderr, "Gdb: Stderr pipe is OK.\n");
  fflush(stdout);

  /* Write in handlePrintfPipe: */
  if (handlePrintfPipe) {
	if (writePipe( handlePrintfPipe, "Gdb: handlePrintfPipe is OK.\n" ) == FALSE) {
		fprintf(stdout, "Gdb: Write in handlePrintfPipe failed.\n");
	} else {
		fprintf(stdout, "Gdb: Printf pipe is OK.\n");
	}
  }
  
  /* Read in handleGetcharPipe: */
  if (handleGetcharPipe) {
	char str[100];
	writePipe( handlePrintfPipe, "GDB7_GETCHAR_REQUEST\n" );
	if (readPipe( handleGetcharPipe, &str[0], 1 ) == FALSE) {
		fprintf(stdout, "Gdb: Read in handleGetcharPipe failed.\n");
	} else {
		fprintf(stdout, "Gdb: Getchar pipe is OK.\n");
	}
  }

  /* Write in handleWorkingPipe: */
  if (handleWorkingPipe ) {
	if (writePipe( handleWorkingPipe,"Gdb: handleWorkingPipe is OK.\n" ) == FALSE ) {
		fprintf(stdout, "Gdb: Write in handleWorkingPipe failed.\n");
	} else {
		fprintf(stdout, "Gdb: Working pipe is OK.\n");
	}
  }
}


/*
 * Install the thread waiting for events sent by GUI
 */
void installThreadWaitingForEvents( )
{

  DWORD   dwIdThreadSignal;

#if 0
  /* Create Mutex for gdi api access among threads */
  hGdiApiMutex = CreateMutex (NULL, FALSE, NULL);
#endif

#if 0
  printf_filtered("current thread identifier = %x\n", GetCurrentThreadId());
#endif

  /* Install the thread waiting for event: */
  hThreadSignal = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE) threadWaitingForEvents, NULL, 0, &dwIdThreadSignal);

  if (hThreadSignal == NULL) {
      if (TestSTVisualDebugCom) {
        warning("The thread for signals has not been created!\n");
	  }

  } else {
      /* 
       * Trap SIGKILL, or SIGTERM if SIGKILL is not defined.
       * => when one of these signals is trapped, call "quit_force"
       */
      trapSIGKILL();
  }
}


/*
 * Function to wait for events "STEventSIGINT" and "STEventSIGKILL"
 * and to signal these events internally
 */
DWORD threadWaitingForEvents (LPVOID lpParam)
{
  HANDLE hEventTable[2];
  DWORD dwReply;
  int i;
  char lpBuffer[255];
  DWORD reply1, reply2;

  /*
   * Get handle "hEventTable[0]" of Stop signal, from "STEventSIGINT" environment variable
   */
  reply1 = GetEnvironmentVariable( "STEventSIGINT", lpBuffer, sizeof(lpBuffer));
 if (reply1 == 0)
   {
     if (TestSTVisualDebugCom)
       warning("The environment variable STEventSIGINT is not defined!\n");
   }
 else
   {
     hEventTable [0] = OpenEvent (EVENT_ALL_ACCESS, FALSE, lpBuffer);
     if (TestSTVisualDebugCom)
       {    
		printf_filtered("Gdb. Environment variable STEventSIGINT: %s\n", lpBuffer);
		printf_filtered("Gdb. hEventTable [0]: %ld\n", hEventTable[0]);
       }
   }

 /*
  * Get handle "hEventTable[1]" of Quit signal, from "STEventSIGKILL" environment variable
  */
 reply2 = GetEnvironmentVariable("STEventSIGKILL", lpBuffer, sizeof(lpBuffer));
 if (reply2 == 0)
   {
     if (TestSTVisualDebugCom)
       warning("Gdb: The environment variable STEventSIGKILL is not defined!\n");
   }
 else
   {
     hEventTable[1] = OpenEvent (EVENT_ALL_ACCESS, FALSE, lpBuffer);
     if (TestSTVisualDebugCom)
       {   
		printf_filtered("Gdb. Environment variable STEventSIGKILL: %s\n", lpBuffer);
		printf_filtered("Gdb. hEventTable [1]: %ld\n", hEventTable[1]);
       }
   }

 if(!reply1 || !reply2)
   {
     if (TestSTVisualDebugCom)
       warning("The thread for signals is not created!\n");
     ExitThread(0);
     exit(0);
   }

 for (i=0; i<2;i++)
   {
     if (!hEventTable [i]) {
       if (TestSTVisualDebugCom)
         warning( "Failed to open event %d\n", i);
       ExitThread(0); 
       exit(0);
    }
   }

 /* Wait for events */
 while (1) 
   {
     dwReply = WaitForMultipleObjects (2, hEventTable, FALSE, INFINITE);
     switch (dwReply) 
       {
       case WAIT_OBJECT_0:
		/* STOP signal: */
		if (TestSTVisualDebugCom) {   
		  printf_filtered( "Gdb: Received STEventSIGINT signal = signal number %d!\n", hEventTable[dwReply]);
		  fflush(stdout);
		}
		/* Signal for SIGINT */
		raise (SIGINT);
		break;

       case WAIT_OBJECT_0 + 1:
		/* EXIT signal: */
		if (TestSTVisualDebugCom) {   
	      printf_filtered( "Gdb: Received STEventSIGKILL signal = signal number %d!\n", hEventTable[dwReply]);
	      fflush(stdout);
		}
#if defined(SIGKILL)
		/* (pm) rehosting win32 */
		/* Signal for SIGKILL */
		raise(SIGKILL);
#elif defined(SIGTERM)
		raise(SIGTERM);
#endif  
		ExitThread(0); 
		break;

       default:
		/* unexpected!! */
		if (TestSTVisualDebugCom)
		{  
	     printf_filtered("Gdb. Received unexpected signal! - last error = %d\n - hEventTable[dwReply]: %d", 
			     GetLastError(), hEventTable[dwReply]);
	     fflush(stdout);
		}
		break;
       }
   }
 
 return TRUE;
}


void CleanSTVisualDebugCom()
{
#if 0 /* This remove fixes MDTst04306 crash problem and is anyway unecessary */
  DWORD dwExitCode;

  if ( hThreadSignal!=0 &&
       ! TerminateThread(hThreadSignal,  // handle to the thread 
			 dwExitCode // exit code for the thread 
			 )
       )
  {
    if (TestSTVisualDebugCom)
      warning("The thread for signals has not terminated correctly!\n");
  }
#endif
}

/* 
 * Handle SIGKILL, or SIGTERM signals: when this signal occurs, the function 
 * "pass_signal" is called.
 */
int trapSIGKILL()
{
  int i;

#if defined (SIGKILL)
  /* (pm) rehosting win32 */
  /* Trap SIGKILL Signal */
  if (signal (SIGKILL, pass_signal) == SIG_ERR)
    {
      error("Can't trap SIGKILL signal\n");
      return 0;
    }
#else
#if defined (SIGTERM)
  /* Trap SIGTERM Signal */
  if (signal (SIGTERM, pass_signal) == SIG_ERR)
    {
      error("Can't trap SIGTERM signal\n");
      return 0;
    }
#endif
#endif
		
  return 1;
}


void pass_signal (int signo)
{
  if (TestSTVisualDebugCom)
    {  
      fprintf(stdout, "In pass_signal\n" );
      fflush(stdout);
    }

  switch (signo)
	{
	  /* case SIGINT: */
#if defined (SIGKILL)
	case SIGKILL:
	  if (TestSTVisualDebugCom)
	    { 
	      printf_filtered("SIGKILL is trapped\n");
	    }
	  quit_force((char *)0, 0);
	  break;
#else
#if defined (SIGTERM)
	  /* (pm) rehosting win32 */
	case SIGTERM:
	  if (TestSTVisualDebugCom)
	    { 
	      printf_filtered("SIGTERM is trapped\n");
	    }
	  quit_force((char *)0, 0);
	  break;
#endif
#endif
	}
}

static void write_working_pipe(char *message);

void begin_progress_bar(char *message)
{
  char *buffer;
      
  buffer = xmalloc((sizeof(message) + 100) * sizeof(char));
  sprintf(buffer, "Description=%s\n", message);
  write_working_pipe(buffer);
  free(buffer);
}

void advance_progress_bar(int progress_indicator)
{
  char buffer[100];
  
  sprintf(buffer, "Value=%d\n", progress_indicator);
  write_working_pipe(buffer);
  Sleep(0);
}

void end_progress_bar(void)
{
  write_working_pipe("Stop\n");
}

int debug_progress_bar_display = 0;

static void write_working_pipe(char *message)
{
  if (handleWorkingPipe)
    writePipe(handleWorkingPipe, message);
  if (debug_progress_bar_display)
    printf_filtered("%s", message);
}

/*
	get a char from the printf pipe
*/
void getchar_from_printf_pipe(char *cp)
{
  if (handleGetcharPipe) {
	char buffer[200];
	long bytesRead, totalBytes;
	PeekNamedPipe(handleGetcharPipe, &buffer[0], 200, &bytesRead, &totalBytes, NULL);
	if (bytesRead == 0 && handlePrintfPipe) {
		writePipe(handlePrintfPipe, "GDB7_GETCHAR_REQUEST\n");
	}
	readPipe(handleGetcharPipe, cp, 1);
  } else if (handlePrintfPipe) {
	  /* STVD without a getchar pipe */
	  error("getchar can't be used with this version of STVD");
  } else {
	gdb_flush(gdb_stdout);
	*cp = fgetc(stdin);
	clearerr(stdin);
  }
}

/*
	write in the printf pipe:
	1) add Sleep(1) otherwise STVD7 can't stop the program
	2) don't print at the same time in stdout and in the printf pipe
	otherwise there are problems in non blocking execution mode:
	the result of get_execution_status may be lost by STVD7
*/
void write_printf_pipe(char *message)
{
  if (handlePrintfPipe) {
	writePipe(handlePrintfPipe, message);
	Sleep(1);
  } else {
	/* Don't use printf_filtered: it consumes virtual memory! */
	printf("%s", message);
  }
}
