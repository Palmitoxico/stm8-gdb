/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* Definitions to make GDB run to ST7 executable
   running on an ST7 emulator.
   Copyright 1986, 1987, 1989, 1991, 1992, 1993 Free Software Foundation, Inc.
   Copyright 1994 C2V.
   
This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/*
---------------------------------------------------------------------
Created 18NOV94 (MC)
---------------------------------------------------------------------
*/
#define TARGET_STM7
#define TARGET_STM7P

#include <stdio.h>
#include <time.h>
#include "st7.h"

#if !defined (TARGET_BYTE_ORDER)
#define TARGET_BYTE_ORDER BIG_ENDIAN
#endif

#if !defined (BITS_BIG_ENDIAN)
#define BITS_BIG_ENDIAN 1
#endif

/* Number of bits in a char or unsigned char for the target machine.
   Just like CHAR_BIT in <limits.h> but describes the target machine.  */
#define TARGET_CHAR_BIT 8

/* Number of bits in a short or unsigned short for the target machine. */
#define TARGET_SHORT_BIT (2 * TARGET_CHAR_BIT)

/* Number of bits in an int or unsigned int for the target machine. */
#define TARGET_INT_BIT	16

#ifdef TARGET_STM7
#define TARGET_PTR_BIT	((mcu_core == ST7_CORE) ? 16 : 24)
#endif

/* Number of bits in a long or unsigned long for the target machine. */
#define TARGET_LONG_BIT (4 * TARGET_CHAR_BIT)

/* Number of bits in a long long or unsigned long long for the target machine. */
#define TARGET_LONG_LONG_BIT 64

/* Number of bits in a float for the target machine. */
#define TARGET_FLOAT_BIT 32

/* Number of bits in a double for the target machine. */
#define TARGET_DOUBLE_BIT TARGET_FLOAT_BIT

/* Number of bits in a long double for the target machine.  */
#define TARGET_LONG_DOUBLE_BIT TARGET_DOUBLE_BIT

/* Don't define CC_HAS_LONG_LONG but add new definition CC_HAS_INT64 */
/* MSVC 6.0 doesn't support long long but __int64 */
#define CC_HAS_INT64

#define SCALE_VALUE(val)	((val)*(TARGET_CHAR_BIT/HOST_CHAR_BIT))
#define SCALE_TYPE_LENGTH(t)	SCALE_VALUE(TYPE_LENGTH(t))

/* Offset from address of function to start of its code.
   Zero on most machines.  */

#define FUNCTION_START_OFFSET 0

/* Advance PC across any function entry prologue instructions
   to reach some "real" code.  */

#define SKIP_PROLOGUE(pc)	pc = st7_skip_prologue (pc)

/* Immediately after a function call, return the saved pc.
   Can't always go through the frames for this because on some machines
   the new frame is not set up until the new function executes
   some instructions.  */

#define SAVED_PC_AFTER_CALL(frame)	(st7_frame_saved_pc(frame))

/* Stack grows downward.
 */
#define INNER_THAN <

/* TRAP instruction used for ST7/STM7 microcontrollers with ICD features */
#define ST7_BREAKPOINT		0x83
/* Software breakpoint used for STM7P microcontrollers with ICD features */
#define STM7P_BREAKPOINT	0x8b

#define BREAKPOINT { ST7_BREAKPOINT }
void set_break_insn(unsigned char *break_insn);
#define SET_BREAK_INSN(BREAK_INSN) set_break_insn(BREAK_INSN)

#define BREAKPOINT_SIZE 1
unsigned int decr_pc_after_break(void);
/* Amount PC must be decremented by after a breakpoint.
   This is often the number of bytes in BREAKPOINT
   but not always.  */
#define DECR_PC_AFTER_BREAK (decr_pc_after_break())

/* This is taken care of in print_floating [IEEE_FLOAT].
   Return 1 if P points to an invalid floating point value.
   */
#define INVALID_FLOAT(p, len) 0   /* Just a first guess; not checked */

/* Say how long (ordinary) registers are.  This is a piece of bogosity
   used in push_word and a few other places; REGISTER_RAW_SIZE is the
   real way to know how big a register is.  */

#define REGISTER_SIZE 1

/* Number of machine registers (the "real" hard and some pseudo registers).
 */
#define NUM_REGS	0x1e

#define ADDITIONAL_REGISTER_NUMBER (6+8)

/* Initializer for an array of names of registers.
   There should be NUM_REGS strings in this initializer.
   */
#define REGISTER_NAMES  \
{"PC"/*0x0:"pc" */,\
"SP"/*0x1:"sp" */,\
"A"/*0x2:"A" */,\
"X"/*0x3:"X" */,\
"Y"/*0x4:"Y" */,\
"CC"/*0x5:"CC" */,\
/* pseudo registers used by the simulator */ \
"DATE"/*0x6:"DATE" */,\
"DATE_LIMIT"/*0x7:"DATE_LIMIT" */,\
"SP_OVERFLOW"/*0x8:"BASE_SP" */,\
"SP_UNDERFLOW"/*0x9:"TOP_SP" */,\
"NB_INST"/*0xa:"NB_INST" */,\
"NB_INST_MAX"/*0xb:"NB_INST_MAX" */,\
/* GDB7 internal pseudo register */ \
"FRAME_POINTER" /*0xc: "FRAME_POINTER" */,\
/* again pseudo registers used by the simulator */ \
"TIME"/*0xd:"TIME" */,\
"TIME_LIMIT"/*0xe:"TIME_LIMIT" */,\
"CPU_FREQUENCY"/*0xf:"CPU_FREQUENCY" */,\
"X:A"/*0x10:"X:A" */,\
"XH"/*0x11:"XH" */,\
"XL"/*0x12:"XL" */,\
"YH"/*0x13:"YH" */,\
"YL"/*0x14:"YL" */,\
"c_x:X"/*0x15:"c_x:X" */,\
"BX"/*0x16:"BX" */,\
"CX"/*0x17:"CX" */,\
"BH"/*0x18:"BH" */,\
"BL"/*0x19:"BL" */,\
"CH"/*0x1a:"CH" */,\
"CL"/*0x1b:"CL" */,\
"BX:CX"/*0x1c:"BX:CX" */,\
"BL:CX"/*0x1d:"BL:CX" */,\
NULL }

/* Register numbers of various important registers.
   Note that some of these values are "real" register numbers,
   and correspond to the general registers of the machine,
   and some are "phony" register numbers as far as the user is concerned
   but do serve to get the desired values when passed to read_register.  */


/* Program counter. */
#define PC_REGNUM	0
/* Contains low byte address of top of stack (ST7). */
/* Contains address of top of stack (STM7, STM8). */
#define SP_REGNUM	1   
#define A_REGNUM	2
#define X_REGNUM	3
#define Y_REGNUM	4	
#define CC_REGNUM	5

/* Now pseudo registers */
/* Some are used by the simulator */
/* DATE --> for the simulator, CPU cycle counter */
#define DATE_REGNUM 6
/* DATE_LIMIT --> for the simulator, maximum number of CPU cycles after which the debugger stops */
#define DATE_LIMIT_REGNUM 7
/* SP_OVERFLOW --> minimum value of stack pointer */
#define SP_OVERFLOW_REGNUM 8
/* SP_UNDERFLOW --> maximum value of stack pointer */
#define SP_UNDERFLOW_REGNUM 9
/* NB_INST --> for the simulator, number of executed CPU instructions */
#define NB_INST_REGNUM 10
/* NB_INST_MAX --> for the simulator, maximum number of executed CPU instructions after which the debugger stops */
#define NB_INST_MAX_REGNUM 11

/* FRAME_POINTER --> reconstituted frame pointer */
/* The ST7 has no frame pointer register. */
/* This pseudo register is used to take into account debug information
	for local variables and parameters in stack. */
#define FRAME_POINTER_REGNUM 12
/* TIME --> for the simulator, elapsed time expressed in nanoseconds */
#define TIME_REGNUM			 13
/* TIME_LIMIT --> for the simulator, maximum time after which the debugger stops */
#define TIME_LIMIT_REGNUM	 14
/* CPU_FREQUENCY --> for the simulator, CPU frequency expressed in kHz */
#define CPU_FREQUENCY_REGNUM 15

/* X:A --> for STM7, 2-byte register, high byte in X, low byte in A */
/* This pseudo register is used to avoid to support DW_OP_piece
	which enables to describe that a C object is located in two registers. */
#define X_A_REGNUM 16

/* XH --> for STM8, X high byte */
#define XH_REGNUM 17
/* XL --> for STM8, X low byte */
#define XL_REGNUM 18
/* YH --> for STM8, Y high byte */
#define YH_REGNUM 19
/* YL --> for STM8, Y low byte */
#define YL_REGNUM 20
/* c_x:X --> for STM8, 3-byte pseudo-register, high byte in memory at c_x, low bytes in X */
/* A Cosmic far pointer argument is passed in c_x:X */
/* This pseudo register is used to avoid to support DW_OP_piece
	which enables to describe that a C object is located in two places. */
#define cx_X_REGNUM 21

/* 8 pseudo-registers added for the Raisonance compiler */
#define BX_REGNUM 22
#define CX_REGNUM 23
#define BH_REGNUM 24
#define BL_REGNUM 25
#define CH_REGNUM 26
#define CL_REGNUM 27
#define BX_CX_REGNUM 28
#define BL_CX_REGNUM 29

/* Needs to be defined but there is no frame pointer register for the ST7 */
#define FP_REGNUM	SP_REGNUM  

/* Total amount of space needed to store our copies of the machine's
   register state, the array `registers'.  */
#if defined(TARGET_STM7P)
#define REGISTER_BYTES (0x46+2+8+16)
#elif defined(TARGET_STM7)
#define REGISTER_BYTES 0x44
#else
#define REGISTER_BYTES 0x42
#endif

/* Index within `registers' of the first byte of the space for
   register N.  */

extern int st7_global_reg_byte_pos [NUM_REGS];
#define REGISTER_BYTE(N)  (st7_global_reg_byte_pos[(N)])

/* Number of bytes of storage in the actual machine representation
   for register N.
   */

extern int st7_global_reg_size [NUM_REGS];
#define REGISTER_RAW_SIZE(N) ((st7_global_reg_size[(N)]+7)/8)

/* Number of bytes of storage in the program's representation
   for register N.
   */

#define REGISTER_VIRTUAL_SIZE(N) REGISTER_RAW_SIZE(N) 

/* Largest value REGISTER_RAW_SIZE can have.
   */
#define MAX_REGISTER_RAW_SIZE	0x8

/* Largest value REGISTER_VIRTUAL_SIZE can have.
 */

#define MAX_REGISTER_VIRTUAL_SIZE MAX_REGISTER_RAW_SIZE

/* Return the GDB type object for the "standard" data type
   of data in register N.
   */
#if defined(TARGET_STM7P)
#define REGISTER_VIRTUAL_TYPE(N) \
  ((N == BX_REGNUM || N == CX_REGNUM || N == X_A_REGNUM || N == X_REGNUM || N == Y_REGNUM \
    || N == SP_REGNUM || N == SP_OVERFLOW_REGNUM || N == SP_UNDERFLOW_REGNUM || N == FRAME_POINTER_REGNUM) ?\
   builtin_type_unsigned_short :\
    ((N == BX_CX_REGNUM || N == BL_CX_REGNUM || N == cx_X_REGNUM || N == PC_REGNUM || N == CPU_FREQUENCY_REGNUM) ?\
     builtin_type_unsigned_long :\
      ((N == DATE_REGNUM || N == DATE_LIMIT_REGNUM\
	    || N == NB_INST_REGNUM || N == NB_INST_MAX_REGNUM\
		|| N == TIME_REGNUM || N == TIME_LIMIT_REGNUM) ?\
       builtin_type_unsigned_long_long : builtin_type_unsigned_char)))
#elif defined(TARGET_STM7)
#define REGISTER_VIRTUAL_TYPE(N) \
  ((N == SP_REGNUM || N == SP_OVERFLOW_REGNUM || N == SP_UNDERFLOW_REGNUM || N == FRAME_POINTER_REGNUM) ?\
   builtin_type_unsigned_short :\
    ((N == PC_REGNUM || N == CPU_FREQUENCY_REGNUM) ?\
     builtin_type_unsigned_long :\
      ((N == DATE_REGNUM || N == DATE_LIMIT_REGNUM\
	    || N == NB_INST_REGNUM || N == NB_INST_MAX_REGNUM\
		|| N == TIME_REGNUM || N == TIME_LIMIT_REGNUM) ?\
       builtin_type_unsigned_long_long : builtin_type_unsigned_char)))
#else
#define REGISTER_VIRTUAL_TYPE(N) \
  ((N == PC_REGNUM || N == SP_REGNUM || N == SP_OVERFLOW_REGNUM || N == SP_UNDERFLOW_REGNUM || N == FRAME_POINTER_REGNUM) ?\
   builtin_type_unsigned_short :\
    ((N == CPU_FREQUENCY_REGNUM) ?\
     builtin_type_unsigned_long :\
     ((N == DATE_REGNUM || N == DATE_LIMIT_REGNUM\
	   || N == NB_INST_REGNUM || N == NB_INST_MAX_REGNUM\
	   || N == TIME_REGNUM || N == TIME_LIMIT_REGNUM) ?\
       builtin_type_unsigned_long_long : builtin_type_unsigned_char)))
#endif

/* CCR IEA bit */
#define CCR_IEA	6
/* CCR I1 bit */
#define CCR_I1	5
/* CCR I0 bit */
#define CCR_I0	3

/****************************************************************/

/* Extract from an array REGBUF containing the (raw) register state
   a function return value of type TYPE, and copy that, in virtual format,
   into VALBUF.
   */
void extract_return_value(struct type *type, char *regbuf, char *valbuf);
#define EXTRACT_RETURN_VALUE(TYPE,REGBUF,VALBUF) \
	extract_return_value((TYPE), (REGBUF), (VALBUF));

/* Write into appropriate registers a function return value
   of type TYPE, given in virtual format.
   */
void store_return_value(struct type *type, char *valbuf);
#define STORE_RETURN_VALUE(TYPE,VALBUF) \
	store_return_value((TYPE), (VALBUF));

/* Describe the pointer in each stack frame to the previous stack frame
   (its caller).  */

/* FRAME_CHAIN takes a frame's nominal address
   and produces the frame's chain-pointer. */

#define FRAME_CHAIN(thisframe) ((CORE_ADDR)st7_frame_chain(thisframe))

/* Saved Pc.
 */
#define FRAME_SAVED_PC(FRAME)	(st7_frame_saved_pc(FRAME))

/* If the argument is on the stack, it will be here.
 */
#define FRAME_ARGS_ADDRESS(fi)	(st7_frame_args_address(fi))

/* Address of the local variables for this frame.
 */
#define FRAME_LOCALS_ADDRESS(fi) (fi)->frame

/* Return number of args passed to a frame.
   Can return -1, meaning no way to tell.
   */
#define FRAME_NUM_ARGS(val,fi) (val = -1)

/* Return number of bytes at start of arglist that are not really args.
   */
#define FRAME_ARGS_SKIP 0

/* Put here the code to store, into a struct frame_saved_regs, the
   addresses of the saved registers of frame described by FRAME_INFO.
   This includes special registers such as pc and fp saved in special
   ways in the stack frame.  sp is even more special:
   the address we return for it IS the sp for the next frame.
   The actual code is in st7tdep.c so we can debug it sanely.
   */
#define FRAME_FIND_SAVED_REGS(fi,frame_saved_regs)    	    \
	st7_frame_find_saved_regs ((fi), &(frame_saved_regs))

#define POP_FRAME		/* NOT USED FOR HI-CROSS ST7 */

/* Indicates the stop condition of frame unrolling algorithm */
#define FRAME_CHAIN_VALID(val,fi) (st7_frame_chain_valid (val, fi))

/* redefine exit to call gdb7_exit which will close gdb properly */
#define exit gdb7_exit

/* Call dummy NOT USED FOR HI-CROSS ST7 */
#define PC_IN_CALL_DUMMY(pc, sp, frame_address) (0)

/* The ST7 execution targets support hardware watchpoints */
 
#define TARGET_HAS_HARDWARE_WATCHPOINTS
 
#define TARGET_CAN_USE_HARDWARE_WATCHPOINT(type, cnt, ot) 1

int target_region_ok_for_watchpoint PARAMS ((CORE_ADDR addr, int len));

#define TARGET_REGION_OK_FOR_HW_WATCHPOINT(vaddr, len) \
	target_region_ok_for_watchpoint(vaddr, len)

/* Use these macros for watchpoint insertion/removal.  */
int st7_insert_watchpoint PARAMS ((CORE_ADDR addr, int len, int read_write, char *shadow_contents));
int st7_remove_watchpoint PARAMS ((CORE_ADDR addr, int len, int read_write, char *shadow_contents));
 
#define target_insert_watchpoint(addr, len, type, shadow_contents)  \
  st7_insert_watchpoint (addr, len, type, shadow_contents)
 
#define target_remove_watchpoint(addr, len, type, shadow_contents)  \
  st7_remove_watchpoint (addr, len, type, shadow_contents)

enum
{
  class_gdi = class_pseudo+1, /* class_pseudo defined in defs.h */
                              /* special command class for gdi interface */
  class_st7 = class_pseudo+2, /* class_pseudo defined in defs.h */
                              /* special command class for ST7 */
};

enum di_stop
{ di_reset,
  di_running,
  di_single_step,
  di_stopped_by_code_breakpoint,
  di_stopped_by_data_breakpoint,
  di_stopped_by_user,
  di_stopped_by_di_specific_feature_with_error,
  di_stopped_by_di_specific_feature_with_warning
};
extern enum di_stop stop_reason_code;
enum data_access
{
  read_write_access,
  read_access,
  write_access,
  undefined_access
};
extern enum data_access stopped_data_access;
extern CORE_ADDR stopped_data_address;
extern char *stop_reason_message;

void di_print_stop_message(void);

CORE_ADDR st7_stopped_data_address(void);
#define target_stopped_data_address() st7_stopped_data_address()
 
/* (rj) read/write on the fly. */
extern int application_is_running;
extern int application_stopped_by_breakpoint;
extern char ST7_running_mode[];
extern int read_write_on_the_fly_mode;
extern int proceed_counter;

void reset_execution_flags(void);
void reset_execution_flags_if_error_after_stop(void);

#define STVD_ERROR_PREFIX "\nError: "

int dwarf2_find_saved_regs (struct frame_info *frame, int where[NUM_REGS]);

#define ADDITIONAL_OPTIONS \
/* Line number separator for commands: list, break, goto, until: */ \
{"linenumber_separator", required_argument, 0, 14},

#define ADDITIONAL_OPTION_HELP \
"\
  --linenumber_separator=SEPARATOR\n\
                     Change current line number separator to SEPARATOR\n\
                     for \"list\", \"break\", \"goto\", \"until\" commands.\n"

extern char file_separator;

#define ADDITIONAL_OPTION_CASES \
	  case 14: \
            if (optarg!=NULL) \
               file_separator = *optarg; \
	    break;

int pathname_eq_no_case(const char *s1, const char *s2);

#define PATHNAME_EQ_NO_CASE(a,b) (pathname_eq_no_case ((a), (b)))

int library_routine_alters_stack_pointer(char *function_name);

void handle_library_routine_altering_return_pc(char *function_name, CORE_ADDR *return_pc);

/* Get processor time expressed in clock ticks. */
clock_t get_clock_ticks_of_processor_time();

/* Compute the duration in seconds from two times expressed in clock ticks. */
double diff_time_clock_ticks(clock_t ct1, clock_t ct0);

extern unsigned int dwarf_loc_size;
extern unsigned int dwarf_frame_size;

int get_disassembled_instruction (bfd_vma memaddr,
                                  struct disassemble_info *info,
	                              char *disass_instr_buffer);

int
get_code_and_disassembled_instruction (bfd_vma memaddr,
										struct disassemble_info *info,
										char *hexa_instr_buffer,
										char *disass_instr_buffer);

int st7_disass
(unsigned long instr_addr, unsigned char *instr_buffer, char *disass_instr_buffer, int symbolic_info);

enum function_call_type
{
	UNKNOWN_CALL_TYPE,
	ERROR_CALL_TYPE,
	NO_CALL,
	NEAR_CALL,
	FAR_CALL,
	INTERRUPT_HANDLER
};

/* function_addr is not used */
#define EXTRA_FRAME_INFO \
    int init_saved_regs; \
    CORE_ADDR saved_regs[NUM_REGS]; \
	int init_sp_after_call; \
	CORE_ADDR sp_after_call; \
	int init_call_type; \
	enum function_call_type call_type; \
	CORE_ADDR function_addr; \
	int interrupt_handler;

void init_extra_frame_info(struct frame_info *fi);
#define INIT_EXTRA_FRAME_INFO(current_frame, fi)	init_extra_frame_info(fi);

int single_step_execution(void);

#define UNKNOWN_VALUE (-1 << 31)

int is_an_interrupt_handler(CORE_ADDR pc);
#define IN_INTERRUPT_HANDLER(pc, name)	is_an_interrupt_handler(pc)

extern int display_pointer_qualifier;

#define MEMORY_SIZE ((mcu_core == ST7_CORE) ? 0x10000 : 0x1000000)

/* Begin ICD */
#define EXTERNAL_HARDWARE_BREAKPOINTS_DISABLED 0x2
void disable_all_hardware_breakpoints_if_necessary(int hardware_breakpoints_needed,
														  int *breakpoints_disabled);
void disable_hardware_breakpoints_if_necessary(int hardware_breakpoints_needed,
													  int *breakpoints_disabled,
													  int only_inserted_breakpoints);
void restore_hardware_breakpoints(int breakpoints_disabled);

int hardware_breakpoint_available(CORE_ADDR address);
int is_disassemble_possible(CORE_ADDR address);
int is_memory_writable(CORE_ADDR address);

char *instruction_backup(char *shadow_contents);
void initialize_inserted_breakpoints(char *shadow_contents);
void add_inserted_breakpoint(char *shadow_contents, unsigned long bp_id);
int get_inserted_breakpoint_id(char *shadow_contents, unsigned long *bp_id);
void remove_inserted_breakpoint(char *shadow_contents);
void set_hardware_breakpoint_implementation_method(char *shadow_contents);
void set_software_breakpoint_implementation_method(char *shadow_contents);
int software_breakpoint_implementation_method(char *shadow_contents);

int debug_instrument_supports_single_step(void);
int is_software_breakpoint(struct breakpoint *b);
int debug_instrument_uses_purely_software_breakpoints(void);
int debug_instrument_uses_gdb_software_breakpoints(void);
int debug_instrument_has_a_limited_number_of_hardware_breakpoints(void);
int debug_instrument_has_an_illimitable_number_of_hardware_breakpoints(void);
int debug_instrument_supports_restart_from_breakpoint(void);
int is_software_breakpoint_possible(CORE_ADDR address);

void di_memory_download_initialize(void);
void di_memory_download_terminate(void);
extern int break_disabled_message_counter;

extern int stopped_by_software_breakpoint;
/* End ICD */

#define CRASH_BARRIER_EXECUTABLE_FILE() \
	(symfile_objfile \
	 && bfd_get_flavour(symfile_objfile->obfd) == bfd_target_cbmap_flavour)

#define ELF_EXECUTABLE_FILE() \
	(symfile_objfile \
	 && bfd_get_flavour(symfile_objfile->obfd) == bfd_target_elf_flavour)

extern unsigned int debug_instrument_opened;
void st7_target_error(void);
void stm7_target_error(void);
void stm7p_target_error(void);
void check_compiler_target_compatibility_with_debug_target(void);

int disassemble_instruction(bfd_vma memaddr, char *disass_instr_buffer, int memory_check);
int is_a_STM7_far_interrupt(void);
#define NO_SINGLE_STEP
extern int SoftwareSingleStep;
enum stepping_mode_enum {
	hardware_stepping_mode,
	software_stepping_mode
} stepping_mode;
extern CORE_ADDR MCU_reset_address;
#define DEFAULT_ST7_RESET_VECTOR 0xfffe
extern CORE_ADDR MCU_reset_vector;

CORE_ADDR get_sp_after_call(struct frame_info *frame);
void get_instruction_mnemonic(CORE_ADDR stop_pc, char *mnemonic, char *first_operand);
int is_a_call_instruction(CORE_ADDR stop_pc, CORE_ADDR *next_pc);
int is_Cosmic_STM7_far_function_pointer_call(char *function_name);
CORE_ADDR get_Cosmic_STM7_far_function_address(char *stub_function_name);

extern int pc_decremented_in_get_frame_block;
int is_a_function_call_stack_offset(CORE_ADDR stack_offset);
int is_an_interrupt_handler_stack_offset(CORE_ADDR stack_offset);

void set_compiler_vendor(char *producer);
int is_cosmic_compiler_used(struct objfile *symfile_objfile);
int is_raisonance_compiler_used(struct objfile *symfile_objfile);

int detect_user_function(CORE_ADDR non_user_func_addr, enum function_call_type call_type, CORE_ADDR *user_func_addr);

void enable_memory_cache(int size);
void disable_memory_cache(void);

int get_symbol_address (const char *sym_name, CORE_ADDR *addr_p);

extern int guess_call_type;
extern int check_disassembly;

struct minimal_symbol *lookup_minimal_symbol_by_address (CORE_ADDR pc);

extern int gdi_spy;
extern FILE *gdi_spy_stream;
