/*	Copyright (C) 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

struct argstruct {
  unsigned char *arglist;		/* pointer beginning of new arg list */
  unsigned char *arglist_pt;	/* current pointer in arg list */
  int arglist_length;			/* current arg list length */
  int arglist_max_length;		/* allocated size of arg list */
};

int parse_argument PARAMS ((char *arg, struct argstruct *argl, int (*is_separator)(int)));
char *next_arg PARAMS ((struct argstruct *argl));
