/*	Copyright (C) 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

#include <windows.h>
#include <ctype.h>
#include <string.h>
#include "defs.h"
#include "breakpoint.h"
#include "command.h"
#include "target.h"
#include "frame.h"
#include "inferior.h"
#include "st7-utils.h"

#define DEFAULT_ARGLIST_LENGTH	80	/* default size of argument file */

static void start_arg_list PARAMS ((struct argstruct *argl))
{
  argl->arglist_max_length = DEFAULT_ARGLIST_LENGTH;
  argl->arglist = (unsigned char *) xmalloc(argl->arglist_max_length);
  argl->arglist_pt = argl->arglist;
  argl->arglist_length = 0;
}

static void add_arg_list PARAMS ((int c, struct argstruct *argl))
{
  if (argl->arglist_length == argl->arglist_max_length)
    {
      argl->arglist_max_length *= 2;
      argl->arglist = (unsigned char *) xrealloc (argl->arglist, 
												  argl->arglist_max_length);
      argl->arglist_pt = argl->arglist + argl->arglist_length;
    }

  *((argl->arglist_pt)++) = (unsigned char) c;
  argl->arglist_length++;
}

int
parse_argument PARAMS ((char *arg, struct argstruct *argl, int (*is_separator)(int)))
{
  int argument_started;			/* true when in the middle of an argument */
  int argument_num;			/* number of new arguments */
  int c;

  start_arg_list (argl);		/* init argument list */

  argument_num = 0;
  argument_started = 0;

  if (arg)
    do
      {
		c = *arg++;
	
		if (c == 0 || is_separator (c))
		{
		  if (argument_started)
			{
			  add_arg_list (0, argl);	/* end this arg by zero */
			  argument_started = 0;
			  argument_num++;
			}
		}
		else
		{
		  argument_started = 1;
		  add_arg_list (c, argl);
		}
      }
    while (c != 0);

  argl->arglist_pt = argl->arglist;	/* be ready for next_arg() */
  return argument_num;			/* return number of arguments */
}

char *next_arg PARAMS ((struct argstruct *argl))
{
  char *argument;

  argument = argl->arglist_pt;
  while (*((argl->arglist_pt)++));	/* points to next argument for next call */
  return argument;
}


extern struct breakpoint *breakpoint_chain;

struct breakpoint_list_type
{
  struct breakpoint *b;
  struct breakpoint_list_type *next;
}; 

static struct breakpoint_list_type *active_breakpoint_list = NULL;

static void
add_breakpoint(struct breakpoint *b, struct breakpoint_list_type **bp_list_tail)
{
	if (*bp_list_tail == NULL)
		*bp_list_tail = (struct breakpoint_list_type *) xmalloc(sizeof(struct breakpoint_list_type));
	else
    {
		(*bp_list_tail)->next
			= (struct breakpoint_list_type *) xmalloc(sizeof(struct breakpoint_list_type));
		*bp_list_tail = (*bp_list_tail)->next;
    }
	(*bp_list_tail)->b = b;
	(*bp_list_tail)->next = NULL;
}

static void free_active_breakpoint_list(void);

#define ALL_BREAKPOINTS 0
#define ONLY_HARDWARE_BREAKPOINTS 1
static void save_enabled_breakpoints(int only_hardware_breakpoints)
{
	struct breakpoint *b;
	struct breakpoint_list_type *bp_list_tail;

	/* It should already have been done.
	   But if there is an error and we jump to the error handler,
	   this list might not have been released. */
	free_active_breakpoint_list();
	
	for (b = breakpoint_chain; b; b = b->next)
    {
		if (b->enable == enabled
			&& (only_hardware_breakpoints ? is_hardware_breakpoint(b) : 1))
		{
			if (active_breakpoint_list == NULL)
			{
				add_breakpoint(b, &active_breakpoint_list);
				bp_list_tail = active_breakpoint_list;
			}
			else
				add_breakpoint(b, &bp_list_tail);
		}
    }
}

static int disable_saved_breakpoints()
{
  struct breakpoint_list_type *bp_list = active_breakpoint_list;
  struct breakpoint *b;
  int val;
  
  while (bp_list != NULL)
    {
	  b = bp_list->b;
	  if (b->inserted)
		{
		  val = remove_breakpoint (b);
		  if (val != 0)
			return val;
		}
      disable_breakpoint(b);
      bp_list = bp_list->next;
    }
  return 0;
}

static void restore_enabled_breakpoints(void)
{
  struct breakpoint_list_type *bp_list = active_breakpoint_list;
  
  while (bp_list != NULL)
    {
      enable_breakpoint(bp_list->b);
      bp_list = bp_list->next;
    }
}

static void free_active_breakpoint_list(void)
{
  struct breakpoint_list_type *bp_list_header;
  
  while (active_breakpoint_list != NULL)
    {
      bp_list_header = active_breakpoint_list;
      active_breakpoint_list = active_breakpoint_list->next;
      free(bp_list_header);
    }
}

static void restore_breakpoints_and_clean_up(void)
{
  restore_enabled_breakpoints();
  free_active_breakpoint_list();
}

extern void continue_command(char *, int);
extern void run_command(char *, int);

#define RUN_COMMAND 0
#define CONTINUE_COMMAND 1

static void
perf_command(char *args, int from_tty, int proceed_command);

static void
perf_run_command(char *args, int from_tty)
{
  perf_command(args, from_tty, RUN_COMMAND);
}

static void
perf_continue_command(char *args, int from_tty)
{
  perf_command(args, from_tty, CONTINUE_COMMAND);
}

extern void gdi_command PARAMS ((char *args));

static void
perf_command(char *args, int from_tty, int proceed_command)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  int nobreak = 0;
  char command_name[30];
  char direct_command[100];
  char option[10];

  if (proceed_command == RUN_COMMAND)
	strcpy(command_name, "perf_run");
  else
	strcpy(command_name, "perf_continue");
  
  /* read/write on the fly. */
  if (application_is_running)
	error("Can't execute %s: application is already running.", command_name);

  if (strcmp(target_shortname, "gdi") != 0)
    error("%s not available for current target: %s.", command_name, target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number > 1)
    error("%s can't have more than one argument.", command_name);

  if (arg_number != 0)
    {
      szarg = next_arg (&arg_list); /* get next argument */
      if (strcmp(szarg, "-nobreak") == 0)
		nobreak = 1;
      else
		error("%s command: unknown option \"%s\".", command_name, szarg);
    }

  if (nobreak)
    strcpy(option, "nobreak");
  else
    strcpy(option, "break");
  sprintf(direct_command, "perf before %s\n", option);
  gdi_command((char *)direct_command);

  if (nobreak)
    {
      save_enabled_breakpoints(ALL_BREAKPOINTS);
      disable_saved_breakpoints();
	  make_cleanup((void (*) (void *))restore_breakpoints_and_clean_up, (char *)0);
    }

  if (proceed_command == RUN_COMMAND)
    run_command(NULL, 0);
  else
    continue_command(NULL, 0);

  enable_command_cancel();
  enable_progress_bar_display();

  sprintf(direct_command, "perf after\n");
  gdi_command((char *)direct_command);

  do_cleanups(old_cleanup_chain);
}

static void proceed_no_wait(char *args, int from_tty, int proceed_command);

/* (rj) read/write on the fly. */
static void
run_no_wait_command(char *args, int from_tty)
{
  proceed_no_wait(args, from_tty, RUN_COMMAND);
}

/* (rj) read/write on the fly. */
static void
continue_no_wait_command(char *args, int from_tty)
{
  proceed_no_wait(args, from_tty, CONTINUE_COMMAND);
}

int read_write_on_the_fly_mode = 0;

static void check_command_call_validity(char *args, char *command_name)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;

  if (strcmp(target_shortname, "gdi") != 0)
    error("%s not available for current target: %s.", command_name, target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number != 0)
    error("%s doesn't have any argument.", command_name);

  do_cleanups(old_cleanup_chain);
}

/* (rj) read/write on the fly. */
static void proceed_no_wait(char *args, int from_tty, int proceed_command)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  char command_name[30];

  if (proceed_command == RUN_COMMAND)
	strcpy(command_name, "run_no_wait");
  else
	strcpy(command_name, "continue_no_wait");

  if (strcmp(target_shortname, "gdi") != 0)
    error("%s not available for current target: %s.", command_name, target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number != 0)
    error("%s doesn't have any argument.", command_name);

  do_cleanups(old_cleanup_chain);

  read_write_on_the_fly_mode = 1;
  
  /* The various caches must be invalidated: they are already validated in "wait_for_inferior".
	 The register cache must be invalidated.
	 registers_changed();
	 The frame cache must be invalidated.
	 flush_cached_frames();
  */

  if (proceed_command == RUN_COMMAND)
	run_command(NULL, from_tty);
  else
    continue_command(NULL, from_tty);
}

extern void set_execution_stop_flag(void);
static void get_execution_status(void);

/* (rj) read/write on the fly. */
static void
stop_no_wait_command(char *args, int from_tty)
{
  check_command_call_validity(args, "stop_no_wait");
  if (proceed_counter != 0)
	{
	  set_execution_stop_flag();
	  
	  /* Restore normal behaviour of "wait_for_inferior" so as to be sure
		 that "target_wait" is called. */
	  application_stopped_by_breakpoint = 0;

	  get_execution_status();
	}
  else
	error("Application is already stopped.");
}

/* (rj) read/write on the fly. */
static void get_execution_status_command(char *args, int from_tty)
{
  check_command_call_validity(args, "get_execution_status");
  get_execution_status();
}

/* (rj) read/write on the fly. */
static void get_execution_status(void)
{
  /* If we have already stopped, just display the stop reason. */
  if (read_write_on_the_fly_mode)
	wait_for_inferior ();
  normal_stop ();
}

static void
set_standalone_mode_command(char *args, int from_tty)
{
  char direct_command[20];

  check_command_call_validity(args, "set_standalone_mode");
  strcpy(direct_command, "standalone set");
  gdi_command((char *)direct_command);
  target_detach (NULL, 1);
}


extern int default_breakpoint_valid; /* in breakpoint.c */
extern struct symtab *default_breakpoint_symtab;
extern int default_breakpoint_line;

extern void breakpoint_1 (int bnum, int allflag);

/*
  GOTO function.
  
  Continue until specified address (or line) is reached, or
  program is stopped for other reasons.
  
  Similar to until command, except that 'until' will stop at the
  line or address specified ONLY when it belongs to the CURRENT
  function (in fact, only when the stack frame equals the stack
  frame at the time we issued the until command).
  Also until will break at the return address of current function,
  this we do not want.
  
  See until_break_command in breakpoint.c
*/

static void
goto_command (char *arg, int from_tty)
{
  struct symtabs_and_lines sals;
  struct symtab_and_line sal;
  struct breakpoint *breakpoint;
  struct cleanup *delete_momentary_breakpoint_cleanup = 0;
  /* Begin ICD */
  int breakpoints_disabled = 0;
  struct cleanup *restore_breakpoints_cleanup = 0;
  /* End ICD */

  if (!target_has_execution)
    error ("The program is not running.");

  if (arg == 0)
    error_no_arg ("line, address or function");

  clear_proceed_status ();

  /* Set a breakpoint where the user wants it and at return from
     this function */
  
  if (default_breakpoint_valid)
    sals = decode_line_1 (&arg, 1, default_breakpoint_symtab,
			  default_breakpoint_line, (char ***)NULL);
  else
    sals = decode_line_1 (&arg, 1, (struct symtab *)NULL, 0, (char ***)NULL);
  
  if (sals.nelts != 1)
    error ("Couldn't get information on specified line.");
  
  sal = sals.sals[0];
  free ((PTR)sals.sals);		/* malloc'd, so freed */
  
  if (*arg)
    error ("Junk at end of arguments.");
  
  resolve_sal_pc (&sal);

  if (sal.pc == read_pc())
    error("We are already there.");

  breakpoint = set_momentary_breakpoint (sal, (struct frame_info *) 0, bp_until, &breakpoints_disabled);
  if (breakpoints_disabled)
	restore_breakpoints_cleanup
	  = make_cleanup((void (*) (char *))restore_hardware_breakpoints,
						(char *)breakpoints_disabled);

  if (breakpoint)
    delete_momentary_breakpoint_cleanup
	  = make_cleanup((void (*) (char *))delete_breakpoint, (char *)breakpoint);
  
  proceed (-1, TARGET_SIGNAL_DEFAULT, 0);

  if (delete_momentary_breakpoint_cleanup)
    do_cleanups(delete_momentary_breakpoint_cleanup);
  /* Begin ICD */
  if (restore_breakpoints_cleanup)
	do_cleanups(restore_breakpoints_cleanup);
  /* End ICD */
}


enum breakpoint_implementation_method_enum
{
  hardware_breakpoint,
  software_breakpoint
};

/* The "shadow_contents" field of the "breakpoint" structure is cast
   as "struct breakpoint_extension" and is used as such.
   This works because "shadow_contents" is declared as an array of 16 characters
   and the size of "breakpoint_extension" is only 13. */

struct inserted_breakpoint_list
{
	unsigned long number;
	struct inserted_breakpoint_list *next;
};
typedef struct inserted_breakpoint_list bp_list_type;
 
struct breakpoint_extension
{
  enum breakpoint_implementation_method_enum breakpoint_implementation_method;
  char instruction_backup[BREAKPOINT_SIZE];
  unsigned long number_of_inserted_breakpoints;
  bp_list_type *inserted_breakpoints;
};

typedef struct breakpoint_extension * bp_extension_ptr; 

char *instruction_backup(char *shadow_contents)
{
  return ((bp_extension_ptr)shadow_contents)->instruction_backup;
}

void initialize_inserted_breakpoints(char *shadow_contents)
{
  ((bp_extension_ptr)shadow_contents)->number_of_inserted_breakpoints = 0;
  ((bp_extension_ptr)shadow_contents)->inserted_breakpoints
	  = (struct inserted_breakpoint_list *)0;
}

void
add_inserted_breakpoint(char *shadow_contents, unsigned long bp_id)
{
  struct inserted_breakpoint_list *inserted_breakpoint;
  unsigned long *bp_num_ptr
	  = &(((bp_extension_ptr)shadow_contents)->number_of_inserted_breakpoints); 

  if (((bp_extension_ptr)shadow_contents)->number_of_inserted_breakpoints == 0)
	{
	  ((bp_extension_ptr)shadow_contents)->inserted_breakpoints
		= (bp_list_type *)xmalloc(sizeof (bp_list_type));
	  inserted_breakpoint = ((bp_extension_ptr)shadow_contents)->inserted_breakpoints;
	}
  else
	{
	  inserted_breakpoint = ((bp_extension_ptr)shadow_contents)->inserted_breakpoints;
	  while (inserted_breakpoint->next != NULL)
		inserted_breakpoint = inserted_breakpoint->next;
	  inserted_breakpoint->next = (bp_list_type *)xmalloc(sizeof (bp_list_type));
	  inserted_breakpoint = inserted_breakpoint->next;
	}
  inserted_breakpoint->number = bp_id;
  inserted_breakpoint->next = NULL;
  *bp_num_ptr += 1;
}

/* returns 1 if breakpoint identifier has been found
   returns 0 otherwise 
*/
int
get_inserted_breakpoint_id(char *shadow_contents, unsigned long *bp_id)
{
  struct inserted_breakpoint_list *inserted_breakpoint;
  unsigned long number_of_inserted_breakpoints;

  number_of_inserted_breakpoints = ((bp_extension_ptr)shadow_contents)->number_of_inserted_breakpoints;
  inserted_breakpoint = ((bp_extension_ptr)shadow_contents)->inserted_breakpoints;
  if (number_of_inserted_breakpoints != 0 && inserted_breakpoint) {
	*bp_id = inserted_breakpoint->number;
	return 1;
  } else {
	return 0;
  }
}

void
remove_inserted_breakpoint(char *shadow_contents)
{
  struct inserted_breakpoint_list *inserted_breakpoint;
  struct inserted_breakpoint_list *next_inserted_breakpoint;
  unsigned long *bp_num_ptr
	  = &(((bp_extension_ptr)shadow_contents)->number_of_inserted_breakpoints); 

  inserted_breakpoint = ((bp_extension_ptr)shadow_contents)->inserted_breakpoints;
  next_inserted_breakpoint = inserted_breakpoint->next;
  free(inserted_breakpoint);
  ((bp_extension_ptr)shadow_contents)->inserted_breakpoints = next_inserted_breakpoint;
  *bp_num_ptr -= 1;
}

/* Begin ICD */
enum breakpoint_implementation_method_enum
breakpoint_implementation_method(char *shadow_contents)
{
  return ((bp_extension_ptr)shadow_contents)->breakpoint_implementation_method;
}
/* End ICD */

/* Begin ICD */
int software_breakpoint_implementation_method(char *shadow_contents)
{
  return breakpoint_implementation_method(shadow_contents) == software_breakpoint;
}
/* End ICD */

/* Begin ICD */
int is_software_breakpoint(struct breakpoint *b)
{
  return breakpoint_implementation_method(b->shadow_contents) == software_breakpoint;
}
/* End ICD */

/* Begin ICD */
int is_hardware_breakpoint(struct breakpoint *b)
{
  return breakpoint_implementation_method(b->shadow_contents) == hardware_breakpoint;
}
/* End ICD */

/* Begin ICD */
static void set_breakpoint_implementation_method
(char *shadow_contents,
 enum breakpoint_implementation_method_enum breakpoint_implementation_method)
{
  ((bp_extension_ptr)shadow_contents)->breakpoint_implementation_method
	  = breakpoint_implementation_method;
}
/* End ICD */

/* Begin ICD */
void set_hardware_breakpoint_implementation_method(char *shadow_contents)
{
  set_breakpoint_implementation_method(shadow_contents, hardware_breakpoint);
}
/* End ICD */

/* Begin ICD */
void set_software_breakpoint_implementation_method(char *shadow_contents)
{
  set_breakpoint_implementation_method(shadow_contents, software_breakpoint);
}
/* End ICD */

/* Begin ICD */
int is_software_breakpoint_possible(CORE_ADDR address)
{
  if ((debug_instrument_uses_gdb_software_breakpoints()
	   && is_memory_writable(address)))
	return 1;
  else
	return 0;
}
/* End ICD */

/* Begin ICD */

/* This function returns the number of hardware breakpoints used by GDB.
   
   Hardware breakpoints used by GDB have to be enabled and not duplicates:
   only one breakpoint is inserted at a given address. Other breakpoints
   at the same address are considered as duplicates.
   GDB checks whether a breakpoint is a duplicate or not at the time it is set.
   
   These breakpoints have been set using a standard GDB user command:
   break, tbreak, watch, ...
   in opposition to external breakpoints set without GDB knowing it:
   gdi <ICD break command>
*/

static unsigned int
get_internal_hardware_breakpoint_number (void)
{
  struct breakpoint *b;
  unsigned int count = 0;

  for (b = breakpoint_chain; b; b = b->next)
  {
    if (b->enable == enabled && !b->duplicate && is_hardware_breakpoint(b))
	  count++;
  }
  return count;
}
/* End ICD */

/* Begin ICD */
/* This function returns the number of inserted hardware breakpoints used by GDB.
*/
static unsigned int
get_internal_inserted_hardware_breakpoint_number (void)
{
  struct breakpoint *b;
  unsigned int count = 0;

  for (b = breakpoint_chain; b; b = b->next)
  {
    if (b->inserted && is_hardware_breakpoint(b))
	  count++;
  }
  return count;
}
/* End ICD */

/* Begin ICD */

/* This function is used when setting a breakpoint.
   It computes the number of hardware breakpoints (enabled and not counting
   duplicate breakpoints) not located at the current new breakpoint
   address.
   If there is already a breakpoint at the current address, there is
   no problem to set a second one at the same address since it will be
   considered as duplicate and only one breakpoint will be inserted by
   the debugging instrument: here st7icd.dll driving the ST7 ICD cell.
*/

static unsigned int
get_internal_hardware_breakpoint_number_not_at_address (CORE_ADDR address)
{
  struct breakpoint *b;
  unsigned int count = 0;

  for (b = breakpoint_chain; b; b = b->next)
  {
    if (b->enable == enabled
		&& !b->duplicate
		&& !is_software_breakpoint(b)
		&& b->address != address)
	  count++;
  }
  return count;
}
/* End ICD */

/* Begin ICD */
extern unsigned int external_hardware_breakpoint_number;

/* This function computes the total number of hardware breakpoints:
   breakpoints set through GDB user commands: break, tbreak, watch, ...
   and breakpoints set without GDB knowing it:
   gdi <ICD break command>
*/
static unsigned int
get_hardware_breakpoint_number(int only_inserted_breakpoints)
{
  int internal_hardware_breakpoint_number;

  if (only_inserted_breakpoints)
	internal_hardware_breakpoint_number = get_internal_inserted_hardware_breakpoint_number();
  else
	internal_hardware_breakpoint_number = get_internal_hardware_breakpoint_number();
  return (internal_hardware_breakpoint_number
	      + external_hardware_breakpoint_number);
}
/* End ICD */

/* Begin ICD */
extern unsigned int di_get_number_of_available_hardware_code_breakpoints (void);

/* This function is used when setting a breakpoint.
   It returns 1 if at least one hardware breakpoint is still available.
   It returns 0 if all available hardware breakpoints are used.
   The argument: "address" is the address of the breakpoint being set.
   If a breakpoint has already been set at this address, there is no
   problem to set a second one at the same address
   because only one is really inserted.
*/

int hardware_breakpoint_available(CORE_ADDR address)
{
  if (debug_instrument_uses_purely_software_breakpoints()
	  || debug_instrument_has_an_illimitable_number_of_hardware_breakpoints())
	return 1;
  else
	{
      unsigned int internal_hardware_breakpoint_number
	    = get_internal_hardware_breakpoint_number_not_at_address(address);
      unsigned int hardware_breakpoint_number
	    = internal_hardware_breakpoint_number + external_hardware_breakpoint_number;
      unsigned int hardware_breakpoint_number_max
	    = di_get_number_of_available_hardware_code_breakpoints();

	  if (hardware_breakpoint_number < hardware_breakpoint_number_max)
		return 1;
	  else if (hardware_breakpoint_number == hardware_breakpoint_number_max)
		return 0;
	  else
		{
		  error("GDB7 internal error: too many breakpoints have been set.");
		  return 0;
		}
	}
}
/* End ICD */

extern void di_direct_command (char *args);

#if 0 /* Not used any more. */
/* Begin ICD */
/* disable_hardware_breakpoints disables ICD hardware breakpoints
	if advanced breakpoints have been set by the user.
	*/
void disable_hardware_breakpoints(int *breakpoints_disabled)
{
	if (external_hardware_breakpoint_number != 0) {
		*breakpoints_disabled = (1 | EXTERNAL_HARDWARE_BREAKPOINTS_DISABLED);
		di_direct_command("directbreak disable");
	}
}
/* End ICD */
#endif

/* Begin ICD */

/* This function disables existing hardware breakpoints if there are not enough available
   breakpoints for the current GDB command to perform correctly.
   Below is the list of GDB commands which can use internal breakpoints:
   "next", "nexti", "finish", "until", "goto"
   and also "step", "stepi" if software stepping is used.

   The argument: "hardware_breakpoints_needed" is the number of needed hardware breakpoints.
   It can be 1 or 2 (2 for a software step on a conditional instruction).
   The argument: "breakpoints_disabled" is set to 1 if the existing breakpoints
   had to be disabled; otherwise it is unchanged. 
*/

void disable_all_hardware_breakpoints_if_necessary(int hardware_breakpoints_needed,
												   int *breakpoints_disabled)
{
  disable_hardware_breakpoints_if_necessary(hardware_breakpoints_needed,
											breakpoints_disabled,
											0);
}

void disable_hardware_breakpoints_if_necessary(int hardware_breakpoints_needed,
											   int *breakpoints_disabled,
											   int only_inserted_breakpoints)
{
  /* If the debug instrument (for example the simulator) uses purely software breakpoints 
     or if the number of available hardware breakpoints is illimitable,
     there is nothing to do. */
  if (debug_instrument_uses_purely_software_breakpoints()
	  || debug_instrument_has_an_illimitable_number_of_hardware_breakpoints())
	return;
  else
	{
	  unsigned int used_hardware_breakpoint_number;
	  unsigned int hardware_breakpoint_number_max
	    = di_get_number_of_available_hardware_code_breakpoints();

	  used_hardware_breakpoint_number = get_hardware_breakpoint_number(only_inserted_breakpoints);
	  if ((used_hardware_breakpoint_number + hardware_breakpoints_needed)
		  <= hardware_breakpoint_number_max)
		return;
	  else
		{
		  *breakpoints_disabled = 1;
		  save_enabled_breakpoints(ONLY_HARDWARE_BREAKPOINTS);
		  disable_saved_breakpoints();
		  if (external_hardware_breakpoint_number != 0)
		  {
			*breakpoints_disabled |= EXTERNAL_HARDWARE_BREAKPOINTS_DISABLED;
			di_direct_command("directbreak disable");
		  }
		}
	 }
}
/* End ICD */

/* Print a warning message.
   The first argument STRING is the warning message, used as a fprintf string,
   and the remaining args are passed as arguments to it.
   The primary difference between warnings and errors is that a warning
   does not force the return to command level.

   Same as "warning" function (see utils.c) but interpreted differently by STVD7.
   The message is displayed in a message box.
*/

/* VARARGS */
void
#ifdef ANSI_PROTOTYPES
caution_message (const char *string, ...)
#else
caution_message (va_alist)
     va_dcl
#endif
{
  va_list args;
#ifdef ANSI_PROTOTYPES
  va_start (args, string);
#else
  char *string;

  va_start (args);
  string = va_arg (args, char *);
#endif

  target_terminal_ours ();
  wrap_here("");			/* Force out any buffered output */
  gdb_flush (gdb_stdout);
  fprintf_unfiltered (gdb_stderr, "\ncaution: ");

  vfprintf_unfiltered (gdb_stderr, string, args);
  fprintf_unfiltered (gdb_stderr, "\n");
  va_end (args);
}

/* Begin ICD */

/* This function is used to restore hardware breakpoints which have been disabled
   to execute a GDB command needing temporary breakpoints
   such as next, nexti, finish, until or goto.
   A special warning message is displayed by GDB7.
   This warning can be displayed in a message box by the graphical interface STVD7.

   This function is necessary only when the number of available breakpoints
   is limited such as for debug instruments consisting of ST7 microcontrollers
   which have an "In Circuit Debugging" cell
   with a limited number of hardware breakpoints.
*/
int break_disabled_message_counter = 0;

void restore_hardware_breakpoints(int breakpoints_disabled)
{
  int restore_external_hardware_breakpoints;

  if (debug_instrument_uses_purely_software_breakpoints()
	  || debug_instrument_has_an_illimitable_number_of_hardware_breakpoints())
	return;

  restore_external_hardware_breakpoints
	  = breakpoints_disabled & EXTERNAL_HARDWARE_BREAKPOINTS_DISABLED;
  if (restore_external_hardware_breakpoints)
	di_direct_command("directbreak enable");
  restore_breakpoints_and_clean_up();
  break_disabled_message_counter++;
  if (break_disabled_message_counter == 1) {
	caution_message(
"BREAK_DISABLED When performing a step, a return to the calling function \
or a go to line, if no breakpoint is available, \
breakpoints are momentarily disabled.");
  }
}
/* End ICD */

void
/* The "_initialize_" keyword is automatically searched at the beginning of the line.
   The "void" keyword must be placed on the preceding line. */
_initialize_st7emu_extensions(void)
{
	add_com("perf_run",
			class_gdi, (void (*) (char *, int))perf_run_command,
			"Run the program in performance analysis mode.\n\
Syntax:\n\
\tperf_run\n\
\tperf_run -nobreak");
	
	add_com("perf_continue",
			class_gdi, (void (*) (char *, int))perf_continue_command,
			"Continue the program in performance analysis mode.\n\
Syntax:\n\
\tperf_continue\n\
\tperf_continue -nobreak");

	add_com("run_no_wait",
			class_gdi, (void (*) (char *, int))run_no_wait_command,
			"Run the program in read/write on the fly mode.\n\
Syntax:\n\
\trun_no_wait\n");

	add_com("continue_no_wait",
			class_gdi, (void (*) (char *, int))continue_no_wait_command,
			"Continue the program in read/write on the fly mode.\n\
Syntax:\n\
\tcontinue_no_wait\n");

	add_com("stop_no_wait",
			class_gdi, (void (*) (char *, int))stop_no_wait_command,
			"Stop the program in read/write on the fly mode.\n\
Syntax:\n\
\tstop_no_wait\n");

	add_com("get_execution_status",
			class_gdi, (void (*) (char *, int))get_execution_status_command,
			"Get execution status of program running in read/write on the fly mode.\n\
Syntax:\n\
\tget_execution_status\n");

	add_com("set_standalone_mode",
			class_gdi, (void (*) (char *, int))set_standalone_mode_command,
			"Release emulated chip from emulator control.\n\
Syntax:\n\
\tset_standalone_mode\n");

		add_com("goto",
			class_st7, (void (*) (char *, int))goto_command,
			"Continue execution until specified line is reached.\n\
Syntax:\n\
\tgoto [<file name>:]<line number>\n");
}
