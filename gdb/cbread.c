/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* Read a symbol table in Crash Barrier format.

   Copyright SGS-THOMSON 1995

*/

/* Read symbols from an Crash Barrier map file. */


#include "defs.h"
#include "bfd.h"
#include "symtab.h"
#include "gdbtypes.h"
#include "symfile.h"
#include "objfiles.h"
#include "gdb-stabs.h"
#include "buildsym.h"
#include "complaints.h"
#include <string.h>

#include <sys/types.h>
#include <fcntl.h>

#include "libcbmap.h" /* Ugly but this is a costless implementation */

static asymbol **symbol_table; /* symbol table initialized at minimal symbol fetch */
static unsigned int number_of_symbols;

#ifdef TARGET_STM7
core_type mcu_core = ST7_CORE; /* ST7, STM7 or STM7P */
core_type mcu_core_variant = ST7_CORE; /* ST7, STM7, STM7PV1 or STM7PV2 */
core_type compiler_target = NO_CORE; /* ST7, STM7 or STM7P */
static unsigned int assembler_target_already_parsed = 0;
#endif

#define LST_MAX_LINE_LENGTH 256 
  char lst_line[LST_MAX_LINE_LENGTH + 1];

static void
cbmap_new_init PARAMS ((struct objfile *));

static void
cbmap_symfile_init PARAMS ((struct objfile *));

static void
cbmap_symfile_read PARAMS ((struct objfile *, struct section_offsets *, int));

static void
cbmap_symfile_finish PARAMS ((struct objfile *));

static void
cbmap_msymtab_read PARAMS ((bfd *,  CORE_ADDR, struct objfile *));

static struct section_offsets *
cbmap_symfile_offsets PARAMS ((struct objfile *, CORE_ADDR));

static void
record_minimal_symbol PARAMS ((char *, CORE_ADDR, enum minimal_symbol_type,
                         struct objfile *));


static void
record_minimal_symbol (name, address, ms_type, objfile)
     char *name;
     CORE_ADDR address;
     enum minimal_symbol_type ms_type;
     struct objfile *objfile;
{
  name = obsavestring (name, strlen (name), &objfile -> symbol_obstack);
  prim_record_minimal_symbol (name, address, ms_type, objfile
#ifdef TARGET_ST18950
                        , INVALID_DATA_SPACE
#endif /* DATA_SPACE */
                        );
}

static void
cbmap_msymtab_read PARAMS ((bfd *,  CORE_ADDR, struct objfile *));


static void
hicross_psymtab_read PARAMS ((bfd *,
                        struct section_offsets *,
                        struct objfile *));


#define PST_PRIVATE(p) ((struct symloc *)(p)->read_symtab_private)
#define SECTION(p) (PST_PRIVATE(p)->section)
#define LANGUAGE(p) (PST_PRIVATE(p)->pst_language)

struct symloc
{
  asection *section;
  enum language pst_language;
};

/* Things we import explicitly from other modules */

extern int info_verbose;

/* strtoul_in encapsulate standard ANSI strtoul plus one limit parameter */
/* Convert the string within limit into an integer */
static unsigned long
strtoul_in (char *ptr,
            char ** endptr,
            int base,
            int limit)
{
  char tempo;
  unsigned long r;

  if (strlen (ptr) > limit)
  {
    tempo = ptr[limit];
    ptr[limit] = 0; /* isolate string until limit */
    r = strtoul (ptr, endptr, base);
    ptr[limit] = tempo;
    return r;
  }
  else return strtoul (ptr, endptr, base);
}

/* skip_include_file is able to skip one INCLUDE part of the lst file
   currently opened and pointed to by fp. path is the current path for
   this file. The string containing the current #INCLUDE line is lst_line.
   return 0 if something wrong occurred. */
#if 0 /* Change Include file parsing strategy. Old code before Crash Barrier change */
static int
skip_include_file (char *path,
            FILE *fp)
{
  char *incname, *next;
  int result;
  unsigned long i, count, line_num;
  FILE *ifp;
  char line[LST_MAX_LINE_LENGTH + 1];

  /* First, find the include file name */
  incname = strchr(lst_line, '"');
  if (!incname) return 0;
  incname++;
  next = strchr (incname, '"');
  if (!next) return 0;
  *next = '\0'; /* isolate file name string */

  /* open the include file and parse it to retreive the number of line */
  result = openp (path, 0, incname, O_RDONLY, 0, 0);
  if (result < 0)
    {
      /* Didn't work.  Try using just the basename. */
      char *p = basename (incname);
      if (p != incname)
        result = openp(path, 0, p, O_RDONLY,0, 0);
    }

  if (result == 0) return 0;

  ifp = fdopen (result, "rt");
  count = 0;
  while (fgets (line, LST_MAX_LINE_LENGTH, ifp)) count++;
  fclose (ifp);
  close (result);

  /* Now we know the number of line, skip in the lst file */ 
  for (i = 0; i < count; i++)
  {
    if (!fgets (lst_line, LST_MAX_LINE_LENGTH, fp)) return 0;
    /* Treat recursively the case of multi-level include files */
    line_num = strtoul_in (lst_line, &next, 10, 5);
    if (line_num == 0)
      if (!strncmp (lst_line, "   0 ", 5))
      {
        /* We are at the beginning of an INCLUDE FILE */
        if (!skip_include_file (path, fp)) return 0;
      }
      else
        i--; /* This is an assembler bannier line. To be skipped always */
  }
  
  return 1;
}
#else
/* End of include files in lst files are now marked <END_OF_INCLUSION>
   since pack8 version of Crash Barrier for ST7. This fix the big
   problem of knowing how many lines there are to skip in include file */

static int
skip_include_file (char *path,
            FILE *fp)
{
  unsigned long line_num;
  while (fgets (lst_line, LST_MAX_LINE_LENGTH, fp))
  {
    /* Don't test any more for <END_OF_INCLUSION> at the beginning of the line.
	   It doesn't seem necessary and it solves the problem of include files
	   with trailing white-space characters. */
	if (strstr (lst_line, "<END_OF_INCLUSION>") != NULL)
	  break;
    /* Treat recursively the case of multi-level include files */
    line_num = strtoul_in (lst_line, NULL, 10, 5);
    if (line_num == 0)
      if (!strncmp (lst_line, "   0 ", 5))
      {
        /* We are at the beginning of an INCLUDE FILE */
        if (!skip_include_file (path, fp)) return 0;
      }
  }
  /* If we have not found END_OF_INCLUSION, this means that the application
     was assembled with a version of Crash Barrier not owning this feature. */
  /* Now we suppose that there is no more version problem.
	 If there are trailing white-space characters, <END_OF_INCLUSION> doesn't start
	 at column 1. */
  if (strncmp (lst_line, "<END_OF_INCLUSION>", 18) != 0)
  {
    printf_filtered ("Line starting with <END_OF_INCLUSION> not found in .lst file\n");
    warning ("Remove white-space characters at the end of the last line of the include file");
	if (strstr (lst_line, "<END_OF_INCLUSION>") != NULL) {
	  return 1;
	} else {
	  return 0;
	}
  }
  return 1;
}
#endif

#ifdef TARGET_STM7
enum function_call_type make_call_type_enum(char *call_type)
{
	if (call_type != NULL) {
		if (strcmp(call_type, "no") == 0)
			return NO_CALL;
		else if (strcmp(call_type, "nearcall") == 0)
			return NEAR_CALL;
		else if (strcmp(call_type, "farcall") == 0)
			return FAR_CALL;
		else if (strcmp(call_type, "interrupt") == 0)
			return INTERRUPT_HANDLER;
		else
			error("GDB7 internal error: %s is not a recognized call type.", call_type);
	} else {
		error("GDB7 internal error: no call type.");
	}
	return NO_CALL;
}
#endif

unsigned int get_digit_number(unsigned int number)
{
	int digit_number = 1;
	int num = number;

	while (num) {
		num /= 10;
		if (num) {
			digit_number++;
		}
	}
	return digit_number;
}

/* psymtab_to_symtab_1 is taken from mdebugread.c */

/* Ancillary function to psymtab_to_symtab().  Does all the work
   for turning the partial symtab PST into a symtab, recurring
   first on all dependent psymtabs.  The argument FILENAME is
   only passed so we can see in debug stack traces what file
   is being read.
*/

static void
psymtab_to_symtab_1 (pst, filename)
     struct partial_symtab *pst;
     char *filename;
{
  unsigned long mptr, i, j;
  struct symtab *st;

  if (pst->readin)
    return;
  pst->readin = 1;

  /* Read in all partial symbtabs on which this one is dependent.
     NOTE that we do have circular dependencies, sigh.  We solved
     that by setting pst->readin before this point.  */

  for (i = 0; i < pst->number_of_dependencies; i++)
    if (!pst->dependencies[i]->readin)
      {
    /* Inform about additional files to be read in.  */
    if (info_verbose)
      {
        fputs_filtered (" ", gdb_stdout);
        wrap_here ("");
        fputs_filtered ("and ", gdb_stdout);
        wrap_here ("");
        printf_filtered ("%s...",
                 pst->dependencies[i]->filename);
        wrap_here ("");    /* Flush output */
        gdb_flush (gdb_stdout);
      }
    /* We only pass the filename for debug purposes */
    psymtab_to_symtab_1 (pst->dependencies[i],
                 pst->dependencies[i]->filename);
      }

    /* GROUP LIST : pass other psymtabs which are attached to the same
       filename. */
    {
      struct partial_symtab *cur_pst;

      if (pst->next_in_group != NULL)
        for (cur_pst = pst->next_in_group;
             cur_pst != pst ;
             cur_pst = cur_pst->next_in_group)
          psymtab_to_symtab_1 (cur_pst, cur_pst->filename);
    }

  /* Do nothing if this is a dummy psymtab.  */

  if (pst->n_global_syms == 0 && pst->n_static_syms == 0
      && pst->textlow == 0 && pst->texthigh == 0)
    return;

  /* Now prepare reading the symbols for this symtab */

  current_objfile = pst->objfile;

  /* See comment in parse_partial_symbols about the @stabs sentinel. */
  processing_gcc_compilation = 0;

  /* start_symtab creates a subfile for the source filename.
     We have to modify the name by eliminating [index] catenation */
  {
    char *dirname = strdup (filename);
    char *eod = strrchr (dirname, '/');
    if (eod) {
        *eod = 0; /* isolate dirname */
        eod++;
    }
    else {
        eod = dirname;
        dirname = NULL;
    }

    start_symtab (eod, dirname, pst->textlow);
    current_subfile->language = LANGUAGE (pst);

  }

  /* Now start the most complicate part of the work :
     we have to open the listing file corresponding to this
     psymtab in order to get the informations about
     the source line table and symbols.
  */
  {
  extern char *source_path;
  char *path = source_path;
  int result;
  char *lstname = alloca (strlen (filename) + 5);
  char *end;

  strcpy (lstname, filename);
  end = strrchr (lstname, '.');
  if (end != NULL)
    strcpy (end, ".lst"); /* replace suffix ASM by LST in lstname */

  result = openp (path, 0, lstname, O_RDONLY, 0, 0);
  if (result < 0)
    {
      /* Didn't work.  Try using just the basename. */
      char *p = basename (lstname);
      if (p != lstname)
        result = openp(path, 0, p, O_RDONLY,0, 0);
    }

  if (result >= 0)
  {
    FILE *fp;
    char *pt;
    int Privacy_Segment_Size_present;
	int Call_present;
    unsigned long i, line_num, addr;
    char *addr_end;
    const unsigned long line_min = SECTION (pst)->target_index;
    const unsigned long line_max = SECTION (pst)->target_index +
                                   SECTION (pst)->lineno_count;
    const unsigned long addr_min = SECTION (pst)->vma;
    const unsigned long segment_size = SECTION (pst)->_raw_size;
    const unsigned long addr_max = SECTION (pst)->vma + SECTION (pst)->_raw_size - 1;


    /* Allocate temporarily an array of _raw_size char that will indicate
       if a symbol has been found at this address of the section */
    char * cb_sym_found = alloca (segment_size);
    memset (cb_sym_found, 0, segment_size);

    fp = fdopen (result, "rt");

#if 0 /* Old code dealing with line number limits */
    /* skip listing lines until start line for the current psymtab section */
    line_num = 0;

    if (!(SECTION (pst)->userdata))
    /* MISUSE of userdata field  :
           we must not try to find the line code infos
           because the section is in an include file.
    */
    while (line_num != line_min)
    {
      if (!fgets (lst_line, LST_MAX_LINE_LENGTH, fp)) break;
      line_num = strtoul_in (lst_line, &pt, 10, 5);
      if (line_num == 0)
        if (!strncmp (lst_line, "   0 ", 5))
          /* We are at the beginning of an INCLUDE FILE */
          if (!skip_include_file (path, fp)) break;
    }

    /* --- Parse the line code infos  --- */
    if (line_num == line_min)
    {
       if (SECTION (pst)->flags & SEC_CODE)
       /* search line informations */

       while ( line_num <= line_max) {

          if (line_num && *(lst_line+13) != ' ') {
            addr = strtoul_in (pt, &pt, 16, 7);
#ifdef TARGET_ST18950
            addr <<= 1;
#endif
            /* add a lineinfo */
            record_line (current_subfile, line_num, addr);
          }

          if (!fgets (lst_line, LST_MAX_LINE_LENGTH, fp)) break;
          pt = lst_line;
          line_num = strtoul_in (pt, &pt, 10, 5);
       }

    }

#else /* #if 0 */
    /* We simplify here the line search algorithm.
       Just compare section limits with the current address */

    *lst_line = 0;

    if (!(SECTION (pst)->userdata)
    /* MISUSE of userdata field  :
           we must not try to find the line code infos
           because the section is in an include file.
    */
       && (SECTION (pst)->flags & SEC_CODE)) 
       /* need search line informations */
    while (strncmp (lst_line, "Symbol Name", 11) != 0)
	{
#ifdef TARGET_STM7
unsigned int code_offset;
unsigned int standard_code_offset;
#endif
      if (!fgets (lst_line, LST_MAX_LINE_LENGTH, fp)) break;

#ifdef TARGET_STM7
      line_num = strtoul (lst_line, &pt, 10);
#else
      line_num = strtoul_in (lst_line, &pt, 10, 5);
#endif
#ifdef TARGET_STM7
	  if (line_num == 1)
	  {
		  int l;
		  char *comment;
		  char *mcu_name;
		  char *strrchr_result;

		  /* possible cases:
			st7
			st7/
			st7.tab
			st7.tab/
			It can also be prefixed by a directory:
			D:\Program Files\STMicroelectronics\st7toolset\asm\ST7/
			*/
		  /* remove spaces after line number */
		  while (isspace(*pt))
			pt++;
		  /* remove comment */
		  if (comment = strchr(lst_line, ';'))
		  {
			*comment = 0;
		  }
		  /* remove spaces at the end */
		  l = strlen(lst_line) - 1;
		  while (l >= 0 && isspace(lst_line[l]))
		  {
			lst_line[l] = 0;
			l--;
		  }
		  /* st7/ or stm7/ is normally expected
			but also expect st7 or stm7 */
		  l = strlen(pt) - 1;
		  /* remove ending '/' if present */
		  if (l >= 0 && pt[l] == '/')
			pt[l] = 0;
		  /* Take care: the MCU name may be preceded by the directory
		     where the MCU.TAB file is located. */
		  if (strrchr_result = strrchr(pt, '\\'))
			pt = strrchr_result + 1;
		  mcu_name = pt;
		  /* also expect upper case */
		  while (*pt)
		  {
			  *pt = tolower(*pt);
			  pt++;
		  }
		  /* the .tab suffix may be present */
		  mcu_name = strtok(mcu_name, ".");
		  if (strcmp(mcu_name, "st7") == 0)
		  {
			  compiler_target = ST7_CORE;
			  standard_code_offset = 13;
		  }
		  else if (strcmp(mcu_name, "stm7") == 0)
		  {
			  compiler_target = STM7_CORE;
			  standard_code_offset = 15;
		  }
		  else if (strcmp(mcu_name, "stm7p") == 0
					|| strcmp(mcu_name, "stm7p_tc") == 0
					|| strcmp(mcu_name, "stm8") == 0
					)
		  {
			  compiler_target = STM7P_CORE;
			  standard_code_offset = 15;
		  }
		  else
		  {
			  compiler_target = NO_CORE;
			  error("GDB7 internal error: not a supported microcontroller.");
		  }
		  if (debug_instrument_opened && !assembler_target_already_parsed) {
			check_compiler_target_compatibility_with_debug_target();
		  }
		  code_offset = standard_code_offset;
		  assembler_target_already_parsed = 1;
		  continue;
	  }
#endif
      if (line_num == 0)
        if (!strncmp (lst_line, "   0 ", 5))
          /* We are at the beginning of an INCLUDE FILE */
          if (!skip_include_file (path, fp)) break;

		  /* Four characters are normally reserved for line numbers. */
		  if (line_num >= 10000) {
			  unsigned int digit_number = get_digit_number(line_num);
			  code_offset = standard_code_offset + (digit_number - 4);
		  }
          /* Test for code existence. */
          if (line_num && *(lst_line+
#ifdef TARGET_STM7
			  code_offset
#else
			  13
#endif
			  ) != ' ') {
#ifdef TARGET_STM7
			addr = strtoul(pt, &addr_end, 16);
#else
            addr = strtoul_in (pt, &addr_end, 16, 7);
#endif
            /* Handle lines with unknown addresses. */
            /* In fact, it is not necessary for EQU directives as there is no code generated:
              "  14  ????                       equ5      equ      {var3+var2}"
              so we will never reach this code. */
            if (addr_end == pt) {
              warning("Incorrect assembler list file syntax: %s", lst_line);
              continue;
            }
#ifdef TARGET_ST18950
            addr <<= 1;
#endif
            if (addr >= addr_min && addr <= addr_max)
              /* add a lineinfo */
              record_line (current_subfile, line_num, addr);
          }
    }
#endif /* #if 0 #else */

    /* --- Parse the symbols --- */

    /* the symbols are listed at the of the lst file, providing that the
       option -sym has been used. The list start after "Symbol Name" keyword */
    while (strncmp (lst_line, "Symbol Name", 11))
	{
      if (!fgets (lst_line, LST_MAX_LINE_LENGTH, fp))
		  break;
	}

    /* We support old version of Crash Barrier assembler that did not
       provide full symbol information. Missing information in symbol list was
       Privacy, Segment and Type. */
    Privacy_Segment_Size_present = (strstr (lst_line, "Privacy") != NULL);
#ifdef TARGET_STM7
	/* We support old version of Crash Barrier assembler that did not
       provide full symbol information. Missing information in symbol list was
       Call type. */
	Call_present = (strstr (lst_line, "Call") != NULL);
#endif

    while (fgets (lst_line, LST_MAX_LINE_LENGTH, fp))
    {
      char *cur, *symbol_name;
      unsigned long value;
      char *value_end;
	  char *call_type;
      unsigned long line_number;
      char *privacy;
      char *segment;
      unsigned long object_size;
      unsigned long bytes;
      char *include_file;
      struct symbol *s;
	  unsigned int line_offset;

      if (!strstr (lst_line, "Internal")) continue; /* Not a scope symbol definition */

#define NAME_COLUMN_WIDTH 34
	  line_offset = NAME_COLUMN_WIDTH;
      cur = lst_line + line_offset;
      value = strtoul(cur, &value_end, 16);
      /* Handle lines with unknown addresses:
        "equ2                                  ????         no  WORD  Internal   rel     10  private  bss          0     0  -"
      */
      if (value_end == cur) {
        /* No address has been found. */
        /* Skip blanks. */
        while (isspace(*cur)) {
          cur++;
        }
        /* Issue a warning only if there are no question marks. */
        if (*cur != '?') {
          warning("Incorrect assembler list file syntax: %s", lst_line);
        }
        continue;
      }
#ifdef TARGET_ST18950
      value <<= 1;
#endif
      /* check that the current symbol belongs to the current psymtab.*/
      if (value < addr_min || value >= addr_min + segment_size) continue;

#define VALUE_COLUMN_WIDTH 10
	  line_offset += VALUE_COLUMN_WIDTH;

#ifdef TARGET_STM7
	  if (Call_present)
	  {
		call_type = lst_line + line_offset;
		/* Skip blanks. */
		while (*call_type == ' ' || *call_type == '\t') call_type++;
		/* Isolate call type. */
		cur = call_type;
        while (*cur != ' ' && *cur != '\t') cur++;
        *cur = 0;
#define CALL_COLUMN_WIDTH 11
		line_offset += CALL_COLUMN_WIDTH;
	  }
#endif

#define TYPE_COLUMN_WIDTH 6
#define SCOPE_COLUMN_WIDTH 11
#define RELATIVITY_COLUMN_WIDTH 6
	  line_offset += (TYPE_COLUMN_WIDTH + SCOPE_COLUMN_WIDTH + RELATIVITY_COLUMN_WIDTH);
      cur = lst_line + line_offset;
      line_number = strtoul(cur, &cur, 10);

      symbol_name = lst_line;
      cur = symbol_name;
      while (*cur != ' ' && *cur != '\t') cur++;
      *cur = 0; /* isolate symbol name */

      if (Privacy_Segment_Size_present)
      {
#define LINE_COLUMN_WIDTH 6
		line_offset += LINE_COLUMN_WIDTH;
        privacy = lst_line + line_offset;
        cur = privacy;
        while (*cur != ' ' && *cur != '\t') cur++;
        *cur = 0; /* isolate privacy type */

#define PRIVACY_COLUMN_WIDTH 9
		line_offset += PRIVACY_COLUMN_WIDTH;
        segment = lst_line + line_offset;
        cur = segment;
        while (*cur != ' ' && *cur != '\t') cur++;
        *cur = 0; /* isolate segment type */

#define SEGMENT_COLUMN_WIDTH 10
		line_offset += SEGMENT_COLUMN_WIDTH;
        cur = lst_line + line_offset;
        object_size = strtoul(cur, &cur, 10);
#ifdef TARGET_ST18950
        object_size <<= 1;
#endif
        /* eliminates EQU and extern definitions */
        if (object_size == 0) continue;
		/* WORKAROUND asm 6.02 bug that may not set 0 to EQU object_size
		   Check with section size in order to eliminate most of cases */
		if (value + object_size > addr_min + segment_size) continue;

#define CODE_SIZE_COLUMN_WIDTH 6
		line_offset += CODE_SIZE_COLUMN_WIDTH;
        cur = lst_line + line_offset;
        bytes = strtoul(cur, &cur, 10);

#define DATA_SIZE_COLUMN_WIDTH 6
		line_offset += DATA_SIZE_COLUMN_WIDTH;
        include_file = lst_line + line_offset;
        cur = include_file;
        while (*cur != ' ' && *cur != '\t'&& *cur != '\n') cur++;
        *cur = 0; /* isolate include file */
        if (*include_file != '-')
			line_number = 0; /* include not supported */
      }

      s = (struct symbol *) 
      obstack_alloc (&current_objfile -> symbol_obstack,
                     sizeof (struct symbol));
      memset (s, 0, sizeof (struct symbol));

      SYMBOL_NAME (s) = obsavestring (symbol_name, strlen (symbol_name),
                       &current_objfile -> symbol_obstack);

      SYMBOL_LINE(s) = line_number;
      SYMBOL_LANGUAGE (s) = LANGUAGE (pst);

      SYMBOL_NAMESPACE (s) = VAR_NAMESPACE;

      if (Privacy_Segment_Size_present)
      {
        if (!strcmp (segment,"text") || !strcmp (segment,"textdata"))
        {
          /* describe any Crash Barrier text label as a function */
          struct context_stack *context;
		  struct block *block;

          SYMBOL_TYPE (s) = lookup_function_type (builtin_type_void);

          SYMBOL_CLASS (s) = LOC_BLOCK;
          SYMBOL_VALUE_ADDRESS(s) = value;

          context = push_context (0, value);
          /* No local symbol to declare here for assembler language */
          context = pop_context ();
          block = finish_block (s, &local_symbols, context->old_blocks,
			                    value, value + object_size, current_objfile);
#ifdef TARGET_STM7
		  if (Call_present)
		  {
			  block->call_type = make_call_type_enum(call_type);
		  }
#endif
        }

        if (!strcmp (segment,"bss") || !strcmp (segment,"data"))
        {
          /* describe any Crash Barrier constant or data label as an array */
          struct type *range_type, *element_type;

          switch (bytes)
          {
            case 2: element_type = builtin_type_unsigned_short;
                    break;
            case 4: element_type = builtin_type_unsigned_long;
                    break;
            default: element_type = builtin_type_unsigned_char;
                     bytes = 1;
          }

          if (bytes == object_size)
            /* This is a single object */
            SYMBOL_TYPE (s) = element_type;
          else
          {
            /* This is an array of objects */
            range_type = alloc_type (current_objfile);
            range_type =
            create_range_type (range_type, builtin_type_int, 0, object_size/bytes-1);
               SYMBOL_TYPE (s) = create_array_type (alloc_type (current_objfile),
                                   element_type, range_type);
          }

          SYMBOL_CLASS (s) = LOC_STATIC;
          SYMBOL_VALUE_ADDRESS (s) = value;
        }

        if (strcmp (privacy, "private") == 0)
		  add_symbol_to_list (s, &file_symbols);
        else
          add_symbol_to_list (s, &global_symbols);
      }
      else
      {
        SYMBOL_CLASS (s) = LOC_STATIC;
        SYMBOL_VALUE_ADDRESS (s) = value;

        SYMBOL_TYPE (s) = builtin_type_unsigned_char;

        add_symbol_to_list (s, &global_symbols);
      }

        cb_sym_found[value-addr_min] = 1;
    }

    /* --- Parse the remaining section symbols  --- */

    /* Find symbols that belong to this section */
    for (i = 0; i < number_of_symbols; i++)
    {
      asymbol *sym = symbol_table[i];
      if (sym -> section == SECTION (pst))
      /* filter only non already found symbols */
      if (!cb_sym_found[sym->value - addr_min])
      {
        struct symbol *s;

        s = (struct symbol *) 
          obstack_alloc (&current_objfile -> symbol_obstack,
                         sizeof (struct symbol));
        memset (s, 0, sizeof (struct symbol));

        SYMBOL_LINE(s) = ((cbmap_symbol_type *)sym)->linenumdef;
        SYMBOL_LANGUAGE (s) = LANGUAGE (pst);
        SYMBOL_VALUE_ADDRESS (s) = sym->value;
        SYMBOL_CLASS (s) = LOC_LABEL;

        switch (((cbmap_symbol_type *)sym)->scope_length)
        {
          case 1: SYMBOL_TYPE (s) = builtin_type_unsigned_char;
                  break;
          case 2: SYMBOL_TYPE (s) = builtin_type_unsigned_short;
                  break;
          default: SYMBOL_TYPE (s) = builtin_type_unsigned_char;
        }

        SYMBOL_NAMESPACE (s) = VAR_NAMESPACE;

        SYMBOL_NAME (s) = obsavestring ((char *) sym->name, strlen (sym->name),
                      &current_objfile -> symbol_obstack);

        add_symbol_to_list (s, &global_symbols);
      }
    }

    fclose (fp);
    close (result);
  }
  else
  {
    printf_filtered ("Cannot open listing file %s\n", lstname);
    warning ("Missing listing file. Please check your assembler options");
  }

  }

  /* pending blocks may be unordered */
  st = end_symtab (pst->texthigh, pst->objfile, SECT_OFF_TEXT);

  /* Sort the symbol table now, we are done adding symbols to it.
     We must do this before parse_procedure calls lookup_symbol.  */
  sort_symtab_syms (st);
  
  /* Now link the psymtab and the symtab.  */
  pst->symtab = st;

  current_objfile = NULL;
}

/* cbmap_psymtab_to_symtab is taken from mdebugread.c */

/* Exported procedure: Builds a symtab from the PST partial one.
   Restores the environment in effect when PST was created, delegates
   most of the work to an ancillary procedure, and sorts
   and reorders the symtab list at the end */

static void
cbmap_psymtab_to_symtab (struct partial_symtab *pst)
{

  if (!pst)
    return;

  if (info_verbose)
    {
      printf_filtered ("Reading in symbols for %s...", pst->filename);
      gdb_flush (gdb_stdout);
    }

  psymtab_to_symtab_1 (pst, pst->filename);

  if (info_verbose)
    printf_filtered ("done.\n");
}

/* cbmap_end_psymtab is taken from end_psymtab in dbxread.c */

/* Close off the current usage of PST.  
   Returns PST or NULL if the partial symtab was empty and thrown away.

   FIXME:  List variables and peculiarities of same.  */

static struct partial_symtab *
cbmap_end_psymtab (pst, include_list, num_includes,
         dependency_list, number_dependencies)
     struct partial_symtab *pst;
     char **include_list;
     int num_includes;
     struct partial_symtab **dependency_list;
     int number_dependencies;
{
  int i;
  struct partial_symtab *p1;
  struct objfile *objfile = pst -> objfile;

  pst->n_global_syms =
    objfile->global_psymbols.next - (objfile->global_psymbols.list + pst->globals_offset);
  pst->n_static_syms =
    objfile->static_psymbols.next - (objfile->static_psymbols.list + pst->statics_offset);

  pst->number_of_dependencies = number_dependencies;
  if (number_dependencies)
    {
      pst->dependencies = (struct partial_symtab **)
    obstack_alloc (&objfile->psymbol_obstack,
               number_dependencies * sizeof (struct partial_symtab *));
      memcpy (pst->dependencies, dependency_list,
         number_dependencies * sizeof (struct partial_symtab *));
    }
  else
    pst->dependencies = 0;

  for (i = 0; i < num_includes; i++)
    {
      struct partial_symtab *subpst =
    allocate_psymtab (include_list[i], objfile);

      subpst->section_offsets = pst->section_offsets;
      subpst->read_symtab_private =
      (char *) obstack_alloc (&objfile->psymbol_obstack,
                  sizeof (struct symloc));
      /* ### NYI LDSYMOFF(subpst) =
      LDSYMLEN(subpst) =
      subpst->textlow =
        subpst->texthigh = 0; */

      /* We could save slight bits of space by only making one of these,
     shared by the entire set of include files.  FIXME-someday.  */
      subpst->dependencies = (struct partial_symtab **)
    obstack_alloc (&objfile->psymbol_obstack,
               sizeof (struct partial_symtab *));
      subpst->dependencies[0] = pst;
      subpst->number_of_dependencies = 1;

      subpst->globals_offset =
    subpst->n_global_syms =
      subpst->statics_offset =
        subpst->n_static_syms = 0;

      subpst->readin = 0;
      subpst->symtab = 0;
      subpst->read_symtab = pst->read_symtab;
    }

  sort_pst_symbols (pst);

  /* If there is already a psymtab or symtab for a file of this name, remove it.
     (If there is a symtab, more drastic things also happen.)
     This happens in VxWorks.  */
  free_named_symtabs (pst->filename);

  return pst;
}

/*

LOCAL FUNCTION

    cbmap_psymtab_read -- read the symbol table of an Crash Barrier map file

SYNOPSIS

    void cbmap_psymtab_read (bfd *abfd, CORE_ADDR addr,
                struct section_offsets *,
                  struct objfile *objfile)

DESCRIPTION

    Given an open bfd, a base address to relocate symbols to, and a
    flag that specifies whether or not this bfd is for an executable
    or not (may be shared library for example), build the
    partial_symtab list of the objfile along with adding all the global
    function and data symbols to the minimal symbol table.

    For the Crash Barier map format, there is one psymtab per section displayed
    inside the map file. The filename of one psymtab is the
    the source filename with [] index appearing at section line definition
    The aim of one psymtab is to hold
    the maximum informations about one module whithout openning it.
    Module objects will be openned and read later at symtab building.
*/

static void
cbmap_psymtab_read (abfd, addr, objfile)
     bfd *abfd;
     struct section_offsets *addr;
     struct objfile *objfile;
{
  struct partial_symtab *pst;
  asection *s;

  asymbol *sym;
  unsigned int i;

  /* Parse sections and build one psymtab per section */
  for (s = abfd->sections; s && !quit_flag; s = s->next)
  {
    char asmname [300];
	char *source_name;
    char *end;
	char * pt;

	strcpy (asmname, s->name);
	end = strrchr (asmname, '[');

    if (end != NULL) 
      *end = '\0'; /* remove temporarily [index] part for source filename */

    source_name = basename (asmname);

    /* force source name to lower case */
    for (pt = asmname; pt < asmname + strlen( asmname ); pt++ )
	{
       if( isupper( *pt ) )
          *pt = _tolower( *pt );
	}

      pst = start_psymtab_common (objfile, addr,
                  source_name,
                  s->vma,
                  objfile->global_psymbols.next,
                  objfile->static_psymbols.next);
      pst->read_symtab_private = ((char *)
                  obstack_alloc (&objfile->psymbol_obstack,
                         sizeof (struct symloc)));
      memset ((PTR) pst->read_symtab_private, 0, sizeof (struct symloc));

      LANGUAGE (pst) = language_unknown;
      SECTION (pst) = s;

      /* The way to turn this into a symtab is to call... */
      pst->read_symtab = cbmap_psymtab_to_symtab;

      pst->texthigh = s->vma + s->_raw_size;

	  /* Now find symbols that belong to this section */
      for (i = 0; i < number_of_symbols; i++)
      {
        sym = symbol_table[i];
        if (sym -> section == s)
        {
		  if (! (sym->flags & BSF_SECTION_SYM))
          /* Build a psymbol */
          add_psymbol_to_list ((char *) sym->name,
            strlen (sym->name),
            VAR_NAMESPACE, LOC_STATIC,
            &objfile->global_psymbols,
			0,
            (CORE_ADDR) sym->value,
            LANGUAGE(pst), objfile);
        }
      }

    /* GROUP LIST : we need to mark all the psymtab having the same 
       filename in order to be able to always translate them into symtabs
       together.
       This is done by using the special next_in_group field of psymtab: it is
       a circular list of same named psymtabs. */
    {
      struct partial_symtab *prev_pst;

      pst->next_in_group = NULL;

      /* parse the already built psymtabs in search of same filenamed */
      for (prev_pst = objfile->psymtabs; prev_pst; prev_pst = prev_pst->next)
      {
        if (prev_pst == pst) continue;
        if (strcmp (source_name, prev_pst->filename)) continue;

        /* we got one, insert it in circular list */
        if (prev_pst->next_in_group == NULL)
           pst->next_in_group = prev_pst; /* initialize circularity */
        else
           pst->next_in_group = prev_pst->next_in_group;
        prev_pst->next_in_group = pst;
        break;

      }
    }

  cbmap_end_psymtab (pst, NULL, 0, NULL, 0);
  /* We need to read the assembler target: st7 or stm7
     which is found in the .lst list file. */
  PSYMTAB_TO_SYMTAB (pst);
  }
}

/*

LOCAL FUNCTION

    cbmap_msymtab_read -- read the symbol table

SYNOPSIS

    void cbmap_msymtab_read (bfd *abfd, CORE_ADDR addr,
                  struct objfile *objfile)

DESCRIPTION

    Given an open bfd, a base address to relocate symbols to, and a
    flag that specifies whether or not this bfd is for an executable
    or not (may be shared library for example), add all the global
    function and data symbols to the minimal symbol table.
*/

static void
cbmap_msymtab_read (abfd, addr, objfile)
     bfd *abfd;
     CORE_ADDR addr;
     struct objfile *objfile;
{
  unsigned int storage_needed;
  asymbol *sym;
  unsigned int i;
  CORE_ADDR symaddr;
  enum minimal_symbol_type ms_type;
  asection *s;
  
  storage_needed = bfd_get_symtab_upper_bound (abfd);

  if (storage_needed > 0)
    {
      symbol_table = (asymbol **) obstack_alloc (&objfile -> psymbol_obstack,
                                                 storage_needed);
      number_of_symbols = bfd_canonicalize_symtab (abfd, symbol_table); 
  
      for (i = 0; i < number_of_symbols; i++)
    {
      sym = symbol_table[i];
      if (sym -> flags & BSF_GLOBAL)
        {
          /* Bfd symbols are not section relative for Crash Barrier. */
          symaddr = sym -> value;
          /* Relocate all non-absolute symbols by base address.  */
          if (sym -> section != &bfd_abs_section)
        {
          symaddr += addr;
        }

          if (sym -> section == &bfd_abs_section)
        {
          ms_type = mst_data; /* map default to data for Crash Barrier */
        }

          /* For non-absolute symbols, use the type of the section
         they are relative to, to intuit text/data.  Bfd provides
         no way of figuring this out for absolute symbols. */
          else if (sym -> section -> flags & SEC_CODE)
        {
          ms_type = mst_text;
        }
          else if (sym -> section -> flags & SEC_DATA)
        {
          ms_type = mst_data;
        }
          else
        {
          ms_type = mst_unknown;
        }
          if (sym -> flags & BSF_WEAK) ms_type = mst_unknown; /* to be eliminated if dupplicate */

          record_minimal_symbol ((char *) sym -> name, symaddr, ms_type,
                     objfile);
        }
    }

    }
}

/* Initialize anything that needs initializing when a completely new symbol
   file is specified (not just adding some symbols from another file, e.g. a
   shared library).

   We reinitialize buildsym, since gdb will be able to read stabs from an NLM
   file at some point in the near future.  */

static void
cbmap_new_init (ignore)
     struct objfile *ignore;
{
  buildsym_new_init ();
  assembler_target_already_parsed = 0;
}


/* cbmap_symfile_init ()
   is the CRASH-BARRIER-specific initialization routine for reading symbols.
   It is passed a struct objfile which contains, among other things,
   the BFD for the file whose symbols are being read, and a slot for
   a pointer to "private data" which we fill with cookies and other
   treats for cbmap_symfile_read ().

*/

static void
cbmap_symfile_init (objfile)
     struct objfile *objfile;
{
  /* CRASH BARRIER objects may be reordered, so set OBJF_REORDERED.  If we
     find this causes a significant slowdown in gdb then we could
     set it in the debug symbol readers only when necessary.  */
  objfile->flags |= OBJF_REORDERED;

  init_entry_point_info (objfile);
}



/* Scan and build partial symbols for a symbol file.
   We have been initialized by a call to cbmap_symfile_init, which 
   currently does nothing.

   SECTION_OFFSETS is a set of offsets to apply to relocate the symbols
   in each section.  We simplify it down to a single offset for all
   symbols.  FIXME.

   MAINLINE is true if we are reading the main symbol
   table (as opposed to a shared lib or dynamically loaded file).

   This function only does the minimum work necessary for letting the
   user "name" things symbolically; it does not read the entire symtab.
   Instead, it reads the external and static symbols and puts them in partial
   symbol tables.  When more extensive information is requested of a
   file, the corresponding partial symbol table is mutated into a full
   fledged symbol table by going back and reading the symbols
   for real.

   Note that NLM files have two sets of information that is potentially
   useful for building gdb's minimal symbol table.  The first is a list
   of the publically exported symbols, and is currently used to build
   bfd's canonical symbol table.  The second is an optional native debugging
   format which contains additional symbols (and possibly duplicates of
   the publically exported symbols).  The optional native debugging format
   is not currently used. */

static void
cbmap_symfile_read (objfile, section_offsets, mainline)
     struct objfile *objfile;
     struct section_offsets *section_offsets;
     int mainline;
{
  bfd *abfd = objfile -> obfd;
  struct cleanup *back_to;
  CORE_ADDR offset;

  init_minimal_symbol_collection ();
  back_to = make_cleanup (discard_minimal_symbols, 0);

  /* FIXME, should take a section_offsets param, not just an offset.  */

  offset = ANOFFSET (section_offsets, 0);

  /* Build minimal and partial symtab */
  cbmap_msymtab_read (abfd, offset, objfile);
  cbmap_psymtab_read (abfd, section_offsets, objfile);

  /* FIXME:  We could locate and read the optional native debugging format
     here and add the symbols to the minimal symbol table. */

  if (!have_partial_symbols ())
    {
      wrap_here ("");
      printf_filtered ("(no debugging symbols found)...");
      wrap_here ("");
    }

  /* Install any minimal symbols that have been collected as the current
     minimal symbols for this objfile. */

  install_minimal_symbols (objfile);

  do_cleanups (back_to);
}


/* Perform any local cleanups required when we are done with a particular
   objfile.  I.E, we are in the process of discarding all symbol information
   for an objfile, freeing up all memory held for it, and unlinking the
   objfile struct from the global list of known objfiles. */

static void
cbmap_symfile_finish (objfile)
     struct objfile *objfile;
{
  if (objfile -> sym_private != NULL)
    {
      mfree (objfile -> md, objfile -> sym_private);
    }
}

/* Fake up identical offsets for all sections.  */

static
struct section_offsets *
cbmap_symfile_offsets (objfile, addr)
     struct objfile *objfile;
     CORE_ADDR addr;
{
  struct section_offsets *section_offsets;
  int i;

  objfile->num_sections = SECT_OFF_MAX;
  section_offsets = (struct section_offsets *)
    obstack_alloc (&objfile -> psymbol_obstack,
           sizeof (struct section_offsets) +
           sizeof (section_offsets->offsets) * (SECT_OFF_MAX-1));

  for (i = 0; i < SECT_OFF_MAX; i++)
    {
      ANOFFSET (section_offsets, i) = addr;
    }
  
  return (section_offsets);
}


/* Register that we are able to handle CB file format. */

static struct sym_fns cbmap_sym_fns =
{
  bfd_target_cbmap_flavour,
  cbmap_new_init,        /* sym_new_init: init anything gbl to entire symtab */
  cbmap_symfile_init,    /* sym_init: read initial info, setup for sym_read() */
  cbmap_symfile_read,    /* sym_read: read a symbol file into symtab */
  cbmap_symfile_finish,    /* sym_finish: finished with file, cleanup */
  cbmap_symfile_offsets,    /* sym_offsets:  Translate ext. to int. relocation */
  NULL            /* next: pointer to next struct sym_fns */
};

void
_initialize_cbread ()
{
  add_symtab_fns (&cbmap_sym_fns);
}
